<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function user_info()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'article');
    }
}
