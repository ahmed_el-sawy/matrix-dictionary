<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = [];

    public function cat_info ()
    {
        return $this->belongsTo('App\Category', 'category');
    }
    public function language_info ()
    {
        return $this->belongsTo('App\Language', 'language');
    }
}
