<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    public function translations(){
        return $this->belongsToMany('App\Category_trans', 'category');
    }

    public function subs()
    {
        return $this->hasMany('App\Category', 'sub_from');
    }

    public function dictionaries(){
        return $this->hasMany(Dictionary::class, 'sub_category');
    }

    public function words_sum($id){
        return Dictionary::where('sub_category', $id)->sum('words_num');
    }

    public function cat_info()
    {
        return $this->belongsTo('App\Category', 'sub_from');
    }

    public function cat_words()
    {
        if($this->sub_from > 0)
        {
            $total = $this->hasMany('App\Dictionary', 'sub_category')->sum('words_num');
            return $total;        }
        else
        {
            $total = $this->hasMany('App\Dictionary', 'category')->sum('words_num');
            return $total;
        }
    }
}
