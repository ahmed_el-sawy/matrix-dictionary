<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $guarded = [];

    public function cat_info ()
    {
        return $this->belongsTo('App\Category', 'category');
    }

    public function files ()
    {
        return $this->hasMany('App\Dictionary_file', 'dictionary');
    }
    
    public function words ()
    {
        return $this->hasMany('App\Word', 'dictionary');
    }
}
