<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictionary_file extends Model
{
    protected $table = 'dictionary_files';
    protected $guarded = [];

    public function words ()
    {
        return $this->hasMany('App\Word', 'file');
    }
}
