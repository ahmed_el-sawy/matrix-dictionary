<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function login(Request $request)
    {
        if($request->isMethod('post')){
            $data = $request->input();
            if(Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'status' => true])){
                if(auth()->user()->type == 0){
                    return redirect('admin/dashboard')->with('success'.'تم تسجيل الدخول بنجاح');
                }else{
                    Auth::logout();
                    return redirect('admin')->with('error'.'ليس لديك صلاحيات دخول');
                }
            }else{
                return redirect('admin')->with('error', 'فشل الدخول بيانات خاطئة');
            }
        }else{
            if(auth()->user()){
                if(auth()->user()->type == 0){
                    return redirect('admin/dashboard')->with('success'.'تم تسجيل الدخول بنجاح');
                }else{
                    Auth::logout();
                    return redirect('admin')->with('error'.'ليس لديك صلاحيات دخول');
                }
            }else{
                return view('dashboard.login');
            }

        }

        return view('dashboard.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/admin');
    }
}
