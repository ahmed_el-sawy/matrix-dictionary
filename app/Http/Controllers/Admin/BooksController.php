<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use App\Category;
use App\Http\Controllers\Controller;
use App\Language;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BooksController extends Controller
{
    use AjaxData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Book::get();
        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '!=', 0)->get();

        return view('dashboard.books/index', ['data' => $data, 'languages' => $languages, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    /**
     * Books ajax DT
     */

    public function books(){
        $books = Book::get();
        return $this->dataResponse($books);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '!=', 0)->get();

        return view('dashboard.books/create', ['languages' => $languages, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'title' => 'required',
            'language' => 'required',
            'category' => 'required',
            'copyRight' => 'required',
            'author' => 'required',
            'book' => 'required|mimes:xlsx,xls,xlsb,xlsm,doc,docx,pdf',
        ];

        $attr = [
            'title' => 'Title',
            'language' => 'Source Language',
            'category' => 'Category',
            'book' => 'Book File',
            'copyRight' => 'copyRight',
            'author' => 'Author'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {

            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        $file = $request->file('book');

        if($request->hasFile('book')){
            $ufile = $request->file('book');
            $fileName = time().'.'.$ufile->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/books/');
            $ufile->move($destinationPath, $fileName);
            $request['file'] = '/uploads/books/'.$fileName;
        }

        Book::create($request->except(['book', '_token', '_method']));

        return response()->json(['success' => true, 'message'=>'Login Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '!=', 0)->get();

        return view('dashboard.books/edit', ['data' => $book, 'languages' => $languages, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);

        $data = [
            'title' => 'required',
            'language' => 'required',
            'category' => 'required',
            'copyRight' => 'required',
            'author' => 'required'
        ];

        $attr = [
            'title' => 'Title',
            'language' => 'Source Language',
            'category' => 'Category',
            'copyRight' => 'copyRight',
            'author' => 'Author'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {

            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }


        if($request->hasFile('book')){

            $data = [
                'book' => 'required|mimes:xlsx,xls,xlsb,xlsm,doc,docx,pdf'
            ];   
            $attr = [
                'book' => 'Book File'
            ];
    
            $validator = Validator::make($request->all(), $data, [], $attr);
    
            if ($validator->fails()) {
    
                return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
            }

            
            $ufile = $request->file('book');
            $fileName = time().'.'.$ufile->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/books/');
            $ufile->move($destinationPath, $fileName);
            $request['file'] = '/uploads/books/'.$fileName;
        }

        $book->update($request->except(['book', '_token', '_method']));

        return response()->json(['success' => true, 'message'=>'Login Successfully']);
    }

    /**
     * change status
     */

    public function status($id){
        $data = Book::findOrFail($id);
        $status = $data->status == 0 ? 1 : 0;

        $data->update(['status' => $status]);

        return back()->with('success', 'Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $book->delete();
        return redirect()->route('books.index')->with('success', 'Success remove book.');
    }
}
