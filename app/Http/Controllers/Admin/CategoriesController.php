<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Category_trans;
use App\Http\Controllers\Controller;
use App\Language;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\ImageManagerStatic as Image;

class CategoriesController extends Controller
{
    use AjaxData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function new_index()
    {
        $main_category = NULL;
        $data = Category::where('sub_from', 0)->count();
        return view('dashboard.categories.index', ['data' => $data, 'main_category'=> $main_category]);
    }

    public function index()
    {
        $main_category = NULL;
        $data = Category::where('sub_from', 0)->count();
        $cats = Category::where('sub_from', 0)->get();
        return view('dashboard.categories.new_index', ['data' => $data, 'main_category'=> $main_category, 'cats'=>$cats]);
    }
    
    public function show($id)
    {
        $main_category = Category::findorfail($id);
        $data = Category::where('sub_from', $id)->count();
        return view('dashboard.categories.index', ['data' => $data, 'main_category' => $main_category]);
    }

    /**
     * Categories ajax DT
     */
    public function categories(){
        $data = Category::where('sub_from', 0)->get();

        return $this->dataResponse($data);
    }
    public function sub_categories($id){
        $main_category = Category::findorfail($id);
        $data = Category::where('sub_from', $id)->get();
        return $this->dataResponse($data);
    }
    
    public function sub_categories_dropdown(Request $request){
        $main_category = Category::findorfail($request->cat);
        $data = Category::where('sub_from', $request->cat)->get();
        ?>
        <option value="">Choose Category</option>
        <?php
        foreach($data as $category)
        {
            ?>
            <option value="<?php echo $category->id; ?>"><?php echo $category->title; ?></option>
            <?php
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('sub_from', 0)->get();

        return view('dashboard.categories/create', ['categories' => $categories, 'page_name' => 'Add new category']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'title' => 'required',
        ];

        $attr = [
            'title' => 'Title',
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $category = new Category();
        $category->title = $request->title;
        $category->sub_from = $request->sub_from;
        if($request->hasFile('icon'))
        {
            $validatedData = $request->validate([
                'icon' => 'required|mimes:jpeg,png,jpg,gif,svg,webp'
            ],
            $messages = [
                'icon.required' => 'Please Choose Image',
                'icon.mimes' => 'Please Choose Image'
            ]);

            $image = $request->file('icon');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/dictionary_categories');
            $image->move($destinationPath, $imageName);
            $category->icon = '/uploads/dictionary_categories/'.$imageName;
            $image = Image::make(public_path().$category->icon)->resize(null, 71, function ($constraint) {$constraint->aspectRatio();})->save(public_path().$category->icon);
        }
        
        $category->save();

        
        return redirect()->route('categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Category::findOrFail($id);

        $categories = Category::where('sub_from', 0)->get();

        return view('dashboard.categories/edit', ['categories' => $categories, 'data' => $data, 'page_name' => 'Edit category']);
    }

    /**
     * change status
     */

    public function status($id){
        $data = Category::findOrFail($id);
        $status = $data->status == 0 ? 1 : 0;

        $data->update(['status' => $status]);

        return back()->with('success', 'Success');
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);

        $data = [
            'title' => 'required',
        ];

        $attr = [
            'title' => 'الإسم',
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $category->title = $request->title;
        $category->sub_from = $request->sub_from;
                if($request->hasFile('icon'))
        {
            $validatedData = $request->validate([
                'icon' => 'required|mimes:jpeg,png,jpg,gif,svg,webp'
            ],
            $messages = [
                'icon.required' => 'Please Choose Image',
                'icon.mimes' => 'Please Choose Image'
            ]);

            if (File::exists(public_path().$category->icon))
            {
                File::delete(public_path().$category->icon);
            }

            $image = $request->file('icon');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/dictionary_categories');
            $image->move($destinationPath, $imageName);
            $category->icon = '/uploads/dictionary_categories/'.$imageName;
            $image = Image::make(public_path().$category->icon)->resize(null, 71, function ($constraint) {$constraint->aspectRatio();})->save(public_path().$category->icon);
        }

        $category->save();

        return redirect()->back()->with('success', 'Success update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return redirect()->back()->with('success', 'Removed');
    }
}
