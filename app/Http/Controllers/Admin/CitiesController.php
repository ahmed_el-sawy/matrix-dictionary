<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\City;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    use AjaxData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = City::count();
        return view('dashboard.cities.index', ['data' => $data]);
    }

    /**
     * cities ajax DT
     */
    public function cities(){
        $data = City::join('countries', 'countries.id', '=', 'cities.country_id')
                ->select('cities.*', 'countries.country')
                ->get();

        return $this->dataResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::get();
        return view('dashboard.cities.create', ['countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'city' => 'required',
        ];

        $attr = [
            'city' => 'إسم المدينة',
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $city = City::create($request->except(['_token', '_method']));

        return redirect()->route('cities.index')->with('success', 'تم الإضافة بنجاح');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = City::findOrFail($id);
        $countries = Country::get();
        return view('dashboard.cities.edit', ['data' => $data, 'countries' => $countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::findOrFail($id);

        $data = [
            'city' => 'required',
        ];

        $attr = [
            'city' => 'إسم المدينة',
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $city->city = $request->city;
        $city->country_id = $request->country_id;
        $city->save();

        return redirect()->back()->with('success', 'تم الحفظ بنجاح');
    }

    /**
     * change status
     */

    public function status($id){
        $data = City::findOrFail($id);
        $status = $data->status == 0 ? 1 : 0;

        $data->update(['status' => $status]);

        return back()->with('success', 'تم تغيير الحالة بنجاح');
     }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $city->delete();

        return redirect()->route('cities.index')->with('success', 'تم الحذف بنجاح');
    }
}
