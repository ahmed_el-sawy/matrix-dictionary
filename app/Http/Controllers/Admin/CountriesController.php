<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;

class CountriesController extends Controller
{
    use AjaxData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Country::count();
        return view('dashboard.countries.index', ['data' => $data]);
    }

    /**
     * Countries ajax DT
     */
    public function countries(){
        $data = Country::get();

        return $this->dataResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'country' => 'required',
        ];

        $attr = [
            'country' => 'country name',
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $country = Country::create($request->except(['_token', '_method']));

        return redirect()->route('countries.index')->with('success', 'Success Add');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Country::findOrFail($id);

        return view('dashboard.countries.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $country = Country::findOrFail($id);

        $data = [
            'country' => 'required',
        ];

        $attr = [
            'country' => 'Country name',
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $country->country = $request->country;
        $country->code = $request->code;
        $country->save();

        return redirect()->back()->with('success', 'Success save');
    }

    /**
     * change status
     */

    public function status($id){
        $data = Country::findOrFail($id);
        $status = $data->status == 0 ? 1 : 0;

        $data->update(['status' => $status]);

        return back()->with('success', 'Success update');
     }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        $country->delete();

        return redirect()->route('countries.index')->with('success', 'Deleted !');
    }
}
