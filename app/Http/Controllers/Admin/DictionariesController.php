<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Dictionary;
use App\Dictionary_file;
use App\Http\Controllers\Controller;
use App\Language;
use App\Word;
use Validator;
use Illuminate\Http\Request;

class DictionariesController extends Controller
{
    use AjaxData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Dictionary::count();

        return view('dashboard.Dictionary/index', ['data' => $data]);
    }

    /**
     * Dictionaries ajax DT
     */

    public function dictionaries(){
        $dictionaries = Dictionary::orderBy('id', 'DESC')
            ->join('languages', 'languages.id', '=', 'dictionaries.source_lang')
            ->join('categories', 'categories.id', '=', 'dictionaries.category')
            ->select('dictionaries.*', 'languages.language', 'categories.title AS category_title')
            ->get();

        // $resource = new DictionaryResource($dictionaries);
        // dd($resource->response()->getData(true));
        return $this->dataResponse($dictionaries);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '!=', 0)->get();

        return view('dashboard.Dictionary/create', ['languages' => $languages, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'title' => 'required',
            'source_lang' => 'required',
            'target_lang' => 'required',
            'category' => 'required',
        ];

        $attr = [
            'title' => 'Title',
            'source_lang' => 'Source Language',
            'target_lang' => 'Target Language',
            'category' => 'Category'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $dictionary = Dictionary::create($request->except(['file', '_token', '_method']));

        Dictionary_file::where('dictionary', $dictionary->id)->update(['status'=>$dictionary->status]);
        Word::where('dictionary', $dictionary->id)->update(['status'=>$dictionary->status]);


        return redirect()->route('dictionaries.index')->with('success', 'Success add dictionary');
    }

    /**
     * change status
     */

    public function status($id){
        $data = Dictionary::findOrFail($id);
        if($data->status == 0)
        {
            $data->status = 1;
            $data->save();
        }
        else
        {
            $data->status = 0;
            $data->save();
        }
        Dictionary_file::where('dictionary', $data->id)->update(['status'=>$data->status]);
        // foreach ($data->files as $file)
        // {
        //     $file->status = $data->status;
        //     $file->save();
        // }
        Word::where('dictionary', $data->id)->update(['status'=>$data->status]);
        // foreach ($data->words as $word)
        // {
        //     $word->status = $data->status;
        //     $word->save();
        // }
        return back()->with('success', 'Success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dictionary = Dictionary::findOrFail($id);

        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '=', $dictionary->category)->get();

        return view('dashboard.Dictionary/edit', ['data' => $dictionary, 'languages' => $languages, 'categories' => $categories, 'sub_categories' => $sub_categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dictionary = Dictionary::findOrFail($id);

        $data = [
            'title' => 'required',
            'source_lang' => 'required',
            'target_lang' => 'required',
            'category' => 'required',
        ];

        $attr = [
            'title' => 'Title',
            'source_lang' => 'Source Language',
            'target_lang' => 'Target Language',
            'category' => 'Category'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $dictionary->update($request->except(['_token', '_method']));

        Dictionary_file::where('dictionary', $dictionary->id)->update(['status'=>$dictionary->status]);
        Word::where('dictionary', $dictionary->id)->update(['status'=>$dictionary->status]);
        return redirect()->route('dictionaries.index')->with('success', 'Success update dictionary');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dictionary = Dictionary::findOrFail($id);
        $dictionary->delete();

        return redirect()->route('dictionaries.index')->with('success', 'Success remove dictionary');
    }
}
