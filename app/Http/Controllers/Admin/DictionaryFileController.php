<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Dictionary;
use App\Dictionary_file;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\DictionariesImport;
use App\Language;
use App\Word;
use App\Words_media;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Facades\Datatables;


class DictionaryFileController extends Controller
{
    public function edit($id)
    {
        $file = Dictionary_file::findorfail($id);
        $dictionary = Dictionary::findorfail($file->dictionary);
        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '!=', 0)->get();
        $dictionaries = Dictionary::all();
        $page_name = "<a href='".url('admin/words?d='.$dictionary->id)."'>".$dictionary->title."</a> \ Edit File";
        return view('dashboard.words.edit_file', ['languages' => $languages, 'categories' => $categories, 'sub_categories' => $sub_categories, 
        'dictionaries' => $dictionaries, 'file'=>$file, 'page_name'=>$page_name]);
    }
    
    public function update($id, Request $request)
    {
        $data = [
            'dictionary' => 'required',
            'source_lang' => 'required',
            'target_lang' => 'required',
            'category' => 'required',
        ];

        $attr = [
            'dictionary' => 'Dictionary',
            'source_lang' => 'Source Language',
            'target_lang' => 'Target Language',
            'category' => 'Category'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        /**\
         * Save Dictionary File
         */
        $dictionary_file = Dictionary_file::findorfail($id);
        $dictionary_file->dictionary = $request->dictionary;
        $dictionary_file->source = $request->source;
        $dictionary_file->copy_right = $request->copy_right;
        $dictionary_file->author = $request->author;
        $dictionary_file->source_lang = $request->source_lang;
        $dictionary_file->target_lang = $request->target_lang;
        $dictionary_file->category = $request->category;
        $dictionary_file->sub_category = $request->sub_category;
        $dictionary_file->status = $request->status;
        $dictionary_file->save();

        Word::where('file', $dictionary_file->id)->update(['status'=>$dictionary_file->status]);

        /*
        if($request->hasFile('file'))
        {
            $data = [
                'file' => 'required|mimes:xlsx,xls,xlsb,xlsm|max:204800',
            ];
    
            $attr = [
                'file' => 'Excel File',
            ];
    
            $validator = Validator::make($request->all(), $data, [], $attr);
    
            if ($validator->fails()) {
                return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $path = Storage::putFile('dictionaries', $file);
            $url = '/storage/app/'.$path;
            $dictionary_file->file = $url;
            $words = Word::where('file', $dictionary_file->id)->get();
            foreach ($words as $word)
            {
                $word->delete();
            }
            $dictionary = Dictionary::findOrFail($request->dictionary);
            $data = Excel::import(new DictionariesImport($request->dictionary, $id), $file);
            $words = Word::where('dictionary', $request->dictionary)->count();
            $dictionary->words_num = $words;
            $dictionary->save();
            $dictionary_file->save();
        }
        */
        return redirect('admin/words?d='.$dictionary_file->dictionary);
    }
    
    
    public function destroy($id)
    {
        $dictionary_file = Dictionary_file::findorfail($id);
        $words = Word::where('file', $dictionary_file->id)->get();
        foreach ($words as $word)
        {
            $word->delete();
        }
        $dictionary = Dictionary::findOrFail($dictionary_file->dictionary);
        $words = Word::where('dictionary', $dictionary_file->dictionary)->count();
        $dictionary->words_num = $words;
        $dictionary->save();
        $dictionary_file->delete();

        return redirect('admin/words?d='.$dictionary_file->dictionary);
    }
    
}