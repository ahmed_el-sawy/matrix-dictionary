<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\ImageManagerStatic as Image;

use App\AboutSection;

class HomeAboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = AboutSection::get();
        return view('dashboard.home_about.index')->with(['sections'=>$sections]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.home_about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title'=>'required',
            'text'=>'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
        ],
        $messages = [
            'title.required' => 'Please Enter Title',
            'text.required' => 'Please Enter Text',
            'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Image'
        ]);

        $slider = new AboutSection;   
        $slider->title = $request->title;
        $slider->text = $request->text;
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/home_about');
        $image->move($destinationPath, $imageName);
        $slider->image ='/uploads/home_about/'.$imageName;
        $image = Image::make(public_path().$slider->image)->resize(null, 71, function ($constraint) {$constraint->aspectRatio();})->save(public_path().$slider->image);
        $slider->save();
        return redirect()->route('home_about_us_sections.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = AboutSection::findOrFail($id);
        return view('dashboard.home_about.edit')->with(['slider'=>$slider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'title'=>'required',
            'text'=>'required',
        ],
        $messages = [
            'title.required' => 'Please Enter Title',
            'text.required' => 'Please Enter Text',
        ]);
        
        $slider = AboutSection::findOrFail($id);
        $slider->title = $request->title;
        $slider->text = $request->text;
        
        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
            ],
            $messages = [
                'image.required' => 'Please Choose Image',
                'image.mimes' => 'Please Choose Image'
            ]);

            if (File::exists(public_path().$slider->image))
            {
                File::delete(public_path().$slider->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/home_about');
            $image->move($destinationPath, $imageName);
            $slider->image = '/uploads/home_about/'.$imageName;
            $image = Image::make(public_path().$slider->image)->resize(null, 71, function ($constraint) {$constraint->aspectRatio();})->save(public_path().$slider->image);
        }
        
        $slider->save();
        return redirect()->route('home_about_us_sections.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = AboutSection::findOrFail($id);

        if (File::exists(public_path().$slider->image))
        {
            File::delete(public_path().$slider->image);
        }
        $slider->delete();
        return redirect()->route('home_about_us_sections.index');
    }
}