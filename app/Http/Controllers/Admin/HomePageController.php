<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

use App\PageSection;

class HomePageController extends Controller
{
    public function banner ()
    {
        $section = PageSection::where('position', 'home_banner')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'home_banner';
            $section->save();
        }
        return view('dashboard.home.banner', compact('section'));
    }
    
    public function save_banner (Request $request)
    {
        $data = [
            'title' => 'required',
            'link' => 'required|url',
            'btn_text' => 'required'
        ];

        $attr = [
            'title' => 'Title',
            'link' => 'Link',
            'btn_text' => 'Button Text'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {return back()->withErrors($validator)->withInput();}
        $section = PageSection::where('position', 'home_banner')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'home_banner';
            $section->save();
        }
        $section->title = $request->title;
        $section->link = $request->link;
        $section->btn_text = $request->btn_text;
        $section->save();
        
        return redirect()->back();
    }
    
    
    
    public function categories_section ()
    {
        $section = PageSection::where('position', 'home_categories_section')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'home_categories_section';
            $section->save();
        }
        return view('dashboard.home.categories', compact('section'));
    }
    
    public function save_categories_section (Request $request)
    {
        $data = [
            'title' => 'required',
            'description' => 'required'
        ];

        $attr = [
            'title' => 'Title',
            'description' => 'Description'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {return back()->withErrors($validator)->withInput();}
        $section = PageSection::where('position', 'home_categories_section')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'home_categories_section';
            $section->save();
        }
        $section->title = $request->title;
        $section->text = $request->description;
        $section->save();
        
        return redirect()->back();
    }
    
    
    public function home_about_us ()
    {
        $section = PageSection::where('position', 'home_about_us')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'home_about_us';
            $section->save();
        }
        return view('dashboard.home.about', compact('section'));
    }
    
    public function save_home_about_us (Request $request)
    {
        $data = [
            'title' => 'required',
            'link' => 'required|url',
            'text' => 'required',
            'btn_text' => 'required'
        ];

        $attr = [
            'title' => 'Title',
            'link' => 'Link',
            'text' => 'Description',
            'btn_text' => 'Button Text'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {return back()->withErrors($validator)->withInput();}
        $section = PageSection::where('position', 'home_about_us')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'home_about_us';
            $section->save();
        }
        $section->title = $request->title;
        $section->link = $request->link;
        $section->text = $request->text;
        $section->btn_text = $request->btn_text;
        
        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg,webp'
            ],
            $messages = [
                'image.required' => 'Please Choose Image',
                'image.mimes' => 'Please Choose Image'
            ]);

            if (File::exists(public_path().$section->image))
            {
                File::delete(public_path().$section->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/home_about');
            $image->move($destinationPath, $imageName);
            $section->image = '/uploads/home_about/'.$imageName;
        }
        
        $section->save();
        
        return redirect()->back();
    }
    
    
    public function search_words ()
    {
        $section = PageSection::where('position', 'search_words')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'search_words';
            $section->save();
        }
        return view('dashboard.home.search_words', compact('section'));
    }
    
    public function save_search_words (Request $request)
    {
        $data = [
            'title' => 'required',
            'description' => 'required'
        ];

        $attr = [
            'title' => 'Title',
            'description' => 'Description'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {return back()->withErrors($validator)->withInput();}
        $section = PageSection::where('position', 'search_words')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'search_words';
            $section->save();
        }
        $section->title = $request->title;
        $section->text = $request->description;
        $section->save();
        
        return redirect()->back();
    }

        
    
    public function work_process ()
    {
        $section = PageSection::where('position', 'work_process')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'work_process';
            $section->save();
        }
        return view('dashboard.home.work_process', compact('section'));
    }
    
    public function save_work_process (Request $request)
    {
        $data = [
            'title' => 'required',
            'description' => 'required'
        ];

        $attr = [
            'title' => 'Title',
            'description' => 'Description'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {return back()->withErrors($validator)->withInput();}
        $section = PageSection::where('position', 'work_process')->first();
        if($section === NULL)
        {
            $section = new PageSection;
            $section->position = 'work_process';
            $section->save();
        }
        $section->title = $request->title;
        $section->text = $request->description;
        $section->save();
        
        return redirect()->back();
    }
    
}