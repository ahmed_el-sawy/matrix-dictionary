<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\ImageManagerStatic as Image;

use App\WorkProcess;

class HomeProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = WorkProcess::get();
        return view('dashboard.work_process.index')->with(['sections'=>$sections]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = WorkProcess::findOrFail($id);
        return view('dashboard.work_process.edit')->with(['slider'=>$slider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'title'=>'required',
            'text'=>'required',
        ],
        $messages = [
            'title.required' => 'Please Enter Title',
            'text.required' => 'Please Enter Text',
        ]);
        
        $slider = WorkProcess::findOrFail($id);
        $slider->title = $request->title;
        $slider->text = $request->text;
        
        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
            ],
            $messages = [
                'image.required' => 'Please Choose Image',
                'image.mimes' => 'Please Choose Image'
            ]);

            if (File::exists(public_path().$slider->image))
            {
                File::delete(public_path().$slider->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/work_process');
            $image->move($destinationPath, $imageName);
            $slider->image = '/uploads/work_process/'.$imageName;
            $image = Image::make(public_path().$slider->image)->resize(null, 71, function ($constraint) {$constraint->aspectRatio();})->save(public_path().$slider->image);
        }
        
        $slider->save();
        return redirect()->route('home_work_process_sections.index');
    }
}