<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Language;
use Illuminate\Http\Request;

class LanguagesController extends Controller
{
    use AjaxData;
    public function index(){
        $data = Language::count();
        return view('dashboard.languages/index', ['data' => $data]);
    }

    public function languages(){
        $data = Language::get();
        return $this->dataResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.languages/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Language::create($request->except(['_token', '_method']));

        return redirect()->route('languages.index')->with('success', 'Success add');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Language::findOrFail($id);
        return view('dashboard.languages/edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Language::findOrFail($id);

        $data->language = $request->language;
        $data->code = $request->code;
        $data->save();

        return redirect()->route('languages.index')->with('success', 'Success edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Language::findOrFail($id);
        $data->delete();
        return redirect()->route('languages.index')->with('success', 'Success remove');
    }
}
