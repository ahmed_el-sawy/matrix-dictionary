<?php
namespace App\Http\Controllers\Admin;

use App\Career;
use App\Country;
use App\Http\Controller\Admin\DT;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ManagersController extends Controller
{
    use AjaxData;

    private $user ;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user()->su;
            if($this->user == 0){
                return abort(404);
            }
            return $next($request);
        })->except([]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('type', '=', 0)->where('su', 0)->orderBy('id', 'DESC')->get();

        return view('dashboard.managers.index', ['users' => $users, 'page_name' => 'Managers']);
    }

    /**
     * users ajax DT
     */

    public function managers(){
        $users = User::where('type', '=', 0)->where('su', 0)->get();
        return $this->dataResponse($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::get();
        $jobs = Career::get();

        return view('dashboard.managers.create', ['countries' => $countries, 'jobs' => $jobs, 'page_name' => 'Add new Manager data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'mobile' => 'required|unique:users',
            'password' => 'required|confirmed',
            'photo' => 'mimes:jpeg,jpg,bmp,png',
        ];

        $attr = [
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'photo' => 'Personal photo'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $path = Storage::putFile('users', $file);
            $url = '/storage/app/'.$path;
            $request['avatar'] = $url;
        }

        $request['password'] = Hash::make($request->password);
        $request['type'] = 0;

        User::create($request->except(['photo', '_token', '_method', 'password_confirmation']));

        return redirect()->route('managers.index')->with('success', 'Success add manager');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findOrFail($id);
        $countries = Country::get();
        $jobs = Career::get();

        return view('dashboard.managers.edit',  ['countries' => $countries, 'data' => $data, 'jobs' => $jobs, 'page_name' => 'Edit manager data']);
    }

    /**
     * change status
     */

     public function status($id){
        $data = User::findOrFail($id);
        $status = $data->status == 0 ? 1 : 0;

        $data->update(['status' => $status]);

        return back()->with('success', 'Success Update');
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $data = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'mobile' => 'required|unique:users,mobile,'.$id,
            'password' => 'confirmed',
            'photo' => 'mimes:jpeg,jpg,bmp,png',
        ];

        $attr = [
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'photo' => 'Personal photo'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $path = Storage::putFile('users', $file);
            $url = '/storage/app/'.$path;
            $request['avatar'] = $url;
        }

        if($request->has('password')){
            $request['password'] = Hash::make($request->password);
        }

        $user->update($request->except(['photo', '_token', '_method', 'password_confirmation']));

        return redirect()->back()->with('success', 'Success Save');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->back()->with('success', 'Success delete user');
    }
}
