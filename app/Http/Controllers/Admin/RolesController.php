<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesController extends Controller
{
    use AjaxData;

    public function index(){
        $roles = Role::all();

        return view('dashboard.roles.index', ['roles' => $roles]);
    }

    /**
     * Roles ajax DT
     */

    public function roles(){
        $roles = Role::all();
        return $this->dataResponse($roles);
    }

    public function create(){
        $permissions = Permission::all();

        return view('dashboard.roles.create', ['permissions' => $permissions]);
    }

    public function store(Request $request){
        $data = [
            'role' => 'required|unique:roles,name',
            'permissions' => 'required'
        ];

        $attr = [
            'role' => 'Role name',
            'permissions' => 'Permissions'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $role = Role::create(['name' => $request->role]);

        $role->syncPermissions($request->permissions);

        return back()->with('success', 'success add');
    }

    public function edit($id){
        $data = Role::findById($id);
        $permissions = Permission::all();

        return view('dashboard.roles.edit', ['data' => $data, 'permissions' => $permissions]);
    }

    public function update(Request $request, $id){
        $data = [
            'role' => 'required|unique:roles,name,'.$id,
            'permissions' => 'required'
        ];

        $attr = [
            'role' => 'Role name',
            'permissions' => 'Permissions'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $role = Role::findById($id);
        $role->syncPermissions($request->permissions);

        return redirect()->back()->with('success', 'success add');
    }
}
