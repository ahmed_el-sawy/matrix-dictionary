<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Dictionary;
use App\Http\Controllers\Controller;
use App\Language;
use App\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    use AjaxData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dictionaries = Dictionary::all();
        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '!=', 0)->get();

        return view('dashboard.search/search', compact('dictionaries', 'languages', 'categories', 'sub_categories'));
    }

    /**
     * Words Results Return
     */

    public function create(){
        $dic = request()->get('dictionary');
        $s_lang = request()->get('source_lang');
        $t_lang = request()->get('target_lang');
        $category = request()->get('category');
        $sub_category = request()->get('sub_category');
        $search = request()->get('search');

        if($dic != null && $dic != 'all'){
            $dictionaries = Dictionary::where('id', $dic)->get();
        }else{
            $dictionaries = Dictionary::get();
        }

        $result = '';
        $results_count = 0;
        $dictionaries_count = 0;
        $categories = [];

        foreach($dictionaries as $dictionary){
            /**
             * Get words results from the dictionary
             */
            $results = Word::leftJoin('dictionaries', 'dictionaries.id', '=',  'words.dictionary')
                            ->leftJoin('words_media', 'words_media.word_id', '=',  'words.id')
                            ->whereRaw("(words.s_language LIKE '%$search%' OR words.t_language LIKE '%$search%')");
            /**
             * get words result category
             */
            $category_title = Category::find($dictionary->category);

            // More search filter options
            if($dic != null && $dic != 'all'){
                $results->where('dictionaries.id', '=', $dic);
            }

            if($s_lang != null){
                $results->where('dictionaries.source_lang', $s_lang);
            }

            if($t_lang != null){
                $results->where('dictionaries.target_lang', $t_lang);
            }

            if($category != null){
                $results->where('dictionaries.category', $category);
            }

            if($sub_category != null){
                $results->where('dictionaries.sub_category', $sub_category);
            }
            // $results;
            $AllResults = $results->where('words.dictionary', $dictionary->id)
                                ->select('words.*','dictionaries.category', DB::raw('count(words_media.id) AS media_count'))
                                ->groupBy('words.id')
                                ->get();

            //
            $results_count = $results_count + count($AllResults);
            //
            $rows = '';
            $x=0;
            if(count($AllResults) > 0){
                // add dictionaries counter
                $dictionaries_count = $dictionaries_count + 1;
                /**
                * push dictionary category to categories array
                */
                if(!in_array($dictionary->category, $categories)){
                    array_push($categories, $dictionary->category);
                }
                /**
                 * fetch results to table
                 */
                foreach($AllResults as $r){
                    $ex = $r->examples != NULL ? count(json_decode($r->examples)) : 0;
                    $x++;
                    $rows .= '
                        <tr>
                            <td style="cursor: pointer;">'.$x.'</td>
                            <td>'.$r->s_language.'</td>
                            <td>'.$r->t_language.'</td>
                            <td style="visibility: hidden;display: none;">'.$r->s_explain.'</td>
                            <td style="visibility: hidden;display: none;">'.$r->t_explain.'</td>
                            <td style="visibility: hidden;display: none;"><a href="admin/examples/'.$r->id.'" class="show-more">Show '.$ex.' Examples</a></td>
                            <td style="visibility: hidden;display: none;"><a href="admin/media/'.$r->id.'" class="show-more">Show '.$r->media_count.' Media Content</a></td>
                        </tr>
                    ';
                }

                // results table
                $result .= '<div class="kt-portlet__body">
                                <p class="text-left"><h4>Dictionary: <small>'.$dictionary->title.'</small></h4><p>
                                <h4 class="text-center">'.$category_title->title.'</h4>
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable kt_table_1" role="grid">
                                    <thead>
                                        <tr>
                                            <th class="sm-col">#</th>
                                            <th>Source Language</th>
                                            <th>Target Language</th>
                                            <th style="visibility: hidden;display: none;">SourceExplain</th>
                                            <th style="visibility: hidden;display: none;">TargetExplain</th>
                                            <th style="visibility: hidden;display: none;">Examples</th>
                                            <th style="visibility: hidden;display: none;">Media</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        '.$rows.'
                                    </tbody>
                                </table>
                            </div>';
            }


        }

        // last categories count
        $categories = count($categories);
        // return view with results
        return view('dashboard.search/results', compact('result', 'categories', 'dictionaries', 'dictionaries_count', 'results_count'));
    }

}
