<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\ImageManagerStatic as Image;


use App\Slider;
use App\SearchSection;

class SliderController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::get();
        return view('dashboard.slider.index')->with(['sliders'=>$sliders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
        ],
        $messages = [
            'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Image'
        ]);

        $slider = new Slider;        
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/slider');
        $image->move($destinationPath, $imageName);
        $slider->image ='/uploads/slider/'.$imageName;
        $image = Image::make(public_path().$slider->image)->resize(1920, 700)->save(public_path().$slider->image);

        $slider->save();
        return redirect()->route('slider.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view('dashboard.slider.edit')->with(['slider'=>$slider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::findOrFail($id);
        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg'
            ],
            $messages = [
                'image.required' => 'Please Choose Image',
                'image.mimes' => 'Please Choose Image'
            ]);

            if (File::exists(public_path().$slider->image))
            {
                File::delete(public_path().$slider->image);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/slider');
            $image->move($destinationPath, $imageName);
            $slider->image = '/uploads/slider/'.$imageName;
            $image = Image::make(public_path().$slider->image)->resize(1920, 700)->save(public_path().$slider->image);
        }
        
        $slider->save();
        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);

        if (File::exists(public_path().$slider->image))
        {
            File::delete(public_path().$slider->image);
        }
        $slider->delete();
        return redirect()->route('slider.index');
    }

    public function slider_text ()
    {
        $search_section = SearchSection::where('position', 'slider_text')->first();
        if($search_section === NULL)
        {
            $search_section = new SearchSection;
            $search_section->position = 'slider_text';
            $search_section->save();
        }
        return view('dashboard.slider.text')->with(['search_section'=>$search_section]);
    }

    public function save_slider_text (Request $request)
    {
        $search_section = SearchSection::where('position', 'slider_text')->first();
        if($search_section === NULL)
        {
            $search_section = new SearchSection;
            $search_section->position = 'slider_text';
            $search_section->save();
        }
        $search_section->title = $request->title;        
        $search_section->text = $request->text;
        $search_section->save();
        
        return redirect()->back();
    }
}
