<?php
namespace App\Http\Controllers\Admin;

use App\Career;
use App\Country;
use App\Http\Controller\Admin\DT;
use App\Http\Controllers\Controller;
use App\Language;
use App\User;
use Spatie\Permission\Models\Role;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    use AjaxData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('su', '!=', 1)->orderBy('id', 'DESC')->get();

        return view('dashboard.users.index', ['users' => $users, 'page_name' => 'Users']);
    }

    /**
     * users ajax DT
     */

    public function users(){
        $users = User::where('su', '!=', 1)->get();
        return $this->dataResponse($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::get();
        $jobs = Career::get();
        $roles = Role::all();
        $languages = Language::all();

        return view('dashboard.users.create', ['countries' => $countries, 'jobs' => $jobs, 'page_name' => 'Add new user data', 'roles' => $roles, 'languages' => $languages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'mobile' => 'required|unique:users',
            'password' => 'required|confirmed',
            'photo' => 'mimes:jpeg,jpg,bmp,png',
        ];

        $attr = [
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'photo' => 'Personal photo'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $path = Storage::putFile('users', $file);
            $url = '/storage/app/'.$path;
            $request['avatar'] = $url;
        }

        $request['password'] = Hash::make($request->password);

        $user = User::create($request->except(['photo', '_token', '_method', 'password_confirmation', 'roles']));

        if($request->has('roles')){
            $user->assignRole($request->roles);
        }

        return redirect()->route('users.index')->with('success', 'Success add user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findOrFail($id);
        $countries = Country::get();
        $jobs = Career::get();
        $roles = Role::all();
        $languages = Language::all();

        return view('dashboard.users.edit',  ['countries' => $countries, 'data' => $data, 'jobs' => $jobs, 'page_name' => 'Edit user data', 'roles' => $roles, 'languages' => $languages]);
    }

    /**
     * change status
     */

     public function status($id){
        $data = User::findOrFail($id);
        $status = $data->status == 0 ? 1 : 0;

        $data->update(['status' => $status]);

        return back()->with('success', 'Success Update');
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $data = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'mobile' => 'required|unique:users,mobile,'.$id,
            'password' => 'confirmed',
            'photo' => 'mimes:jpeg,jpg,bmp,png',
        ];

        $attr = [
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'photo' => 'Personal photo'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $path = Storage::putFile('users', $file);
            $url = '/storage/app/'.$path;
            $request['avatar'] = $url;
        }

        if($request->has('password')){
            $request['password'] = Hash::make($request->password);
        }

        $user->update($request->except(['photo', '_token', '_method', 'password_confirmation', 'roles']));

        if($request->has('roles')){
            $user->assignRole($request->roles);
        }

        return redirect()->back()->with('success', 'Success Save');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->back()->with('success', 'Success delete user');
    }
}
