<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Dictionary;
use App\Dictionary_file;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\DictionariesImport;
use App\Language;
use App\Word;
use App\Words_media;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Facades\Datatables;


class WordsController extends Controller
{
    // use AjaxData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dic = request()->get('d');

        $data = Word::where('dictionary', $dic)->count();

        $files = Dictionary_file::where('dictionary', $dic)->get();
        $dictionary = Dictionary::findorfail($dic);
        return view('dashboard.words/index', ['data' => $data, 'files' => $files, 'dictionary'=>$dictionary]);
    }

     /**
     * Words ajax DT
     */

    public function words()
    {
        $dic = request()->get('d');

        $words = Word::leftJoin('words_media', 'words_media.word_id', '=', 'words.id')
                    ->where('dictionary', $dic)
                    ->orderBy('id', 'DESC')
                    ->select('words.*', DB::raw('count(words_media.id) AS mediaCounts'))
                    ->groupBy('words.id')
                    ->get();

        // dd($words);

        return Datatables()->of($words)->addColumn('action', function ($words) {
            return '
                <div class="dropdown dropup">
                    <a href="#" class="btn btn-hover-brand btn-icon btn-pill" data-toggle="dropdown">
                        <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="'.route('words.edit', $words->id).'"><i class="la la-edit"></i> Edit</a>
                        <a class="dropdown-item" href="admin/words/'.$words->id.'/delete"><i class="la la-trash"></i> Delete</a>
                    </div>
                </div>
                <form id="form-group" data-toggle="tooltip" title="Delete" action="'.route('words.destroy', $words->id).'" method="POST">
                    '.csrf_field().'
                    <input type="hidden" name="_method" value="DELETE">
                </form>
               ';
        })->make(true);
    }

    /**
     * Word Media return
     */

    public function media($id)
    {
        $media = Words_media::where('word_id', $id)->get();

        $img_types = ['png', 'jpeg', 'jpg', 'bmp'];

        $results = "
            <table class='table' width='100%'>
                <tr>
                    <th>#</th>
                    <th>Media</th>
                    <th>Media Type</th>
                </tr>
        ";

        $x=0;
        foreach($media as $md){
            $x++;
            if(in_array($md->media_type, $img_types)){
                $results .= "
                    <tr>
                        <td>".$x."</td>
                        <td><img src='".asset($md->media)."' class='thumbnail' style='height: 70px' /></td>
                        <td>".$md->media_type."</td>
                    </tr>
                ";
            }else{
                $results .= "
                    <tr>
                        <td>".$x."</td>
                        <td><a href='".asset($md->media)."' target='_blank'>".$md->media."</a></td>
                        <td>".$md->media_type."</td>
                    </tr>
                ";
            }

        }

        $results .= "</table>";

        echo $results;
    }

    /**
     * Word Examples return
     */

    public function examples($id)
    {
        $examples = Word::findOrFail($id)->examples;

        $results = "
            <table class='table' width='100%'>
                <tr>
                    <th>#</th>
                    <th>Example</th>
                </tr>
        ";
        if($examples != NULL){
            $x=0;
            foreach(json_decode($examples) as $ex){
                $x++;
                $results .= "
                    <tr>
                        <td>".$x."</td>
                        <td>".$ex."</td>
                    </tr>
                ";
            }
        }


        $results .= "</table>";

        echo $results;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '!=', 0)->get();
        $dictionaries = Dictionary::all();

        return view('dashboard.words/create', ['languages' => $languages, 'categories' => $categories, 'sub_categories' => $sub_categories, 
        'dictionaries' => $dictionaries]);
    }
    public function create_file($id)
    {
        $dictionary = Dictionary::findorfail($id);
        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '!=', 0)->get();
        $dictionaries = Dictionary::all();
        $page_name = "<a href='".url('admin/words?d='.$dictionary->id)."'>".$dictionary->title."</a> \ Add New File";
        return view('dashboard.words/create_file', ['languages' => $languages, 'categories' => $categories, 'sub_categories' => $sub_categories, 
        'dictionaries' => $dictionaries, 'dicty' => $dictionary, 'page_name'=>$page_name]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'dictionary' => 'required',
            'file' => 'required|mimes:xlsx,xls,xlsb,xlsm|max:204800',
            'source_lang' => 'required',
            'target_lang' => 'required',
            'category' => 'required',
        ];

        $attr = [
            'dictionary' => 'Dictionary',
            'file' => 'Excel File',
            'source_lang' => 'Source Language',
            'target_lang' => 'Target Language',
            'category' => 'Category'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        /**\
         * Save Dictionary File
         */
        $file = $request->file('file');
        $dictionary_file = new Dictionary_file();
        $dictionary_file->dictionary = $request->dictionary;
        $dictionary_file->source = $request->source;
        $dictionary_file->copy_right = $request->copy_right;
        $dictionary_file->author = $request->author;
        $dictionary_file->source_lang = $request->source_lang;
        $dictionary_file->target_lang = $request->target_lang;
        $dictionary_file->category = $request->category;
        $dictionary_file->sub_category = $request->sub_category;
        $dictionary_file->status = $request->status;
        $dictionary_file->save();

        /**\
        * Save Dictionary Words
        */

        $dictionary = Dictionary::findOrFail($request->dictionary);

        $data = Excel::import(new DictionariesImport($request->dictionary, $dictionary_file->id), $file);

        if($request->hasFile('file')){
            $ufile = $request->file('file');
            $fileName = time().'.'.$ufile->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/dictionaries/');
            $ufile->move($destinationPath, $fileName);
            $dictionary_file->file = '/uploads/dictionaries/'.$fileName;
            $dictionary_file->file_name = $ufile->GetClientOriginalName();
            $dictionary_file->save();
        }

        $words = Word::where('dictionary', $request->dictionary)->count();

        $dictionary->words_num = $words;
        $dictionary->save();

        Word::where('file', $dictionary_file->id)->update(['status'=>$dictionary_file->status]);
        
        return redirect('admin/words?d='.$dictionary->id)->with('success', 'Success add dictionary words');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $word = Word::findOrFail($id);
        $media = Words_media::where('word_id', $id)->get();

        return view('dashboard.words/edit', ['data' => $word, 'media' => $media]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $word = Word::findOrFail($id);

        $data = [
            'files.*' => 'mimes:png,jpg,jpeg,bmp,ppt,pptx,doc,docx,pdf,xlsx,xls,xlsb,xlsm|max:204800',
            's_language' => 'required',
            't_language' => 'required'
        ];

        $attr = [
            'files' => 'Media File',
            'source_lang' => 'Source Language',
            'target_lang' => 'Target Language'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        if($request->has('old_files')){
            Words_media::where('word_id' , $id)->where('media_type', '!=', 'url')->whereNotIn('id', $request->old_files)->delete();
        }
        if($request->hasFile('files')){
            foreach($request->file('files') as $file){
                $path = Storage::putFile('media', $file);
                $url = '/storage/app/'.$path;
                $media = new Words_media();
                $media->word_id = $id;
                $media->media = $url;
                $media->media_type = $file->getClientOriginalExtension();
                $media->save();
            }
        }

        if($request->has('urls')){
            Words_media::where(['word_id' => $id, 'media_type' => 'url'])->delete();
            foreach($request->input('urls') as $url){
                if($url != NULL){
                    $media = new Words_media();
                    $media->word_id = $id;
                    $media->media = $url;
                    $media->media_type = 'url';
                    $media->save();
                }
            }
        }

        $examples = [];

        if($request->has('examples')){
            foreach($request->input('examples') as $example){
                array_push($examples, $example);
            }
        }

        $request['examples'] = json_encode($examples);

        $word->update($request->except(['files', '_token', '_method', 'urls', 'old_files']));

        return redirect()->route('words.index', 'd='.$word->dictionary)->with('success', 'Success edit word data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $word = Word::findOrFail($id);
        $word->delete();
        return redirect()->route('words.index', 'd='.$word->dictionary)->with('success', 'Success remove word data');
    }
}
