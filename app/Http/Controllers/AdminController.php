<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;

class AdminController extends Controller
{
    public function loginform ()
    {
        return redirect('/');
    }

    public function login(Request $request)
    {
        if($request->isMethod('post'))
        {
            $data = $request->input();
            if(Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'status' => true]))
            {
                if(auth()->user()->type == 0)
                {
                    return redirect('admin/dashboard');
                }
                else
                {
                    Auth::logout();
                    return redirect('admin')->withErrors(['error'=>'ليس لديك صلاحيات دخول']);
                }
            }
            else
            {
                return redirect('admin')->withErrors(['error'=>'فشل الدخول بيانات خاطئة']);
            }
        }


        return view('dashboard.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/admin');
    }
}
