<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;
use App\User;

class BlogController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy('id', 'desc')->paginate(5);
        $latarts = Article::orderBy('id', 'desc')->paginate(3);
        return view('blog', compact(['articles', 'latarts']));
    }

    public function user_articles($id)
    {
        $user = User::findorfail($id);
        $articles = Article::where('user', $id)->orderBy('id', 'desc')->paginate(5);
        $latarts = Article::orderBy('id', 'desc')->paginate(3);
        return view('blog', compact(['articles', 'latarts']));
    }
    
    public function article($id)
    {
        $article = Article::findorfail($id);
        $latarts = Article::orderBy('id', 'desc')->paginate(3);
        $next_article = Article::where('id', '>', $id)->orderBy('id', 'asc')->first();
        $prev_article = Article::where('id', '<', $id)->orderBy('id', 'desc')->first();
        $related_posts = Article::where('id', '!=', $id)->inRandomOrder()->limit(4)->get();
        
        return view('article', compact(['article', 'latarts', 'next_article', 'prev_article', 'related_posts']));
    }
}