<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Slider;
use App\SearchSection;
use App\Category;
use App\Word;
use App\Dictionary;
use App\Subscriber;
use App\PageSection;
use App\AboutSection;
use App\SearchWord;
use App\WorkProcess;
use App\TodayWord;
use Validator;
use App\Article;
use App\WordArticle;
use App\Language;
use App\MyWord;
use LanguageDetector;

class HomeController extends Controller
{
    public $search_filter;
    public $dictionaries_filter;
    public $dictionaries;
    public $query;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sliders = Slider::all();
        $search_section = SearchSection::where('position', 'slider_text')->first();
        $banner = PageSection::where('position', 'home_banner')->first();

        $main_cats = Category::where('sub_from', '=', 0)->orderBy('title')->get();
        $cats = Category::withCount('dictionaries')->where('sub_from', '!=', 0)->orderBy('title')->get();

        $cats_section = PageSection::where('position', 'home_categories_section')->first();
        $about_section = PageSection::where('position', 'home_about_us')->first();
        $about_sections = AboutSection::get();
        $search_words_sections = PageSection::where('position', 'search_words')->first();
        $dictionaries = Dictionary::count();
        $languages_data = Language::get();

        $work_process_section = PageSection::where('position', 'work_process')->first();

        $processs = WorkProcess::get();
        $languages = Language::count();
        $this->dictionaries = Dictionary::count();

        $words_counter = Word::count();
        $words = SearchWord::orderBy('counter', 'asc')->take(10)->get();
        
        $cats_counter = Category::where('sub_from', '!=', 0)->count();

        return view('home', compact('sliders', 'main_cats', 'search_section', 'cats', 'banner', 'cats_section', 'about_section', 'about_sections',
        'search_words_sections', 'words', 'work_process_section', 'languages_data', 'processs', 'languages', 'dictionaries', 'words_counter', 'cats_counter'));
    }

    public function contact()
    {
        return view('contact');
    }

    public function send_contact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ],
        [
            'name.required'=>'Please Enter Your Name',
            'email.required'=>'Please Enter Your Email',
            'email.email'=>'Please Enter Valid Email Address',
            'phone.required'=>'Please Enter Your Phone Number',
            'subject.required'=>'Please Enter Your Subject',
            'message.required'=>'Please Enter Your Message',
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        $to = "info@matrixclouds.com";

        $subject = $request->input('subject');
        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $smsg = $request->input('message');

        $message = '<html><body>';
        $message .= "<p><b>Name : </b>".$name."</p><p><b>E-mail Address : </b>".$email."</p><p><b>Phone : </b>".$phone."</p>".$smsg;
        $message .= '</body></html>';

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';
        $headers[] = 'To: Matrix Dictionary <'.$to.'>';
        $headers[] = 'From: '.$name.' <'.$email.'>';

        @mail($to, $subject, $message, implode("\r\n", $headers));

        return response()->json(['success' => true, 'message'=>'Your Message Sent Successfully']);

    }

    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:subscribers'
        ],
        [
            'email.required'=>'Please Enter Your Email',
            'email.email'=>'Please Enter Valid Email Address',
            'email.unique'=>'This Email Address Is Alreadu Subscribed'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        $s = new Subscriber;
        $s->email = $request->email;
        $s->save();
        return response()->json(['success' => true, 'message'=>'Subscribed Successfully']);

    }

    public function search(Request $request)
    {
        if($request->has('to'))
        {
            $to = $request->to;
        }
        else
        {
            $to = 0;
        }        
        if($request->has('from'))
        {
            $from = $request->from;
        }
        else
        {
            $from = 0;
        }

        if($from == 0 && $to == 0)
        {
            $dictionaries = Dictionary::pluck('id')->toArray();
        }
        else if($from > 0 && $to == 0)
        {
            $dictionaries = Dictionary::where('source_lang', $from)->orWhere('target_lang', $from)->pluck('id')->toArray();
        }
        else if($from == 0 && $to > 0)
        {
            $dictionaries = Dictionary::where('target_lang', $to)->orWhere('source_lang', $to)->pluck('id')->toArray();
        }
        else if($from > 0 && $to > 0)
        {
            $dictionaries = Dictionary::where(function ($query) use ($from, $to) {
                $query->where('source_lang', $from)->where('target_lang', $to);
            })->orWhere(function ($query) use ($from, $to) {
                $query->where('target_lang', $from)->where('source_lang', $to);
            })->
            pluck('id')->toArray();
        }

        $search_text_language = '';
        $this->search_filter = NULL;
        $languages = Language::all();

        if($request->has('cat'))
        {
            if(in_array("All", $request->cat))
            {
                $cats_filter = array();
            }
            else
            {
                $cats_filter = array_map('htmlentities', $request->get('cat'));                
            }

        }
        else
        {
            $cats_filter = array();
        }

        $sort_filter = 's_language';
        if($request->has('sort_by'))
        {
            $sort_filter = htmlentities($request->get('sort_by'));
            if(($sort_filter == 'source'))
            {
                $sort_filter = 's_language';
            }
            else if(($sort_filter == 'target'))
            {
                $sort_filter = 't_language';
            }
        }
        else
        {
            $sort_filter = 's_language';
        }

        $languages_filter = array();
        if($request->has('language'))
        {
            $languages_filter = array_map('htmlentities', $request->get('language'));
        }
        else
        {
            $languages_filter = array();
        }

        

        if(isset($_GET['page']))
        {
            $curent_page = (int) filter_input(INPUT_GET, 'page', FILTER_SANITIZE_SPECIAL_CHARS);
        }
        else
        {
            $curent_page = 1;
        }

        if($request->has('search'))
        {
            $search = html_entity_decode($request->get('search'));
            $search = htmlentities($search);
            $search = str_replace('&lrm;', '', $search);
            $search = addslashes ($search);
            if($search != '')
            {
                $this->search_filter = $search;
                $detector = new LanguageDetector\LanguageDetector();
                $detector->getSupportedLanguages();

                $search_text_language = $detector->evaluate($this->search_filter)->getLanguage();

                $checker = SearchWord::where('word', $this->search_filter)->first();
                if($checker === NULL)
                {
                    $checker = new SearchWord;
                    $checker->word = $this->search_filter;
                    $checker->save();
                }
                $checker->counter = $checker->counter + 1;
                $checker->save();

                // Search
                if(count($cats_filter) > 0 && count($languages_filter) > 0)
                {
                    $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                    ->join('categories', 'dictionaries.category', '=', 'categories.id')
                    ->where('dictionaries.status', 1)->where('words.status', 1)->whereIn('categories.id', $cats_filter)
                    ->whereIn('dictionaries.id', $dictionaries)
                    ->where(function ($query) use ($search) {
                        $query->where('words.s_language', 'LIKE', '% $search %')
                            ->orWhere('words.t_language', 'LIKE', '% $search %')
                            ->orWhere('words.s_language', 'LIKE', '$search %')
                            ->orWhere('words.t_language', 'LIKE', '$search %')
                            ->orWhere('words.s_language', 'LIKE', '% $search')
                            ->orWhere('words.t_language', 'LIKE', '% $search')
                            ->orWhere('words.s_language', '$search')
                            ->orWhere('words.t_language', '$search');
                    })
                    ->with(['dictionary_info'])->orderBy('words.'.$sort_filter, 'asc')->paginate(10);
                }
                else if(count($cats_filter) == 0 && count($languages_filter) > 0)
                {
                    $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                    ->join('categories', 'dictionaries.category', '=', 'categories.id')
                    ->where('dictionaries.status', 1)->where('words.status', 1)
                    ->whereIn('dictionaries.id', $dictionaries)
                    ->where(function ($query) use ($search) {
                        $query->where('words.s_language', 'LIKE', '% '.$search.' %')
                            ->orWhere('words.t_language', 'LIKE', '% '.$search.' %')
                            ->orWhere('words.s_language', 'LIKE', $search.' %')
                            ->orWhere('words.t_language', 'LIKE', $search.' %')
                            ->orWhere('words.s_language', 'LIKE', '% '.$search)
                            ->orWhere('words.t_language', 'LIKE', '% '.$search)
                            ->orWhere('words.s_language', $search)
                            ->orWhere('words.t_language', $search);
                    })->with(['dictionary_info'])->orderBy('words.'.$sort_filter, 'asc')->paginate(10);
                }
                else if(count($cats_filter) > 0 && count($languages_filter) == 0)
                {
                    $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                    ->join('categories', 'dictionaries.category', '=', 'categories.id')
                    ->where('dictionaries.status', 1)->where('words.status', 1)
                    ->whereIn('categories.id', $cats_filter)->whereIn('dictionaries.id', $dictionaries)
                    ->where(function ($query) use ($search) {
                        $query->where('words.s_language', 'LIKE', '% '.$search.' %')
                            ->orWhere('words.t_language', 'LIKE', '% '.$search.' %')
                            ->orWhere('words.s_language', 'LIKE', $search.' %')
                            ->orWhere('words.t_language', 'LIKE', $search.' %')
                            ->orWhere('words.s_language', 'LIKE', '% '.$search)
                            ->orWhere('words.t_language', 'LIKE', '% '.$search)
                            ->orWhere('words.s_language', $search)
                            ->orWhere('words.t_language', $search);
                    })
                    ->with(['dictionary_info'])->orderBy('words.'.$sort_filter, 'asc')->paginate(10);
                }
                else if(count($cats_filter) == 0 && count($languages_filter) == 0)
                {
                    $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                    ->join('categories', 'dictionaries.category', '=', 'categories.id')
                    ->where('dictionaries.status', 1)->where('words.status', 1)
                    ->whereIn('dictionaries.id', $dictionaries)
                    ->where(function ($query) use ($search) {
                        $query->where('words.s_language', 'LIKE', '% '.$search.' %')
                            ->orWhere('words.t_language', 'LIKE', '% '.$search.' %')
                            ->orWhere('words.s_language', 'LIKE', $search.' %')
                            ->orWhere('words.t_language', 'LIKE', $search.' %')
                            ->orWhere('words.s_language', 'LIKE', '% '.$search)
                            ->orWhere('words.t_language', 'LIKE', '% '.$search)
                            ->orWhere('words.s_language', $search)
                            ->orWhere('words.t_language', $search);
                    })->with(['dictionary_info'])->orderBy('words.'.$sort_filter, 'asc')->paginate(10);
                }
            }
            else
            {
                if(count($cats_filter) > 0 && count($languages_filter) > 0)
                {
                    $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                    ->join('categories', 'dictionaries.category', '=', 'categories.id')
                    ->where('dictionaries.status', 1)->where('words.status', 1)->whereIn('categories.id', $cats_filter)
                    ->whereIn('dictionaries.id', $dictionaries)->with(['dictionary_info'])->
                    orderBy('words.'.$sort_filter, 'asc')->paginate(10);
                }
                else if(count($cats_filter) == 0 && count($languages_filter) > 0)
                {
                    $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                    ->join('categories', 'dictionaries.category', '=', 'categories.id')
                    ->where('dictionaries.status', 1)->where('words.status', 1)
                    ->whereIn('dictionaries.id', $dictionaries)->with(['dictionary_info'])->
                    orderBy('words.'.$sort_filter, 'asc')->paginate(10);
                }
                else if(count($cats_filter) > 0 && count($languages_filter) == 0)
                {
                    $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                    ->join('categories', 'dictionaries.category', '=', 'categories.id')
                    ->where('dictionaries.status', 1)->where('words.status', 1)->whereIn('categories.id', $cats_filter)
                    ->whereIn('dictionaries.id', $dictionaries)
                    ->with(['dictionary_info'])->
                    orderBy('words.'.$sort_filter, 'asc')->paginate(10);
                }
                else if(count($cats_filter) == 0 && count($languages_filter) == 0)
                {
                    $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                    ->join('categories', 'dictionaries.category', '=', 'categories.id')
                    ->where('dictionaries.status', 1)->where('words.status', 1)
                    ->whereIn('dictionaries.id', $dictionaries)->
                    with(['dictionary_info'])->orderBy('words.'.$sort_filter, 'asc')->paginate(10);
                }
            }
        }
        else
        {
            if(count($cats_filter) > 0 && count($languages_filter) > 0)
            {
                $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                ->join('categories', 'dictionaries.category', '=', 'categories.id')
                ->where('dictionaries.status', 1)->where('words.status', 1)->whereIn('categories.id', $cats_filter)
                ->where(function ($query) use ($languages_filter) {
                    $query->whereIn('dictionaries.source_lang', $languages_filter)->orWhereIn('dictionaries.target_lang', $languages_filter);
                })->with(['dictionary_info'])->
                orderBy('words.'.$sort_filter, 'asc')->paginate(10);
            }
            else if(count($cats_filter) == 0 && count($languages_filter) > 0)
            {
                $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                ->join('categories', 'dictionaries.category', '=', 'categories.id')
                ->where('dictionaries.status', 1)->where('words.status', 1)
                ->where(function ($query) use ($languages_filter) {
                    $query->whereIn('dictionaries.source_lang', $languages_filter)->orWhereIn('dictionaries.target_lang', $languages_filter);
                })->with(['dictionary_info'])->
                orderBy('words.'.$sort_filter, 'asc')->paginate(10);
            }
            else if(count($cats_filter) > 0 && count($languages_filter) == 0)
            {
                $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                ->join('categories', 'dictionaries.category', '=', 'categories.id')
                ->where('dictionaries.status', 1)->where('words.status', 1)->whereIn('categories.id', $cats_filter)
                ->with(['dictionary_info'])->
                orderBy('words.'.$sort_filter, 'asc')->paginate(10);
            }
            else if(count($cats_filter) == 0 && count($languages_filter) == 0)
            {
                $words = $words = Word::select('words.*')->join('dictionaries', 'words.dictionary', '=', 'dictionaries.id')
                ->join('categories', 'dictionaries.category', '=', 'categories.id')
                ->where('dictionaries.status', 1)->where('words.status', 1)->
                with(['dictionary_info'])->orderBy('words.'.$sort_filter, 'asc')->paginate(10);
            }
        }

        // $all_words_count = $words->total();
        $all_words_count = 0;
        $cats = Category::where('sub_from', 0)->orderBy('title')->get();


        $chtd = TodayWord::where('word_date', date('Y-m-d'))->first();
        if($chtd === NULL)
        {
            $word_rand = Word::where('status', 1)->inRandomorder()->first();
            $chtd = new TodayWord;
            $chtd->word_date = date('Y-m-d');
            $chtd->word = $word_rand->id;
            $chtd->save();
        }
        else
        {
            $word_rand = Word::where('status', 1)->where('id', $chtd->word)->first();
            if($word_rand === NULL)
            {
                $word_rand = Word::where('status', 1)->inRandomorder()->first();
                $chtd = TodayWord::where('word_date', date('Y-m-d'))->first();
                $chtd->word = $word_rand->id;
                $chtd->save();
            }
        }

        $this->search_filter = stripslashes($search);

        $search_filter = $this->search_filter;

        $my_words = array();
        if(Auth::check())
        {
            $my_words = MyWord::where('user',Auth::user()->id)->pluck('word');
        }
        $all_dictionaries = array();
        $dictionaries_filter = array();
        $languages_data = Language::get();

        return view('dictionary', compact('all_words_count', 'words', 'curent_page', 'cats', 'search_filter', 
        'all_dictionaries', 'cats_filter', 'dictionaries_filter', 'word_rand', 'sort_filter', 'languages', 
        'languages_filter', 'search_text_language', 'my_words', 'languages_data', 'from', 'to'));
    }
}
