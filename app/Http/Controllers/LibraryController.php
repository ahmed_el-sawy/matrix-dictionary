<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Book;
use App\Category;

class LibraryController extends Controller
{
    public function index(Request $request)
    {
        $search_filter = NULL;
        if($request->has('cat'))
        {
            $cats_filter = array_map('htmlentities', $request->get('cat'));
        }
        else
        {
            $cats_filter = array();
        }
        if($request->has('search'))
        {
            $search = htmlentities($request->get('search'));
            $search_filter = $search;
            $books = Book::where('status', 1)->where('privacy', 0)->where('title', 'LIKE', '%'.$search.'%');
            $all_books = Book::where('status', 1)->where('privacy', 0)->where('title', 'LIKE', '%'.$search.'%');            
            if(count($cats_filter) > 0 && $cats_filter[0] != 'All')
            {
                $books = $books->whereIn('category', $cats_filter);
                $all_books = $all_books->whereIn('category', $cats_filter);         
            }
            $books = $books->orderBy('title', 'asc')->paginate('30');
            $all_books = $all_books->get()->count();            
        }
        elseif(count($cats_filter) > 0)
        {
            $words = Book::where('status', 1)->where('privacy', 0)->whereIn('category', $cats_filter)->orderBy('title', 'asc')->paginate('30');
            $all_books = Book::where('status', 1)->where('privacy', 0)->whereIn('category', $cats_filter)->orderBy('title', 'asc')->get()->count();
        }
        else
        {
            $books = Book::where('status', 1)->where('privacy', 0)->orderBy('title', 'asc')->paginate('30');
            $all_books = Book::where('status', 1)->where('privacy', 0)->orderBy('title', 'asc')->get()->count();
        }

        $curent_page = 1;
        $cats = Category::where('sub_from', 0)->orderBy('title')->get();
        return view('library', compact(['books', 'cats', 'search_filter', 'cats_filter', 'curent_page', 'all_books']));
    }

    public function show($id)
    {
        $book = Book::findorfail($id);
        if($book->privacy == 1 && (!Auth::check() || Auth::user()->id != $book->user)) {abort(404);}
        $search_filter = NULL;
        $cats_filter = array();
        $cats = Category::where('sub_from', 0)->orderBy('title')->get();
        return view('book', compact(['book', 'search_filter', 'cats_filter', 'cats']));
    }
}
