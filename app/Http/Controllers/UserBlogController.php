<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

use Validator;

use App\Article;
use App\Comment;
use App\WordArticle;
use App\Word;

class UserBlogController extends Controller
{
    public function index(Request $request)
    {
        $active_page = "My-Blog";
        $articles = Article::where('user', Auth::user()->id);
        if($request->has('search'))
        {
            $search = htmlentities($request->get('search'));
            if($search != '')
            {
                $articles = $articles->where('text', 'LIKE', '%'.$search.'%');
            }
        }
        $articles = $articles->orderBy('id', 'desc')->paginate(10);
        return view('my_blog', compact('active_page', 'articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $with_word = false;
        return view('new_article', compact(['with_word']));
    }
    public function create_wword($id)
    {
        $word = Word::findorfail($id);
        $with_word = true;
        return view('new_article', compact(['with_word', 'id', 'word']));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'title' => 'required',
            'text' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg,webp',
        ],
        [
            'title.required' => 'Please Enter Title',
            'text.required' => 'Please Enter Description',
            'image.required' => 'Please Choose Image',
            'image.mimes' => 'Please Choose Valid Image File',
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }
        $section = new Article;
        $section->title = $request->title;
        $section->text  = $request->text;
        $section->user  = Auth::user()->id;
        
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/articles');
        $image->move($destinationPath, $imageName);
        $section->image ='/uploads/articles/'.$imageName;
        $section->save();

        if($request->has('word'))
        {
            $mm = new WordArticle;
            $mm->word = $request->word;
            $mm->article = $section->id;
            $mm->save();
        }
        
        return response()->json(['success' => true,  'data'=> url('My-Blog')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::findorfail($id);
        if($article->user != Auth::user()->id) {return abort(404);}
        return view('edit_article', compact('article'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
			'title' => 'required',
            'text' => 'required',
        ],
        [
            'title.required' => 'Please Enter Title',
            'text.required' => 'Please Enter Description',
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        $section = Article::findorfail($id);
        if($section->user != Auth::user()->id) {return abort(404);}
        $section->title = $request->title;
        $section->text  = $request->text;
        $section->user  = Auth::user()->id;
        
        if($request->hasFile('image'))
        {
            $validator = Validator::make($request->all(), [
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg,webp',
            ],
            [
                'image.required' => 'Please Choose Image',
                'image.mimes' => 'Please Choose Valid Image File',
            ]);
            if ($validator->fails())
            {
                return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
            }
            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/articles');
            $image->move($destinationPath, $imageName);
            $section->image ='/uploads/articles/'.$imageName;
    
        }
        $section->save();
        
        return response()->json(['success' => true,  'data'=> url('My-Blog')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findorfail($id);
        if($article->user != Auth::user()->id) {return abort(404);}
        $article->delete();

        return redirect('My-Blog');
    }
    
    public function article_comment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required'
        ],
        [
            'comment.required'=>'Please Enter Your Comment'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }
        $comment = new Comment;
        $comment->user = Auth::user()->id;
        $comment->article = $request->article;
        $comment->comment = $request->comment;
        $comment->save();
        return response()->json(['success'=>true, 'message'=>'Successfully']);
    }
}
