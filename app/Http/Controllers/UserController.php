<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Validator;
use Illuminate\Support\Str;

use App\Country;
use App\Career;
use App\Language;
use App\User;
use App\MyWord;
use App\MyBook;
use App\Book;
use App\Word;

class UserController extends Controller
{
    public function favourite_word ($id)
    {
        $word = Word::findorfail($id);
        $x = MyWord::where('user', Auth::user()->id)->where('word', $word->id)->first();
        if($x === NULL)
        {
            $x = new MyWord;
            $x->user = Auth::user()->id;
            $x->word = $word->id;
            $x->save();
        }
        else
        {
            $x->delete();
        }
        return redirect()->back();
    }

    public function favourite_book ($id, Request $request)
    {
        $book = Book::findorfail($id);
        $x = MyBook::where('user', Auth::user()->id)->where('book', $book->id)->first();
        if($x === NULL)
        {
            $x = new MyBook;
            $x->user = Auth::user()->id;
            $x->book = $book->id;
            $x->save();
            echo '<i class="fas fa-heart"></i>';
        }
        else
        {
            $x->delete();
            echo '<i class="far fa-heart"></i>';
        }
    }

    public function register_form ()
    {
        $countries = Country::where('status', 1)->orderBy('country')->get();
        $jobs = Career::orderBy('name')->get();
        $languages = Language::orderBy('language')->get();

        return view ('register', compact('countries', 'jobs', 'languages'));
    }
    
    public function register (Request $request)
    {
        $data = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'mobile' => 'required|unique:users',
            'password' => 'required|confirmed',
            'terms'=>'required'
        ];

        $attr = [
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'terms' => 'our Terms and Conditions and Privacy Policy'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput();
        }

       
        $request['password'] = Hash::make($request->password);

        $user = User::create($request->except(['photo', '_token', '_method', 'password_confirmation', 'roles', 'terms']));

        $user->type = 1;
        $user->status = 0;
        $aas = md5(Str::random(32)."_".strtotime('now'));
        $user->remember_token = substr($aas, 0, 20);
        $user->save();

        $a = $user->email;

        $subject = $request->input('subject');
        $name = $request->input('name');
        $email = $request->input('email');
        $url = url('activate_account/'.$user->remember_token);
        $message = '<html><body>';
        $message .= "<p>Hi ".$name.", </p><p>Welcome At Matrix Dictionary</p><p>To Confirm Your Account Please <a href='".$url."'>Click Here</a></p>";
        $message .= '</body></html>';

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';
        $headers[] = 'From: Matrix Dictionary <info@mydictionary.app>';
        $headers[] = 'To: '.$name.' <'.$email.'>';

        @mail($a, $subject, $message, implode("\r\n", $headers));  

        return redirect()->back()->with('success', 'Your Account Created Successfully');
        
    }

    public function activate_account ($token)
    {
        $user = User::where('remember_token', $token)->first();
        if ($user === NULL)
        {
            return abort('404');
        }
        else
        {
            $user->status = 1;
            $user->remember_token = '';
            $user->save();
            return view('activate_account');
        }
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ],
        [
            'email.required'=>'Please Enter Your Email',
            'email.email'=>'Please Enter Valid Email Address',
            'password.required'=>'Please Enter Your password'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        if($request->isMethod('post'))
        {
            $data = $request->input();
            $user = User::where('email', $data['email'])->first();
            if($user === NULL)
            {
                return response()->json(['success'=>false, 'errors'=>'Error In Email Or Password']);
            }
            else if(!Hash::check($request->password, $user->password))
            {
                return response()->json(['success'=>false, 'errors'=>'Error In Email Or Password']);
            }
            else if($user->status == 0)
            {
                return response()->json(['success'=>false, 'errors'=>'Your Account Is Not Active']);
            }
            else
            {
                if(Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'status' => true]))
                {
                    return response()->json(['success'=>true, 'message'=>'Logged In Successfully']);
                }
                else
                {
                    return response()->json(['success'=>false, 'errors'=>'Error In Email Or Password']);
                }
            }

        }
        else
        {
            if(auth()->user())
            {
                return response()->json(['success'=>true, 'message'=>'Logged In Successfully']);
            }
            else
            {
                return response()->json(['success'=>false, 'errors'=>'Not Authorized To Log in']);
            }

        }

        return response()->json(['success'=>false, 'errors'=>'Not Authorized To Log in']);
    }

    public function profile()
    {
        $active_page = "Dashboard";
        $search_text_language = 'en';
        return view('profile', compact('active_page', 'search_text_language'));
    }

    
    public function change_information()
    {
        $active_page = "Change-Information";
        $countries = Country::where('status', 1)->orderBy('country')->get();
        $jobs = Career::orderBy('name')->get();
        $languages = Language::orderBy('language')->get();


        return view('change-information', compact('active_page', 'countries', 'jobs', 'languages'));
    }

    
    public function save_information(Request $request)
    {
        $id = Auth::user()->id;
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id.',id',
            'mobile' => 'required'
        ],
        [
            'name.required'=>'Please Enter Your Name',
            'email.required'=>'Please Enter Your Email',
            'email.email'=>'Please Enter Valid Email Address',
            'email.unique'=>'This Email Address Is Registered To Another User',
            'mobile.required'=>'Please Enter Your Mobile Number'
        ]);

        if($request->hasFile('image'))
        {
            $validatedData = $request->validate([
                'image' => 'mimes:jpeg,png,jpg,gif,svg,gif,webp'
            ],
            $messages = [
                'image.mimes' => 'Please Choose Image File'
            ]);

            if (File::exists(public_path().Auth::user()->avatar))
            {
                File::delete(public_path().Auth::user()->avatar);
            }

            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/users');
            $image->move($destinationPath, $imageName);
            Auth::user()->avatar = '/uploads/users/'.$imageName;
        }

        Auth::user()->name = $request->name;
        Auth::user()->email = $request->email;
        Auth::user()->mobile = $request->mobile;
        Auth::user()->country = $request->country;
        Auth::user()->job = $request->job;
        Auth::user()->language = $request->language;
        if($request->has('show_personal_info')) {Auth::user()->pinfo = 1;}
        else {Auth::user()->pinfo = 0;}
        Auth::user()->save();


        return redirect()->back()->with('success', 'Your Account Created Successfully');
    }

    
    public function change_password()
    {
        $active_page = "Change-Password";
        return view('change-password', compact('active_page'));
    }

    
    public function save_password(Request $request)
    {
        $data = [
            'password' => 'required|confirmed'
        ];
        $validator = Validator::make($request->all(), $data, [], []);

        if ($validator->fails()) 
        {
            return back()->withErrors($validator)->withInput();
        }

       
        Auth::user()->password = Hash::make($request->password);
        Auth::user()->save();

        return redirect()->back()->with('success', 'Your Password Saved Successfully');
    }
    
    public function logout ()
    {
        Auth::logout();
        return redirect('/');
    }
    
    
    public function preview_resume()
    {
        $user = User::findorfail(Auth::user()->id);
        return view('resumeview', compact('user'));
    }

    public function view_resume($id)
    {
        $user = User::findorfail($id);
        return view('resumeview', compact('user'));
    }
}
