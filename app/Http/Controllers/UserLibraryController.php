<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use App\Book;
use App\Language;
use App\Category;
use App\MyBook;
use Validator;

class UserLibraryController extends Controller
{
    public function index(Request $request)
    {
        $ids = array();
        
        $ixs = Book::where('user', Auth::user()->id)->get()->pluck('id');
        for ($i = 0; $i < count($ixs); $i++) {$ids[] = $ixs[$i];}
        
        $iba = MyBook::where('user', Auth::user()->id)->get()->pluck('book');
        for ($i = 0; $i < count($iba); $i++) {$ids[] = $iba[$i];}

        $data = Book::whereIn('id', $ids)->paginate(10);
        $active_page = "My-Library";
        $languages = Language::all();
        $categories = Category::where('sub_from', 0)->get();
        $sub_categories = Category::where('sub_from', '!=', 0)->get();
        return view('my_library', compact('active_page', 'languages', 'categories', 'sub_categories', 'data'));
    }

    public function save_book (Request $request)
    {
        $data = [
            'title' => 'required',
            'language' => 'required',
            'category' => 'required',
            'copyRight' => 'required',
            'author' => 'required',
            'book' => 'required|mimes:xlsx,xls,xlsb,xlsm,doc,docx,pdf',
        ];

        $attr = [
            'title' => 'Title',
            'language' => 'Source Language',
            'category' => 'Category',
            'book' => 'Book File',
            'copyRight' => 'copyRight',
            'author' => 'Author'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {

            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        $file = $request->file('book');

        if($request->hasFile('book')){
            $ufile = $request->file('book');
            $fileName = time().'.'.$ufile->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/books/');
            $ufile->move($destinationPath, $fileName);
            $request['file'] = '/uploads/books/'.$fileName;
        }

        $book = Book::create($request->except(['book', '_token', '_method']));
        $book->user = Auth::user()->id;
        $book->save();
        return response()->json(['success' => true, 'message'=>'Login Successfully']);
    }



    public function update_book(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        if($book->user != Auth::user()->id) {abort(404);}
        $data = [
            'title' => 'required',
            'language' => 'required',
            'category' => 'required',
            'copyRight' => 'required',
            'author' => 'required'
        ];

        $attr = [
            'title' => 'Title',
            'language' => 'Source Language',
            'category' => 'Category',
            'copyRight' => 'copyRight',
            'author' => 'Author'
        ];

        $validator = Validator::make($request->all(), $data, [], $attr);

        if ($validator->fails()) {

            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }


        if($request->hasFile('book')){

            $data = [
                'book' => 'required|mimes:xlsx,xls,xlsb,xlsm,doc,docx,pdf'
            ];   
            $attr = [
                'book' => 'Book File'
            ];
    
            $validator = Validator::make($request->all(), $data, [], $attr);
    
            if ($validator->fails()) {
    
                return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
            }

            $ufile = $request->file('book');
            $fileName = time().'.'.$ufile->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/books/');
            $ufile->move($destinationPath, $fileName);
            $request['file'] = '/uploads/books/'.$fileName;
        }

        $book->update($request->except(['book', '_token', '_method']));

        return response()->json(['success' => true, 'message'=>'Login Successfully']);
    }

    public function delete_book($id)
    {
        $book = Book::findOrFail($id);
        if($book->user != Auth::user()->id) {abort(404);}
        $book->delete();
        return redirect()->back();
    }

}
