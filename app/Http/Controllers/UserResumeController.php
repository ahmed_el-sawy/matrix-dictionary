<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Validator;

use App\CoverLetter;
use App\Education;
use App\Experince;
use App\Award;
use App\Skill;
use App\Language;
use App\LanguageLevel;
use App\UserLanguage;

class UserResumeController extends Controller
{
    public function index()
    {
        $active_page = "My-Resume";
        $cover_letter = CoverLetter::where('user', Auth::user()->id)->first();
        $languages = Language::orderBy('language')->get();
        $levels = LanguageLevel::get();
        return view('resume', compact('active_page', 'cover_letter', 'languages', 'levels'));
    }

    public function cover_letter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cover_letter' => 'required'
        ],
        [
            'cover_letter.required'=>'Please Enter Your Cover Letter'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        $cover_letter = CoverLetter::where('user', Auth::user()->id)->first();
        if($cover_letter === NULL)
        {
            $cover_letter = new CoverLetter;
            $cover_letter->user = Auth::user()->id;
            $cover_letter->letter = $request->input('cover_letter');
            $cover_letter->save();
        }
        else
        {
            $cover_letter->letter = $request->input('cover_letter');
            $cover_letter->save();
        }
        return response()->json(['success'=>true, 'message'=>'']);
    }

    public function new_education (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'institute' => 'required',
            'text' => 'required'
        ],
        [
            'title.required'=>'Please Enter Title',
            'institute.required'=>'Please Enter Institue Name',
            'text.required' => 'Please Enter Description'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        if(!$request->has('attend'))
        {
            if($request->to_year < $request->from_year)
            {
                return response()->json(['success' => false, 'errors'=>'End Year Must Be After First Year']);
            }
        }

        $edu = new Education;
        $edu->user = Auth::user()->id;
        $edu->title = $request->input('title');
        $edu->institute = $request->input('institute');
        $edu->text = $request->input('text');  
        $edu->from_year = $request->input('from_year');   
        if($request->has('to_year'))
        {
            $edu->to_year = $request->input('to_year');       
        }      
        else
        {
            $edu->to_year = 0;
        }     
        $edu->save();

        return response()->json(['success'=>true, 'message'=>'']);
    }

    public function update_education ($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'institute' => 'required',
            'text' => 'required'
        ],
        [
            'title.required'=>'Please Enter Title',
            'institute.required'=>'Please Enter Institue Name',
            'text.required' => 'Please Enter Description'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        if(!$request->has('attend'))
        {
            if($request->to_year < $request->from_year)
            {
                return response()->json(['success' => false, 'errors'=>'End Year Must Be After First Year']);
            }
        }

        $edu = Education::findorfail($id);
        if($edu->user != Auth::user()->id) {return response()->json(['success' => false, 'errors'=>'Error']);}
        $edu->title = $request->input('title');
        $edu->institute = $request->input('institute');
        $edu->text = $request->input('text');  
        $edu->from_year = $request->input('from_year');   
        if($request->has('to_year'))
        {
            $edu->to_year = $request->input('to_year');       
        }      
        else
        {
            $edu->to_year = 0;
        }     
        $edu->save();

        return response()->json(['success'=>true, 'message'=>'']);
    }

    public function delete_education ($id)
    {
        $edu = Education::findorfail($id);
        if($edu->user != Auth::user()->id) {return abort(404);}
        $edu->delete();
        return redirect()->back();
    }



    public function new_experince (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'company' => 'required',
            'text' => 'required',
            'from_year' => 'required'
        ],
        [
            'title.required'=>'Please Enter Title',
            'company.required'=>'Please Enter Company Name',
            'text.required' => 'Please Enter Description',
            'from_year.required' => 'Please Enter Start Date'
            ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        if(!$request->has('attend'))
        {
            if($request->to_year == '')
            {
                return response()->json(['success' => false, 'errors'=>'Please Enter End Date']);
            }
            else if($request->to_year < $request->from_year)
            {
                return response()->json(['success' => false, 'errors'=>'End Date Must Be After Start Date']);
            }
        }

        $edu = new Experince;
        $edu->user = Auth::user()->id;
        $edu->title = $request->input('title');
        $edu->company = $request->input('company');
        $edu->text = $request->input('text');  
        $edu->from_year = $request->input('from_year');   
        if($request->has('to_year'))
        {
            $edu->to_year = $request->input('to_year');       
        }      
        else
        {
            $edu->to_year = NULL;
        }     
        $edu->save();

        return response()->json(['success'=>true, 'message'=>'']);
    }

    public function update_experince ($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'company' => 'required',
            'text' => 'required',
            'from_year' => 'required'
        ],
        [
            'title.required'=>'Please Enter Title',
            'company.required'=>'Please Enter Company Name',
            'text.required' => 'Please Enter Description',
            'from_year.required' => 'Please Enter Start Date'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        if(!$request->has('attend'))
        {
            if($request->to_year == '')
            {
                return response()->json(['success' => false, 'errors'=>'Please Enter End Date']);
            }
            else if($request->to_year < $request->from_year)
            {
                return response()->json(['success' => false, 'errors'=>'End Date Must Be After Start Date']);
            }
        }

        $edu = Experince::findorfail($id);
        if($edu->user != Auth::user()->id) {return response()->json(['success' => false, 'errors'=>'Error']);}
        $edu->title = $request->input('title');
        $edu->company = $request->input('company');
        $edu->text = $request->input('text');  
        $edu->from_year = $request->input('from_year');   
        if($request->has('to_year'))
        {
            $edu->to_year = $request->input('to_year');       
        }      
        else
        {
            $edu->to_year = NULL;
        }     
        $edu->save();

        return response()->json(['success'=>true, 'message'=>'']);
    }

    public function delete_experince ($id)
    {
        $edu = Experince::findorfail($id);
        if($edu->user != Auth::user()->id) {return abort(404);}
        $edu->delete();
        return redirect()->back();
    }


    
    public function new_award (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'institute' => 'required',
            'text' => 'required',
            'start_date' => 'required',
            'end_date' => 'required|after:start_date'
        ],
        [
            'title.required'=>'Please Enter Title',
            'institute.required'=>'Please Enter Institute Name',
            'text.required' => 'Please Enter Description',
            'start_date.required' => 'Please Enter Start Date',
            'end_date.required' => 'Please Enter End Date',
            'end_date.after' => 'End Date Must Be After Start Date'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        $edu = new Award;
        $edu->user = Auth::user()->id;
        $edu->title = $request->input('title');
        $edu->institute = $request->input('institute');
        $edu->text = $request->input('text');  
        $edu->start_date = $request->input('start_date');   
        $edu->end_date = $request->input('end_date');   
        $edu->save();

        return response()->json(['success'=>true, 'message'=>'']);
    }

    public function update_award ($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'institute' => 'required',
            'text' => 'required',
            'start_date' => 'required',
            'end_date' => 'required|after:start_date'
        ],
        [
            'title.required'=>'Please Enter Title',
            'institute.required'=>'Please Enter Institute Name',
            'text.required' => 'Please Enter Description',
            'start_date.required' => 'Please Enter Start Date',
            'end_date.required' => 'Please Enter End Date',
            'end_date.after' => 'End Date Must Be After Start Date'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }

        $edu = Award::findorfail($id);
        if($edu->user != Auth::user()->id) {return response()->json(['success' => false, 'errors'=>'Error']);}
        
        $edu->user = Auth::user()->id;
        $edu->title = $request->input('title');
        $edu->institute = $request->input('institute');
        $edu->text = $request->input('text');  
        $edu->start_date = $request->input('start_date');   
        $edu->end_date = $request->input('end_date');   
        $edu->save();

        return response()->json(['success'=>true, 'message'=>'']);
    }

    public function delete_award ($id)
    {
        $edu = Award::findorfail($id);
        if($edu->user != Auth::user()->id) {return abort(404);}
        $edu->delete();
        return redirect()->back();
    }

    public function skills (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title.*' => 'required',
            'precentage.*' => 'required|numeric|min:0'
        ],
        [
            'title.*.required'=>'Please Enter Each Skill Title',
            'percentage.*.required'=>'Please Enter Each Skill Percentage',
            'percentage.*.numeric'=>'Skill Percentage Must Be Numeric',
            'percentage.*.min'=>'Skill Percentage Mi. Value Is 0',

        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }
        foreach (Auth::user()->skills as $skill)
        {
            $skill->delete();
        }
        for($i = 0; $i < count($request->input('title')); $i++)
        {
            $skill = new Skill;
            $skill->user = Auth::user()->id;
            $skill->percentage = $request->percentage[$i];
            $skill->title = $request->input('title')[$i];
            $skill->save();
        }
        return response()->json(['success'=>true, 'message'=>'']);
    }

    public function add_skill (Request $request)
    {
        return view('new_skill');
    }


    
    public function languages (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'language.*' => 'required',
            'language_level.*' => 'required'
        ],
        [
            'language.*.required'=>'Please Choose Each Langague',
            'language_level.*.required'=>'Please Choose Each Langauge Level',
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }
        if(count($request->language_selector) != count($request->language))
        {
            return response()->json(['success' => false, 'errors'=>'Please Choose Each Langague']);
        }
        if(count($request->language_selector) != count($request->language_level))
        {
            return response()->json(['success' => false, 'errors'=>'Please Choose Each Langague Level']);
        }


        foreach (Auth::user()->languages as $language)
        {
            $language->delete();
        }
        for($i = 0; $i < count($request->input('language_selector')); $i++)
        {
            $checker = UserLanguage::where('language', $request->language[$i])->where('user', Auth::user()->id)->first();
            if($checker === NULL)
            {
                $ul = new UserLanguage;
                $ul->user = Auth::user()->id;
                $ul->language = $request->language[$i];
                $ul->level = $request->language_level[$i];
                $ul->save();    
            }
        }
        return response()->json(['success'=>true, 'message'=>'']);
    }

    public function add_language (Request $request)
    {
        $languages = Language::orderBy('language')->get();
        $levels = LanguageLevel::get();
        return view('new_language', compact('languages', 'levels'));
    }

}
