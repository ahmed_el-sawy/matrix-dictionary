<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\WordComment;
use App\Word;

use App\WordSuggestion;
use App\WordSuggestionLike;

use Validator;

class WordController extends Controller
{
    public function word_comments ($id)
    {
        $word = Word::findorfail($id);
        return view('partials.comments', compact('word'));
    }

    public function save_word_comments (Request $request)
    {
        if(!Auth::check())
        {
            return response()->json(['success' => false, 'errors'=>'Please Login To Be Able To Publish Your Comment']);
        }

        $word = Word::findorfail($request->word);
        $validator = Validator::make($request->all(), [
            'comment' => 'required'
        ],
        [
            'comment.required'=>'Please Enter Your Comment'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }
        $comment = new WordComment;
        $comment->user = Auth::user()->id;
        $comment->word = $request->word;
        $comment->comment = $request->comment;
        $comment->save();


        $comments = view('partials.comments', compact('word'))->render();
        return response()->json(['success'=>true, 'message'=>'Successfully', 'data'=>$comments]);
    }



    public function word_suggestions ($id)
    {
        $word = Word::findorfail($id);
        return view('partials.suggestions', compact('word'));
    }

    public function save_word_suggestions (Request $request)
    {
        $word = Word::findorfail($request->word);
        $validator = Validator::make($request->all(), [
            'comment' => 'required'
        ],
        [
            'comment.required'=>'Please Enter Your Comment'
        ]);
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors'=>$validator->errors()->first()]);
        }
        $comment = new WordSuggestion;
        $comment->user = Auth::user()->id;
        $comment->word = $request->word;
        $comment->comment = $request->comment;
        $comment->save();


        $comments = view('partials.suggestions', compact('word'))->render();
        return response()->json(['success'=>true, 'message'=>'Successfully', 'data'=>$comments]);
    }

    public function word_articles ($id)
    {
        $word = Word::findorfail($id);
        return view('partials.articles', compact('word'));        
    }

    public function like_suggesations (Request $request, $id)
    {
        $word = Word::findorfail($request->word);
        $ch = WordSuggestionLike::where('user', Auth::user()->id)->where('suggest', $id)->first();
        if($ch === NULL)
        {
            $ch = new WordSuggestionLike;
            $ch->suggest = $id;
            $ch->user = Auth::user()->id;
            $ch->status ='like';
            $ch->save();
        }
        else
        {
            if ($ch->status == 'like')
            {
                $ch->delete();
            }
            else
            {
                $ch->delete();
                $ch = new WordSuggestionLike;
                $ch->suggest = $id;
                $ch->user = Auth::user()->id;
                $ch->status ='like';
                $ch->save();
            }
        }

        $comments = view('partials.suggestions', compact('word'))->render();
        return response()->json(['success'=>true, 'message'=>'Successfully', 'data'=>$comments]);
    }

    public function dislike_suggesations (Request $request, $id)
    {
        $word = Word::findorfail($request->word);
        $ch = WordSuggestionLike::where('user', Auth::user()->id)->where('suggest', $id)->first();
        if($ch === NULL)
        {
            $ch = new WordSuggestionLike;
            $ch->suggest = $id;
            $ch->user = Auth::user()->id;
            $ch->status ='dislike';
            $ch->save();
        }
        else
        {
            if ($ch->status == 'dislike')
            {
                $ch->delete();
            }
            else
            {
                $ch->delete();
                $ch = new WordSuggestionLike;
                $ch->suggest = $id;
                $ch->user = Auth::user()->id;
                $ch->status ='dislike';
                $ch->save();
            }
        }

        $comments = view('partials.suggestions', compact('word'))->render();
        return response()->json(['success'=>true, 'message'=>'Successfully', 'data'=>$comments]);
    }
}
