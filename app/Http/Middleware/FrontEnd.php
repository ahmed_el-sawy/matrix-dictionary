<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use App\Article;
use App\Book;

class FrontEnd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $footer_articles = Article::orderBy('id', 'desc')->limit(6)->get();
        $footer_books = Book::orderBy('id', 'desc')->limit(6)->get();
        
        View::share('footer_books',$footer_books);
        View::share('footer_articles',$footer_articles);
        
        return $next($request);
    }
}
