<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

class DictionaryResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title'=> $this->title,
            'about'=> $this->about,
            'source_lang' => DB::table('languages')->where('id', $this->source_lang)->first()->language,
            'target_lang' => DB::table('languages')->where('id', $this->target_lang)->first()->language,
            'words_num' => $this->words_num,
            'category' => DB::table('categories')->where('id', $this->category)->first()->title,
            'sub_category' => $this->sub_category != NULL ? DB::table('categories')->where('id', $this->sub_category)->first()->title : NULL,
            'status' => $this->status,
            'type' => $this->type,
        ];
    }
}
