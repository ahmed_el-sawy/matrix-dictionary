<?php

namespace App\Imports;

use App\Word;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class DictionariesImport implements ToModel
{
    private $dictionary;
    private $id;
    
    public function __construct($dictionary, $id)
    {
        $this->dictionary = $dictionary;
        $this->id = $id;
    }
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        // Store data
        try {
            if($row[1] != '' && $row[3] != '')
            {
                $data = new Word();
                $data->dictionary =  $this->dictionary;
                $data->file = $this->id;
                $data->s_language = $row[1];
                $data->t_language = $row[3];
                $data->s_explain = isset($row[4]) ? $row[4] : NULL;
                $data->t_explain = isset($row[5]) ? $row[5] : NULL;
                $data->save();
            }
        }catch (\Exception $e) {
            return;
        }
    }
}
