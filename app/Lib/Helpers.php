<?php

use App\Dictionary;
use App\Word;
use App\MyWord;
use App\WordSuggestionLike;
use App\MyBook;

if(!function_exists('word_cat_icon'))
{
    function word_cat_icon ($search)
    {
        $search = addslashes($search);
        $icon = '<i class="flaticon-account"></i>';
        $word = Word::where('status', 1)->whereRaw("(words.s_language LIKE '% $search %' OR 
            words.t_language LIKE '% $search %' OR 
            words.s_language LIKE '$search %' OR 
            words.t_language LIKE '$search %' OR
            words.s_language LIKE '% $search' OR 
            words.t_language LIKE '% $search' OR
            words.s_language LIKE '$search' OR 
            words.t_language LIKE '$search'
        )")->first();
        if($word !== NULL)
        {
            $icona = $word->dictionary_info->cat_info->icon;
            if($icona != '')
            {
                $icon = "<img src='".asset($icona)."' />";
            }
        }
        return $icon;
    }
}

if(!function_exists('remove_word_special_chars'))
{
    function remove_word_special_chars ($word)
    {
        $word = html_entity_decode($word);
        $word = htmlentities($word);
        $word = str_replace('&lrm;', '', $word);
        return $word;
    }
}

if(!function_exists('like_suggest'))
{
    function like_suggest ($suggest, $user)
    {
        $suggest = addslashes($suggest);
        $aa = WordSuggestionLike::where('suggest', $suggest)->where('user', $user)->where('status', 'like')->first();
        if($aa === NULL)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

if(!function_exists('dislike_suggest'))
{
    function dislike_suggest ($suggest, $user)
    {
        $suggest = addslashes($suggest);
        $aa = WordSuggestionLike::where('suggest', $suggest)->where('user', $user)->where('status', 'dislike')->first();
        if($aa === NULL)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}


if(!function_exists('dictionary_info'))
{
    function dictionary_info ($dicy)
    {
        $dictionary = Dictionary::findorfail($dicy);
        return $dictionary;
    }
}

if(!function_exists('language_words_counter'))
{
    function language_words_counter ($search, $cats, $dictionaries, $language)
    {
        $result = 0;

        if(count($cats) == 1 && $cats[0] == 'All')
        {
            $cats = array();
        }
        
        if(count($cats) > 0)
        {
            if(count($dictionaries) == 0)
            {
                $dictionaries = Dictionary::where('status', 1)->whereIn('category', $cats)->where(function ($query) use ($language) {
                    $query->where('source_lang', $language)->orWhere('target_lang', $language);
                })->get()->pluck('id');
            }
            else
            {
                $dictionaries = Dictionary::where('status', 1)->whereIn('id', $dictionaries)->whereIn('category', $cats)->where(function ($query) use ($language) {
                    $query->where('source_lang', $language)->orWhere('target_lang', $language);
                })->get()->pluck('id');
            }
        }
        else
        {
            if(count($dictionaries) == 0)
            {
                $dictionaries = Dictionary::where('status', 1)->where(function ($query) use ($language) {
                    $query->where('source_lang', $language)->orWhere('target_lang', $language);
                })->get()->pluck('id');
            }
            else
            {
                $dictionaries = Dictionary::where('status', 1)->whereIn('id', $dictionaries)->where(function ($query) use ($language) {
                    $query->where('source_lang', $language)->orWhere('target_lang', $language);
                })->get()->pluck('id');
            }
        }

        $search = addslashes($search);
        $words = Word::where('status', 1)->whereRaw("(words.s_language LIKE '% $search %' OR 
            words.t_language LIKE '% $search %' OR 
            words.s_language LIKE '$search %' OR 
            words.t_language LIKE '$search %' OR
            words.s_language LIKE '% $search' OR 
            words.t_language LIKE '% $search' OR
            words.s_language LIKE '$search' OR 
            words.t_language LIKE '$search'
        )")->whereIn('dictionary', $dictionaries)->count();
        return $words;
    }
}


if(!function_exists('category_words_counter'))
{
    function category_words_counter ($search, $cat, $dictionaries, $languages)
    {
        $result = 0;
        if(count($dictionaries) == 0)
        {
            $dictionaries = Dictionary::where('status', 1)->where('category', $cat)->get()->pluck('id');
        }
        else
        {
            $dictionaries = Dictionary::where('status', 1)->whereIn('id', $dictionaries)->where('category', $cat)->get()->pluck('id');
        }

        if(count($languages) > 0)
        {
            $dictionaries = Dictionary::where('status', 1)->whereIn('id', $dictionaries)->where(function ($query) use ($languages) {
                $query->whereIn('source_lang', $languages)->orWhereIn('target_lang', $languages);
            })->get()->pluck('id');
        }
        $search = addslashes($search);
        $words = Word::where('status', 1)->whereRaw("(
            words.s_language LIKE '% $search %' OR 
            words.t_language LIKE '% $search %' OR 
            words.s_language LIKE '$search %' OR 
            words.t_language LIKE '$search %' OR
            words.s_language LIKE '% $search' OR 
            words.t_language LIKE '% $search' OR
            words.s_language LIKE '$search' OR 
            words.t_language LIKE '$search'
        )")->whereIn('dictionary', $dictionaries)->count();
        return $words;
    }
}


if(!function_exists('dictionary_words_counter'))
{
    function dictionary_words_counter ($search, $dictionary, $languages)
    {
        if(count($languages) > 0)
        {
            $dictionaries = Dictionary::where('status', 1)->where('id', $dictionary)->where(function ($query) use ($languages) {
                $query->whereIn('source_lang', $languages)->orWhereIn('target_lang', $languages);
            })->get()->pluck('id');
            if(count($dictionaries) == 0)
            {
                return 0;
            }
        }
        $result = 0;
        $search = addslashes($search);
        $words = Word::where('status', 1)->whereRaw("(words.s_language LIKE '% $search %' OR 
            words.t_language LIKE '% $search %' OR 
            words.s_language LIKE '$search %' OR 
            words.t_language LIKE '$search %' OR
            words.s_language LIKE '% $search' OR 
            words.t_language LIKE '% $search' OR
            words.s_language LIKE '$search' OR 
            words.t_language LIKE '$search'
        )")->where('dictionary', $dictionary)->count();
        return $words;
    }
}



if(!function_exists('all_trans'))
{
    function all_trans ($search)
    {
        $result = 0;
        $words = Word::where('status', 1)->where('s_language', 'LIKE', $search)->get();
        return $words;
    }
}

if(!function_exists('my_word'))
{
    function my_word ($word, $user)
    {
        $x = MyWord::where('user',$user)->where('word', $word)->first();
        if($x === NULL)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}


if(!function_exists('my_book'))
{
    function my_book ($book, $user)
    {
        $x = MyBook::where('user',$user)->where('book', $book)->first();
        if($x === NULL)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}