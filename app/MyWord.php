<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyWord extends Model
{
    public function word_info ()
    {
        return $this->belongsTo('App\Word', 'word');
    }
}
