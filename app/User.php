<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function country_info ()
    {
        return $this->belongsTo('App\Country', 'country');
    }
    
    public function job_info ()
    {
        return $this->belongsTo('App\Career', 'job');
    }
    
    public function language_info ()
    {
        return $this->belongsTo('App\Language', 'language');
    }
    
    public function cover_letter ()
    {
        return $this->hasOne('App\CoverLetter', 'user');
    }
    
    public function educations ()
    {
        return $this->hasMany('App\Education', 'user')->orderBy('from_year', 'desc');
    }

    public function experinces ()
    {
        return $this->hasMany('App\Experince', 'user')->orderBy('from_year', 'desc');
    }

    public function awards ()
    {
        return $this->hasMany('App\Award', 'user')->orderBy('start_date', 'desc');
    }
    public function skills ()
    {
        return $this->hasMany('App\Skill', 'user');
    }
    public function languages ()
    {
        return $this->hasMany('App\UserLanguage', 'user');
    }
    public function articles ()
    {
        return $this->hasMany('App\Article', 'user');
    }
    public function books ()
    {
        return $this->hasMany('App\Book', 'user');
    }
    public function words ()
    {
        return $this->hasMany('App\MyWord', 'user');
    }
}
