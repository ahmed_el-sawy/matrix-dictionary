<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLanguage extends Model
{
    public function language_info ()
    {
        return $this->belongsTo('App\Language', 'language');
    }

    public function level_info ()
    {
        return $this->belongsTo('App\LanguageLevel', 'level');
    }
}
