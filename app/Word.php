<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $guarded = [];
    
    public function media ()
    {
        return $this->hasMany('App\Words_media', 'word_id');
    }

    public function dictionary_info ()
    {
        return $this->belongsTo('App\Dictionary', 'dictionary');
    }

    public function favs ()
    {
        return $this->hasMany('App\MyWord', 'word');
    }

    public function comments()
    {
        return $this->hasMany('App\WordComment', 'word');
    }

    public function suggestions()
    {
        return $this->hasMany('App\WordSuggestion', 'word');
    }    

    public function articles()
    {
        return $this->hasMany('App\WordArticle', 'word');
    }    
}
