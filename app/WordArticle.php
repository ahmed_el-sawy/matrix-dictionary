<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WordArticle extends Model
{
    public function article_info()
    {
        return $this->belongsTo('App\Article', 'article');
    }
}
