<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WordSuggestion extends Model
{
    public function user_info()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function likes()
    {
        return $this->hasMany('App\WordSuggestionLike', 'suggest')->where('status', 'like');
    }

    public function dislikes()
    {
        return $this->hasMany('App\WordSuggestionLike', 'suggest')->where('status', 'dislike');
    }
}
