<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Words_media extends Model
{
    protected $guarded = [];
    protected $table = 'words_media';
}
