<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // gmail registeration
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->unsignedBigInteger('job')->nullable();
            $table->foreign('job')->references('id')->on('careers')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->string('mobile')->nullable();
            $table->unsignedBigInteger('country')->nullable();
            $table->foreign('country')->references('id')->on('countries')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->smallInteger('type')->default(1)->comment('0 admin, 1 user');
            $table->boolean('su')->default(0)->comment('super admin');
            $table->string('language')->nullable();
            $table->string('avatar')->nullable();
            $table->text('bio')->nullable();
            $table->boolean('status')->default(1);
            $table->string('created_by')->nullable();
            $table->integer('created_by_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
