<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDictionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionaries', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('about')->nullable();
            $table->unsignedBigInteger('source_lang')->nullable();
            $table->foreign('source_lang')->references('id')->on('languages')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->unsignedBigInteger('target_lang')->nullable();
            $table->foreign('target_lang')->references('id')->on('languages')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->float('words_num')->default(0);
            $table->unsignedBigInteger('category')->nullable();
            $table->foreign('category')->references('id')->on('categories')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->unsignedBigInteger('sub_category')->nullable();
            $table->foreign('sub_category')->references('id')->on('categories')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->string('created_by');
            $table->unsignedBigInteger('created_by_id')->nullable();
            $table->foreign('created_by_id')->references('id')->on('users')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionaries');
    }
}
