<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->unsignedBigInteger('language')->nullable();
            $table->foreign('language')->references('id')->on('languages')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->unsignedBigInteger('category')->nullable();
            $table->foreign('category')->references('id')->on('categories')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->unsignedBigInteger('sub_category')->nullable();
            $table->foreign('sub_category')->references('id')->on('categories')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->string('author');
            $table->text('notes')->nullable();
            $table->string('source');
            $table->string('copyRight');
            $table->string('file');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
