<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDictionaryFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionary_files', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dictionary');
            $table->foreign('dictionary')->references('id')->on('dictionaries')->onDelete('CASCADE')->onUpdate('NO ACTION');
            $table->string('source')->nullable();
            $table->string('copy_right')->nullable();
            $table->string('author')->nullable();
            $table->unsignedBigInteger('source_lang')->nullable();
            $table->foreign('source_lang')->references('id')->on('languages')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->unsignedBigInteger('target_lang')->nullable();
            $table->foreign('target_lang')->references('id')->on('languages')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->unsignedBigInteger('category')->nullable();
            $table->foreign('category')->references('id')->on('categories')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->unsignedBigInteger('sub_category')->nullable();
            $table->foreign('sub_category')->references('id')->on('categories')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->string('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionary_files');
    }
}
