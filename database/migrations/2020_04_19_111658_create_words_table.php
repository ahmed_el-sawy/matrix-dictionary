<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('words', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dictionary');
            $table->foreign('dictionary')->references('id')->on('dictionaries')->onDelete('CASCADE')->onUpdate('NO ACTION');
            $table->string('s_language');
            $table->string('t_language');
            $table->longText('s_explain')->nullable();
            $table->longText('t_explain')->nullable();
            $table->longText('examples')->nullable()->comment('json content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('words');
    }
}
