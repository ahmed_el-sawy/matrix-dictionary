<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWordsDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('words_data', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('word_id');
            $table->foreign('word_id')->references('id')->on('words')->onDelete('CASCADE')->onUpdate('NO ACTION');
            $table->string('word');
            $table->longText('explain')->nullable();
            $table->longText('examples')->comment('json examples array')->nullable();
            $table->unsignedBigInteger('language')->nullable();
            $table->foreign('language')->references('id')->on('languages')->onDelete('SET NULL')->onUpdate('NO ACTION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('words_data');
    }
}
