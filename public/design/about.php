<?php include('includes/head.php'); ?>
<?php include('includes/header.php'); ?>


<!--=================================
inner banner -->
<section class="header-inner header-inner-big bg-holder text-white text-center" style="background-image: url(images/bg/banner-01.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="text-primary">About Us</h2>
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="index.html"> Home </a></li>
          <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> About us </span></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!--=================================
inner banner -->

<!--=================================
Millions of jobs -->
<section class="space-pt">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <img class="img-fluid" src="images/blog/01.jpg" alt="">
      </div>
      <div class="col-lg-8">
        <h2 class="mb-4">Millions of jobs, finds the one that's right for you</h2>
        <p class="mb-lg-5 mb-4 lead">We also know those epic stories, those modern-day legends surrounding the early failures of such supremely successful folks as Michael Jordan and Bill Gates. We can look a bit further back in time to Albert Einstein or even further back to Abraham Lincoln.</p>
      </div>
    </div>
  </div>
</section>
<section class="space-ptb">
  <div class="container">
    <div class="row">
      <div class="col-md-4 order-2">
        <img class="img-fluid" src="images/blog/01.jpg" alt="">
      </div>
      <div class="col-lg-8 order-1">
        <h2 class="mb-4">Millions of jobs, finds the one that's right for you</h2>
        <p class="mb-lg-5 mb-4 lead">We also know those epic stories, those modern-day legends surrounding the early failures of such supremely successful folks as Michael Jordan and Bill Gates. We can look a bit further back in time to Albert Einstein or even further back to Abraham Lincoln.</p>
      </div>
    </div>
  </div>
</section>
<!--=================================
Millions of jobs -->

<!--=================================
Advertise A Job -->
<section class="space-pb">
  <div class="container">
    <div class="row">
      <div class="col-md-4 mb-4 mb-md-0">
        <div class="feature-info feature-info-border p-xl-5 p-4 text-center">
          <div class="feature-info-icon mb-3">
            <i class="flaticon-contract"></i>
          </div>
          <div class="feature-info-content">
            <h5 class="text-black">Advertise A Job</h5>
            <p class="mb-0">Use a past defeat as a motivator. Remind yourself you have nowhere to go except.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-4 mb-md-0">
        <div class="feature-info feature-info-border p-xl-5 p-4 text-center">
          <div class="feature-info-icon mb-3">
            <i class="flaticon-profiles"></i>
          </div>
          <div class="feature-info-content">
            <h5 class="text-black">Recruiter Profiles</h5>
            <p class="mb-0">Let success motivate you. Find a picture of what epitomizes success to you have already.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="feature-info feature-info-border p-xl-5 p-4 text-center">
          <div class="feature-info-icon mb-3">
            <i class="flaticon-job-3"></i>
          </div>
          <div class="feature-info-content">
            <h5 class="text-black">Find Your Dream Job</h5>
            <p class="mb-0">Make a list of your achievements toward your long-term goal and remind your.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Advertise A Job -->


<?php include('includes/footer.php'); ?>