<?php include('includes/head.php'); ?>
<?php include('includes/header.php'); ?>

<!--=================================
Dashboard Nav -->
<?php include('profile_menu.php'); ?>
<!--=================================
Dashboard Nav -->


<!--=================================
My Profile -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="user-dashboard-info-box">
          <div class="section-title-02 mb-4">
            <h4>Basic Information</h4>
          </div>
          <div class="cover-photo-contact">
            <div class="cover-photo">
              <img class="img-fluid " src="images/bg/cover-bg.png" alt="">
              <i class="fas fa-times-circle"></i>
            </div>
            <div class="upload-file">
              <div class="custom-file">
                <input type="file" class="custom-file-input">
                <label class="custom-file-label">Upload Cover Photo</label>
              </div>
            </div>
          </div>
          <form>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label>Company Name</label>
                <input type="text" class="form-control" value="Fleet Improvements Pvt">
              </div>
              <div class="form-group col-md-6">
                <label>Email</label>
                <input type="email" class="form-control" value="support@fleetimprovements.com">
              </div>
              <div class="form-group col-md-6">
                <label>First Name</label>
                <input type="text" class="form-control" value="Melissa">
              </div>
              <div class="form-group col-md-6">
                <label>Last Name</label>
                <input type="text" class="form-control" value="Doe">
              </div>
              <div class="form-group col-md-6 datetimepickers">
                <label>Date of Founded</label>
                <div class="input-group date" id="datetimepicker-01" data-target-input="nearest">
                  <input type="text" class="form-control datetimepicker-input" value="02/03/2012" data-target="#datetimepicker-01">
                  <div class="input-group-append" data-target="#datetimepicker-01" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label>Phone</label>
                <input type="text" class="form-control" value="+(123) 345-6789">
              </div>
              <div class="form-group col-md-6 select-border">
                <label>Sector</label>
                <select class="form-control basic-select">
                  <option value="value 01" selected="selected">Taunton, London</option>
                  <option value="value 02">Needham, MA</option>
                  <option value="value 03">New Castle, PA</option>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label>Website</label>
                <input type="text" class="form-control" value="example.com">
              </div>
              <div class="form-group col-md-12 mb-0">
                <label>Description</label>
                <textarea class="form-control" rows="5" placeholder="Use a past defeat as a motivator. Remind yourself you have nowhere to go except up as you have already been at the bottom"></textarea>
              </div>
            </div>
          </form>
        </div>
        <div class="user-dashboard-info-box">
          <div class="section-title-02 mb-3">
            <h4>Social Links</h4>
          </div>
          <form>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label>Facebook</label>
                <input type="text" class="form-control" value="https://www.facebook.com/">
              </div>
              <div class="form-group col-md-6">
                <label>Twitter</label>
                <input type="email" class="form-control" value="https://www.twitter.com/">
              </div>
              <div class="form-group col-md-12 mb-0">
                <label>Linkedin</label>
                <input type="text" class="form-control" value="https://www.linkedin.com/">
              </div>
            </div>
          </form>
        </div>
        <div class="user-dashboard-info-box">
          <div class="section-title-02 mb-3">
            <h4>Address</h4>
          </div>
          <form>


        </div>
        <a class="btn btn-md btn-primary" href="#">Save Settings</a>
      </div>
    </div>
  </div>
</section>
<!--=================================
My Profile -->
<?php include('includes/footer.php'); ?>