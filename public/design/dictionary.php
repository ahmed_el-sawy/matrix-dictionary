<?php include('includes/head.php'); ?>
<?php include('includes/header.php'); ?>

<!--=================================
banner -->
<section class="header-inner header-inner-big bg-holder text-white" style="background-image: url(images/bg/banner-01.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="job-search-field">
          <div class="job-search-item">
            <form class="form row">
              <div class="col-lg-10">
                <div class="form-group left-icon">
                  <input type="text" class="form-control" name="job_title" placeholder="What?">
                <i class="fas fa-search"></i> </div>
              </div>
              <div class="col-lg-2 col-sm-12">
                <div class="form-group form-action">
                  <button type="submit" class="btn btn-primary mt-0"><i class="fas fa-search-location"></i> Find Job</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
banner -->

<!--=================================
job-list -->
<section class="space-ptb">
<div class="container">
  <div class="row">
    <div class="col-lg-3">
      <!--=================================
      left-sidebar -->
      <div class="sidebar">
          <div class="widget">
            <div class="widget-title widget-collapse">
              <h6>Specialism</h6>
              <a class="ml-auto" data-toggle="collapse" href="#specialism" role="button" aria-expanded="false" aria-controls="specialism"> <i class="fas fa-chevron-down"></i> </a> </div>
              <div class="collapse show" id="specialism">
                <div class="widget-content">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism1">
                    <label class="custom-control-label" for="specialism1">Accountancy</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism2">
                    <label class="custom-control-label" for="specialism2">Charity & Voluntary</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism3">
                    <label class="custom-control-label" for="specialism3">Digital & Creative</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism4">
                    <label class="custom-control-label" for="specialism4">Estate Agency</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism5">
                    <label class="custom-control-label" for="specialism5">Apprenticeships</label>
                  </div>

                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism5">
                    <label class="custom-control-label" for="specialism5">Legal</label>
                  </div>
                  
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism1">
                    <label class="custom-control-label" for="specialism1">Accountancy</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism2">
                    <label class="custom-control-label" for="specialism2">Charity & Voluntary</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism3">
                    <label class="custom-control-label" for="specialism3">Digital & Creative</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism4">
                    <label class="custom-control-label" for="specialism4">Estate Agency</label>
                  </div>
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism5">
                    <label class="custom-control-label" for="specialism5">Apprenticeships</label>
                  </div>

                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="specialism5">
                    <label class="custom-control-label" for="specialism5">Legal</label>
                  </div>
                  
                                  </div>
              </div>
            </div>
            <hr>
                      <div class="widget">
                        <div class="widget-add"> <img class="img-fluid" src="images/add-banner.png" alt=""></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-9">
                    <!--=================================
                    right-sidebar -->
                    <div class="row mb-4">
                       <div class="col-md-6">
                        <div class="section-title mb-0">
                          <h6 class="mb-0">Showing 1-10 of <span class="text-primary">66 Jobs</span></h6>
                        </div>
                      </div>
                    <div class="col-md-6">
                    <div class="job-filter mb-0 d-sm-flex align-items-center">
                      <div class="job-shortby ml-sm-auto d-flex align-items-center">
                        <form class="form-inline">
                          <div class="form-group mb-0">
                            <label class="justify-content-start mr-2">sort by :</label>
                            <div class="short-by">
                              <select class="form-control basic-select">
                                <option>Newest</option>
                                <option>Oldest</option>
                              </select>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    </div>
                    </div>
                    <div class="row">
                    	<?php
						for($i = 0; $i < 2; $i++)
						{
							?>
                      <div class="col-12">
                        <div class="job-list ">
                          <div class="job-list-details">
                            <div class="job-list-info">
                              <div class="job-list-title">
                                <h5 class="mb-0"><a href="#">Marketing and Communications</a></h5>
                              </div>
                              <div class="job-list-option">
                                <ul class="list-unstyled">
                                  <li> <span>via</span> <a href="#">Fast Systems Consultants</a> </li>
                                  <li><i class="fas fa-map-marker-alt pr-1"></i>Wellesley Rd, London</li>
                                  <li><i class="fas fa-filter pr-1"></i>Accountancy</li>
                                  <li><a class="freelance" href="#"><i class="fas fa-suitcase pr-1"></i>Freelance</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="job-list-favourite-time"> <a class="job-list-favourite order-2" href="#"><i class="far fa-heart"></i></a></div>
                        </div>
                      </div>
                      <div class="col-12">
                        <div class="job-list">
                          <div class="job-list-details">
                            <div class="job-list-info">
                              <div class="job-list-title">
                                <h5 class="mb-0"><a href="#">Web Developer – .net</a></h5>
                              </div>
                              <div class="job-list-option">
                                <ul class="list-unstyled">
                                  <li> <span>via</span> <a href="#">Pendragon Green Ltd</a> </li>
                                  <li><i class="fas fa-map-marker-alt pr-1"></i>Needham, MA</li>
                                  <li><i class="fas fa-filter pr-1"></i>IT &amp; Telecoms</li>
                                  <li><a class="part-time" href="#"><i class="fas fa-suitcase pr-1"></i>Part-Time</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="job-list-favourite-time"> <a class="job-list-favourite order-2" href="#"><i class="far fa-heart"></i></a></div>
                        </div>
                      </div>
                      <div class="col-12">
                        <div class="job-list">
                          <div class="job-list-details">
                            <div class="job-list-info">
                              <div class="job-list-title">
                                <h5 class="mb-0"><a href="#">Payroll and Office Administrator</a></h5>
                              </div>
                              <div class="job-list-option">
                                <ul class="list-unstyled">
                                  <li> <span>via</span> <a href="#">Wight Sound Hearing LLC</a> </li>
                                  <li><i class="fas fa-map-marker-alt pr-1"></i>New Castle, PA</li>
                                  <li><i class="fas fa-filter pr-1"></i>Banking</li>
                                  <li><a class="temporary" href="#"><i class="fas fa-suitcase pr-1"></i>Temporary</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="job-list-favourite-time"> <a class="job-list-favourite order-2" href="#"><i class="far fa-heart"></i></a></div>
                        </div>
                      </div>
                      <div class="col-12">
                        <div class="job-list">
                          <div class="job-list-details">
                            <div class="job-list-info">
                              <div class="job-list-title">
                                <h5 class="mb-0"><a href="#">Data Entry Administrator</a></h5>
                              </div>
                              <div class="job-list-option">
                                <ul class="list-unstyled">
                                  <li> <span>via</span> <a href="#">Tan Electrics Ltd</a> </li>
                                  <li><i class="fas fa-map-marker-alt pr-1"></i>Park Avenue, Mumbai</li>
                                  <li><i class="fas fa-filter pr-1"></i>Charity &amp; Voluntary</li>
                                  <li><a class="full-time" href="#"><i class="fas fa-suitcase pr-1"></i>Full-time</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="job-list-favourite-time"> <a class="job-list-favourite order-2" href="#"><i class="far fa-heart"></i></a></div>
                        </div>
                      </div>
                      <div class="col-12">
                        <div class="job-list">
                          <div class="job-list-details">
                            <div class="job-list-info">
                              <div class="job-list-title">
                                <h5 class="mb-0"><a href="#">Operational manager part-time</a></h5>
                              </div>
                              <div class="job-list-option">
                                <ul class="list-unstyled">
                                  <li> <span>via</span> <a href="#">Fleet Home Improvements Pvt</a> </li>
                                  <li><i class="fas fa-map-marker-alt pr-1"></i>Green Lanes, London</li>
                                  <li><i class="fas fa-filter pr-1"></i>Accountancy (Qualified)</li>
                                  <li><a class="part-time" href="#"><i class="fas fa-suitcase pr-1"></i>Part-Time</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="job-list-favourite-time"> <a class="job-list-favourite order-2" href="#"><i class="far fa-heart"></i></a></div>
                        </div>
                      </div>
						<?php
                        }
                        ?>
                    </div>
                    <div class="row">
                      <div class="col-12 text-center mt-4 mt-md-5">
                        <ul class="pagination justify-content-center mb-0">
                          <li class="page-item disabled"> <span class="page-link b-radius-none">Prev</span> </li>
                          <li class="page-item active" aria-current="page"><span class="page-link">1 </span> <span class="sr-only">(current)</span></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item"><a class="page-link" href="#">...</a></li>
                          <li class="page-item"><a class="page-link" href="#">25</a></li>
                          <li class="page-item"> <a class="page-link" href="#">Next</a> </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
</section>
<!--=================================
job-list -->


<?php include('includes/footer.php'); ?>