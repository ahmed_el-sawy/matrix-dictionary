<body>

<!--================================= Header -->
<header class="header bg-dark">
  <nav class="navbar navbar-static-top navbar-expand-lg header-sticky">
    <div class="container-fluid">
      <button id="nav-icon4" type="button" class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse">
          <span></span>
          <span></span>
          <span></span>
      </button>
      <a class="navbar-brand" href="index.php">
        <img class="img-fluid" src="images/logo.png" alt="logo">
      </a>
      <div class="navbar-collapse collapse justify-content-start">
        <ul class="nav navbar-nav">
          <li class="nav-item dropdown active"><a class="nav-link" href="index.php">Home</a></li>
          <li class="nav-item dropdown"><a class="nav-link" href="about.php">About Us</a></li>
          <li class="nav-item dropdown"><a class="nav-link" href="blog.php">Blog</a></li>
          <li class="nav-item dropdown"><a class="nav-link" href="contact.php">Contact Us</a></li>
          <li class="nav-item dropdown"><a class="nav-link" href="profile.php">My Profile</a></li>
      
        </ul>
      </div>
      <div class="add-listing">
          <div class="login d-inline-block mr-4">
            <a href="login.php" data-toggle="modal" data-target="#exampleModalCenter"><i class="far fa-user pr-2"></i>Sign in</a>
          </div>
          <a class="btn btn-white btn-md" href="post-article.php"> <i class="fas fa-plus-circle"></i>Post Article</a>
        </div>
    </div>
  </nav>
</header>
<!--================================= Header -->