<?php include('includes/head.php'); ?>
<?php include('includes/header.php'); ?>


<!--=================================
banner -->
<section class="banner-bg-slider">
  <div id="bg-slider">
    <img src="images/bg-slider/01.jpg" alt="">
    <img src="images/bg-slider/02.jpg" alt="">
    <img src="images/bg-slider/03.jpg" alt="">
  </div>
  <div class="banner-bg-slider-content">
    <div class="container">
    <div class="row justify-content-center">
      <div class=" col-lg-9 col-md-9 d-flex">
        <div class="content text-center">
          <h1 class="text-white mb-2">Drop <span class="text-primary"> Resume &amp; Get </span> Your Desired Job</h1>
          <p class="lead mb-4 font-weight-normal text-white">We've got monthly and daily plans that fit your needs. You can always exchange out jobs, upgrade or scale down when you need to.</p>
          <div class="job-search-field">
            <div class="job-search-item">
              <form method="get" action="dictionary.php">
                <div class="col-sm-12">
                  <div class="form-group mb-md-0 justify-content-center">
                      <input type="text" class="form-control" name="job_title" placeholder="Job Title, Skill or Company">
                    <button type="submit" class="btn btn-primary btn-lg m-0"><i class="fas fa-search"></i> Find Jobs</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!--=================================
banner -->


<!--=================================
Action-box -->
<section class="bg-primary py-4 py-lg-5 ">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-9 mb-4 mb-sm-4 mb-lg-0">
        <div class="d-sm-flex">
          <h4 class="text-white">Create free account to find thousands Jobs, Employment &amp; Career Opportunities around you!</h4>
        </div>
      </div>
      <div class="col-md-3 text-lg-right">
        <a class="btn btn-dark" href="post-article.php">Post Article</a>
      </div>
    </div>
  </div>
</section>
<!--=================================
Action-box -->


<!--=================================
Category-style -->
<section class="space-ptb">
  <div class="container">
    <div class="section-title center">
      <h2 class="title">Choose Your Sector</h2>
      <p class="mb-0">What made each of these people so successful? Motivation.</p>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="category-style text-center">
          <a href="dictionary.php" class="category-item">
            <div class="category-icon mb-4">
              <i class="flaticon-account"></i>
            </div>
            <h6>Accountancy</h6>
            <span class="mb-0">301 Open Position </span>
          </a>
          <a href="dictionary.php" class="category-item">
            <div class="category-icon mb-4">
              <i class="flaticon-conversation"></i>
            </div>
            <h6>Apprenticeships</h6>
            <span class="mb-0">287 Open Position </span>
          </a>
          <a href="dictionary.php" class="category-item">
            <div class="category-icon mb-4">
              <i class="flaticon-money"></i>
            </div>
            <h6>Banking</h6>
            <span class="mb-0">542 Open Position </span>
          </a>
          <a href="dictionary.php" class="category-item">
            <div class="category-icon mb-4">
              <i class="flaticon-mortarboard"></i>
            </div>
            <h6>Education</h6>
            <span class="mb-0">785 Open Position </span>
          </a>
          <a href="dictionary.php" class="category-item">
            <div class="category-icon mb-4">
              <i class="flaticon-worker"></i>
            </div>
            <h6>Engineering</h6>
            <span class="mb-0">862 Open Position </span>
          </a>
          <a href="dictionary.php" class="category-item">
            <div class="category-icon mb-4">
              <i class="flaticon-businessman"></i>
            </div>
            <h6>Estate Agency</h6>
            <span class="mb-0">423 Open Position </span>
          </a>
          <a href="dictionary.php" class="category-item">
            <div class="category-icon mb-4">
              <i class="flaticon-coding"></i>
            </div>
            <h6>IT & Telecoms</h6>
            <span class="mb-0">253 Open Position </span>
          </a>
          <a href="dictionary.php" class="category-item">
            <div class="category-icon mb-4">
              <i class="flaticon-balance"></i>
            </div>
            <h6>Legal</h6>
            <span class="mb-0">689 Open Position </span>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Category-style -->

<!--=================================
Why You Choose -->
<section class="bg-light">
  <div class="container-fluid p-0">
    <div class="row align-self-center m-0">
      <div class="col-md-6 bg-holder" style="background-image: url(images/bg/cover-bg-2.jpg); background-size: cover;">
      </div>
      <div class="col-xl-6 col-lg-6 col-md-12">
        <div class="feature-content">
          <div class="row">
            <div class="col-xl-10 col-lg-12">
              <div class="section-title-02">
                <h2>Why You Choose Job Among Other Job Site?</h2>
                <p>We know this in our gut, but what can we do about it? How can we motivate ourselves? One of the most difficult aspects of achieving success is staying motivated over the long haul.</p>
              </div>
            </div>
          </div>
          <div class="align-self-center">
            <div class="row">
              <div class="col-lg-12">
                <div class="row category-style pb-2">
                  <div class="col-md-6 col-sm-12 mb-3">
                    <div class="category-icon mb-3">
                      <i class="flaticon-team"></i>
                    </div>
                    <h6 class="mb-2">Best talented people</h6>
                    <p>If success is a process with a number of defined steps.</p>
                  </div>
                  <div class="col-md-6 col-sm-12 mb-3">
                    <div class="category-icon mb-3">
                      <i class="flaticon-chat"></i>
                    </div>
                    <h6 class="mb-2">Easy to communicate</h6>
                    <p>Having clarity of purpose and a clear picture of what you desire.</p>
                  </div>
                  <div class="col-md-6 col-sm-12 mb-3">
                    <div class="category-icon mb-3">
                      <i class="flaticon-job-3"></i>
                    </div>
                    <h6 class="mb-2">Easy to find candidate</h6>
                    <p>Introspection is the trick. Understand what you want.</p>
                  </div>
                  <div class="col-md-6 col-sm-12 mb-3">
                    <div class="category-icon mb-3">
                      <i class="flaticon-job-2"></i>
                    </div>
                    <h6 class="mb-2">Global recruitment option</h6>
                    <p>There are basically six key areas to higher achievement.</p>
                  </div>
                </div>
                <a class="btn btn-primary" href="about.php">Get Started</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Why You Choose -->

<!--=================================
Top Companies -->
<section class="space-ptb">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 text-center">
        <div class="section-title center">
          <h2 class="title">Top Companies</h2>
          <p>Data trends and insights, tips for employers, product updates and best practices</p>
        </div>
        <div class="owl-carousel owl-nav-bottom-center" data-nav-arrow="false" data-nav-dots="true" data-items="4" data-md-items="3" data-sm-items="2" data-xs-items="1" data-xx-items="1" data-space="15" data-autoheight="true">
          <div class="item">
            <div class="employers-grid mb-4 mb-lg-0">
              <div class="employers-list-logo">
                <img class="img-fluid" src="images/svg/07.svg" alt="">
              </div>
              <div class="employers-list-details">
                <div class="employers-list-info">
                  <div class="employers-list-title">
                    <h5 class="mb-0"><a href="employer-detail.html">Trout Design Ltd</a></h5>
                  </div>
                  <div class="employers-list-option">
                    <ul class="list-unstyled">
                      <li><i class="fas fa-map-marker-alt pr-1"></i>Wellesley Rd, London</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="employers-list-position">
                <a class="btn btn-sm btn-dark" href="#">30 Open position</a>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="employers-grid mb-4 mb-md-0">
              <div class="employers-list-logo">
                <img class="img-fluid" src="images/svg/08.svg" alt="">
              </div>
              <div class="employers-list-details">
                <div class="employers-list-info">
                  <div class="employers-list-title">
                    <h5 class="mb-0"><a href="employer-detail.html">Lawn Hopper</a></h5>
                  </div>
                  <div class="employers-list-option">
                    <ul class="list-unstyled">
                      <li><i class="fas fa-map-marker-alt pr-1"></i>Needham, MA</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="employers-list-position">
                <a class="btn btn-sm btn-dark" href="#">35 Open position</a>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="employers-grid mb-4 mb-lg-0">
              <div class="employers-list-logo">
                <img class="img-fluid" src="images/svg/09.svg" alt="">
              </div>
              <div class="employers-list-details">
                <div class="employers-list-info">
                  <div class="employers-list-title">
                    <h5 class="mb-0"><a href="employer-detail.html">Trout Design Ltd</a></h5>
                  </div>
                  <div class="employers-list-option">
                    <ul class="list-unstyled">
                      <li><i class="fas fa-map-marker-alt pr-1"></i>Wellesley Rd, London</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="employers-list-position">
                <a class="btn btn-sm btn-dark" href="#">30 Open position</a>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="employers-grid mb-4 mb-md-0">
              <div class="employers-list-logo">
                <img class="img-fluid" src="images/svg/10.svg" alt="">
              </div>
              <div class="employers-list-details">
                <div class="employers-list-info">
                  <div class="employers-list-title">
                    <h5 class="mb-0"><a href="employer-detail.html">Lawn Hopper</a></h5>
                  </div>
                  <div class="employers-list-option">
                    <ul class="list-unstyled">
                      <li><i class="fas fa-map-marker-alt pr-1"></i>Needham, MA</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="employers-list-position">
                <a class="btn btn-sm btn-dark" href="#">35 Open position</a>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="employers-grid mb-4 mb-lg-0">
              <div class="employers-list-logo">
                <img class="img-fluid" src="images/svg/11.svg" alt="">
              </div>
              <div class="employers-list-details">
                <div class="employers-list-info">
                  <div class="employers-list-title">
                    <h5 class="mb-0"><a href="employer-detail.html">Rippin LLC</a></h5>
                  </div>
                  <div class="employers-list-option">
                    <ul class="list-unstyled">
                      <li><i class="fas fa-map-marker-alt pr-1"></i>Park Avenue, Mumbai</li>
                    </ul>
                  </div>
                </div>
                <div class="employers-list-position">
                  <a class="btn btn-sm btn-dark" href="#">20 Open position</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="employers-grid mb-4 mb-lg-0">
              <div class="employers-list-logo">
                <img class="img-fluid" src="images/svg/12.svg" alt="">
              </div>
              <div class="employers-list-details">
                <div class="employers-list-info">
                  <div class="employers-list-title">
                    <h5 class="mb-0"><a href="employer-detail.html">Trophy and Sons</a></h5>
                  </div>
                  <div class="employers-list-option">
                    <ul class="list-unstyled">
                      <li><i class="fas fa-map-marker-alt pr-1"></i>Green Lanes, London</li>
                    </ul>
                  </div>
                </div>
                <div class="employers-list-position">
                  <a class="btn btn-sm btn-dark" href="#">25 Open position</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="employers-grid mb-4 mb-md-0">
              <div class="employers-list-logo">
                <img class="img-fluid" src="images/svg/13.svg" alt="">
              </div>
              <div class="employers-list-details">
                <div class="employers-list-info">
                  <div class="employers-list-title">
                    <h5 class="mb-0"><a href="employer-detail.html">Lawn Hopper</a></h5>
                  </div>
                  <div class="employers-list-option">
                    <ul class="list-unstyled">
                      <li><i class="fas fa-map-marker-alt pr-1"></i>Needham, MA</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="employers-list-position">
                <a class="btn btn-sm btn-dark" href="#">35 Open position</a>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="employers-grid mb-4 mb-lg-0">
              <div class="employers-list-logo">
                <img class="img-fluid" src="images/svg/14.svg" alt="">
              </div>
              <div class="employers-list-details">
                <div class="employers-list-info">
                  <div class="employers-list-title">
                    <h5 class="mb-0"><a href="employer-detail.html">Rippin LLC</a></h5>
                  </div>
                  <div class="employers-list-option">
                    <ul class="list-unstyled">
                      <li><i class="fas fa-map-marker-alt pr-1"></i>Park Avenue, Mumbai</li>
                    </ul>
                  </div>
                </div>
                <div class="employers-list-position">
                  <a class="btn btn-sm btn-dark" href="#">20 Open position</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="employers-grid mb-4 mb-lg-0">
              <div class="employers-list-logo">
                <img class="img-fluid" src="images/svg/15.svg" alt="">
              </div>
              <div class="employers-list-details">
                <div class="employers-list-info">
                  <div class="employers-list-title">
                    <h5 class="mb-0"><a href="employer-detail.html">Trophy and Sons</a></h5>
                  </div>
                  <div class="employers-list-option">
                    <ul class="list-unstyled">
                      <li><i class="fas fa-map-marker-alt pr-1"></i>Green Lanes, London</li>
                    </ul>
                  </div>
                </div>
                <div class="employers-list-position">
                  <a class="btn btn-sm btn-dark" href="#">25 Open position</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Top Companies -->

<!--=================================
Easiest Way to Use -->
<section class="space-ptb bg-primary">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-8 col-md-10">
        <div class="section-title-02 text-center text-white">
          <h2 class="text-white">Easiest Way to Use</h2>
          <p>Positive pleasure-oriented goals are much more powerful motivators than negative fear-based ones.</p>
        </div>
      </div>
    </div>
    <div class="row bg-holder-pattern mr-md-0 ml-md-0" style="background-image: url('images/step/pattern-01.png');">
      <div class="col-md-4 mb-4 mb-md-0">
        <div class="feature-step text-center">
          <div class="feature-info-icon">
            <i class="flaticon-resume"></i>
          </div>
          <div class=" text-white">
            <h5>Create Account</h5>
            <p class="mb-0">Create an account and access your saved settings on any device.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-4 mb-md-0">
        <div class="feature-step text-center">
          <div class="feature-info-icon">
            <i class="flaticon-recruitment"></i>
          </div>
          <div class=" text-white">
            <h5>Find your Vacancy</h5>
            <p class="mb-0">Don't just find. Be found. Put your CV in front of great employers.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-0">
        <div class="feature-step text-center">
          <div class="feature-info-icon">
            <i class="flaticon-position"></i>
          </div>
          <div class=" text-white">
            <h5>Get a Job</h5>
            <p class="mb-0">Your next career move starts here. Choose Job from thousands of companies</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Easiest Way to Use -->
<?php include('includes/footer.php'); ?>