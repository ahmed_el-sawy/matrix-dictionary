<?php include('includes/head.php'); ?>
<?php include('includes/header.php'); ?>

<!--=================================
inner banner -->
<section class="header-inner header-inner-big bg-holder text-white text-center" style="background-image: url(images/bg/banner-01.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="text-primary">Login</h2>
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="index.php"> Home </a></li>
          <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Login </span></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!--=================================
inner banner -->

<!--=================================
Register -->
<section class="space-ptb">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-8 col-lg-10 col-md-12">
        <div class="login-register">
         <div class="section-title">
          <h4 class="text-center">Create Your Account</h4>
         </div>
          <div class="tab-content">
            <div class="tab-pane active" id="candidate" role="tabpanel">
              <form class="mt-4">
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label>Email Address *</label>
                    <input type="text" class="form-control">
                  </div>
                  <div class="form-group col-md-6">
                    <label>Password *</label>
                    <input type="password" class="form-control">
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-6">
                    <a class="btn btn-primary d-block" href="#">Sign in</a>
                  </div>
                  <div class="col-md-6 text-md-right mt-2 text-center">
                    <p>Don't have account? <a href="login.php"> Sign up here</a></p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Register -->

<?php include('includes/footer.php'); ?>