<?php include('includes/head.php'); ?>
<?php include('includes/header.php'); ?>

<!--=================================
Dashboard Nav -->
<?php include('profile_menu.php'); ?>
<!--=================================
Dashboard Nav -->


<!--=================================
Manage Jobs -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="user-dashboard-info-box mb-0">
          <div class="row mb-4">
            <div class="col-md-7 col-sm-5 d-flex align-items-center">
              <div class="section-title-02 mb-0 ">
                <h4 class="mb-0">My Blog <a href="post-new-article.php" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> Post New Article</a></h4>
              </div>
            </div>
            <div class="col-md-5 col-sm-7 mt-3 mt-sm-0">
              <div class="search">
                <i class="fas fa-search"></i>
                <input type="text" class="form-control" placeholder="Search...">
              </div>
            </div>
          </div>
          <div class="user-dashboard-table table-responsive">
            <table class="table table-bordered">
              <thead class="bg-light">
                <tr >
                  <th scope="col">Title</th>
                  <th scope="col">Publish Date</th>
                  <th scope="col">Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">Article 01</th>
                    <td>2020-04-15</td>
                  <td>
                    <ul class="list-unstyled mb-0 d-flex">
                      <li><a href="#" class="text-primary" data-toggle="tooltip" title="view"><i class="far fa-eye"></i></a></li>
                      <li><a href="#" class="text-info" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a></li>
                      <li><a href="#" class="text-danger" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt"></i></a></li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Article 02</th>
                    <td>2020-04-20</td>
                    <td>
                    <ul class="list-unstyled mb-0 d-flex">
                      <li><a href="#" class="text-primary" data-toggle="tooltip" title="view"><i class="far fa-eye"></i></a></li>
                      <li><a href="#" class="text-info" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a></li>
                      <li><a href="#" class="text-danger" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt"></i></a></li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Article 03</th>
                    <td>2020-04-23</td>
                  <td>
                    <ul class="list-unstyled mb-0 d-flex">
                      <li><a href="#" class="text-primary" data-toggle="tooltip" title="view"><i class="far fa-eye"></i></a></li>
                      <li><a href="#" class="text-info" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a></li>
                      <li><a href="#" class="text-danger" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt"></i></a></li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <th scope="row">Article 04</th>
                    <td>2020-04-25</td>
                  <td>
                    <ul class="list-unstyled mb-0 d-flex">
                      <li><a href="#" class="text-primary" data-toggle="tooltip" title="view"><i class="far fa-eye"></i></a></li>
                      <li><a href="#" class="text-info" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a></li>
                      <li><a href="#" class="text-danger" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt"></i></a></li>
                    </ul>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="row justify-content-center">
            <div class="col-12 text-center">
              <ul class="pagination mt-3">
                <li class="page-item disabled mr-auto">
                  <span class="page-link b-radius-none">Prev</span>
                </li>
                <li class="page-item active" aria-current="page"><span class="page-link">1 </span> <span class="sr-only">(current)</span></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item ml-auto">
                  <a class="page-link" href="#">Next</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Manage Jobs -->

<?php include('includes/footer.php'); ?>