<?php include('includes/head.php'); ?>
<?php include('includes/header.php'); ?>

<!--=================================
tab -->
<section class="header-inner header-inner-big bg-holder text-white text-center" style="background-image: url(images/bg/banner-01.jpg);">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="section-title text-center">
        <h2 class="text-primary">Post a New Article</h2>
       </div>
      </div>
    </div>
  </div>
</section>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade active show" id="Job-detail" role="tabpanel" aria-labelledby="Job-detail-tab">
    <section class="space-ptb">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <form class="form-row">
              <div class="form-group col-md-12">
                <label>Title *</label>
                <input type="text" class="form-control" value="" placeholder="Enter a Title">
              </div>
              <div class="form-group col-md-12">
                <label>Description *</label>
                <textarea class="form-control" rows="4"></textarea>
              </div>

                <div class="row mt-4 mt-lg-5">
                  <div class="col-12">
                    <h5 class="mb-4">File Attachments</h5>
                  </div>
                </div>
                <div class="custom-file">
                  <input type="file" class="custom-file-input">
                  <label class="custom-file-label">Choose file</label>
                </div>
         
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<!--=================================
feature-info-->


<?php include('includes/footer.php'); ?>