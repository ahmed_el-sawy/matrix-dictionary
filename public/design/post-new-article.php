<?php include('includes/head.php'); ?>
<?php include('includes/header.php'); ?>

<!--=================================
Dashboard Nav -->
<?php include('profile_menu.php'); ?>
<!--=================================
Dashboard Nav -->

<!--=================================
Post New Job -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="user-dashboard-info-box">
          <div class="section-title-02 mb-4">
            <h4>Post a New Article</h4>
          </div>
          <div class="row">
            <div class="col-12">
             
            <form class="form-row">
              <div class="form-group col-md-12">
                <label>Title *</label>
                <input type="text" class="form-control" value="" placeholder="Enter a Title">
              </div>
              <div class="form-group col-md-12">
                <label>Description *</label>
                <textarea class="form-control" rows="4"></textarea>
              </div>

                <div class="row mt-4 mt-lg-5">
                  <div class="col-12">
                    <h5 class="mb-4">File Attachments</h5>
                  </div>
                </div>
                <div class="custom-file">
                  <input type="file" class="custom-file-input">
                  <label class="custom-file-label">Choose file</label>
                </div>
         
            </form>
            
      </div>
    </div>
  </div>
        </div>
    </div>
  </div>
</section>
<!--=================================
Post New Job -->
<?php include('includes/footer.php'); ?>