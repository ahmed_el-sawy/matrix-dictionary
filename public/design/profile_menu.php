<style>
header.header {background: #001935 !important;}
</style>

<!--=================================
inner banner -->
<section class="header-inner header-inner-big bg-holder bg-light pb-5">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <div class="jobber-user-info">
            <div class="profile-avatar">
              <img class="img-fluid " src="images/avatar/04.jpg" alt="">
              <i class="fas fa-pencil-alt"></i>
            </div>
            <div class="profile-avatar-info ml-4">
               <h3 class="text-dark">Felica Queen</h3>
            </div>
          </div>
      </div>
      
      <div class="col-lg-6">
            <div class="progress">
              <div class="progress-bar" role="progressbar" style="width:85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                <span class="progress-bar-number">85%</span>
              </div>
            </div>
            <div class="candidates-skills">
              <div class="candidates-skills-info">
                <h3 class="text-primary">85%</h3>
                <span class="d-block">Skills increased by job Title.</span>
              </div>
              <div class="candidates-required-skills ml-auto mt-sm-0 mt-3">
                <a class="btn btn-dark" href="#">Complete Required Skills</a>
              </div>
            </div>
          </div>

    </div>
  </div>
</section>
<!--=================================
inner banner -->


<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="secondary-menu-sticky-top">
          <div class="secondary-menu">
            <ul>
              <li><a class="active" href="profile.php">Dashboard</a></li>
              <li><a href="change-information.php">Change Information</a></li>
              <li><a href="change-password.php">Change Password</a></li>
              <li><a href="my-resume.php">My Resume</a></li>
              <li><a href="my-blog.php">My Blog</a></li>
              <li><a href="my-library.php">My Library</a></li>
              <li><a href="my-dictionaries.php">My Dictionaries</a></li>
              <li><a href="login.php">Log Out</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>