/*================================================
[  Table of contents  ]
================================================

:: Preloader
:: Menu
:: Tooltip
:: Counter
:: Owl carousel
:: Slickslider
:: Magnific Popup
:: Datetimepicker
:: Select2
:: Range Slider
:: Countdown
:: Scrollbar
:: Back to top

======================================
[ End table content ]
======================================*/
//POTENZA var


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

( function ($) {
  "use strict";
  var POTENZA = {};

	/*************************
	  Predefined Variables
	*************************/
	
	var $window     = $(window),
    $document       = $(document),
    $body           = $('body'),
    $countdownTimer = $('.countdown'),
    $counter        = $('.counter');
  
	//Check if function exists
	$.fn.exists = function () {
		return this.length > 0;
	};

	/*************************
		Preloader
    *************************/
	
	POTENZA.preloader = function () {
       $("#load").fadeOut();
       $('#pre-loader').delay(0).fadeOut('slow');
	};

	/*************************
		menu
	*************************/
	POTENZA.dropdownmenu = function () {
		if ( $('.navbar').exists() ) {
			$('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
				if ( !$(this).next().hasClass('show') ) {
					$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
				}
				var $subMenu = $(this).next(".dropdown-menu");
				$subMenu.toggleClass('show');
				
				$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
					$('.dropdown-submenu .show').removeClass("show");
				});
				return false;
			});
		}
	};

	$(document).ready(function(){

		$('body').on('submit', '#SearchTermForm', function() {
			var search = $('#searchterminput').val();
			if(search == '')
			{
				return false;
			}
			else if(search.length < 2)
			{
				return false;				
			}
			else
			{
				return true;
			}
		});


		$('body').on('submit', '#subscribe_form', function(e) {
			e.preventDefault(); 
			$('#subscribe-res').html('<i class="fas fa-spinner fa-pulse mr-3"></i>');
			var action = $(this).attr('action');
			var formData = new FormData($(this)[0]);
			$.ajax({
				type: 'POST',
				data: formData,
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				error: function(data) {
					jQuery.each(data.errors, function(key, value){
						$('#subscribe-res').html('<div class="alert alert-danger">'+value+'</div>');	
					});
				},
				success: function(data) 
				{
					if(data.success)
					{
						$('#subscribe-res').html('<div class="alert alert-success">'+data.message+'</div>');	
					}
					else
					{
						$('#subscribe-res').html('<div class="alert alert-danger">'+data.errors+'</div>');	
					}
				}
			});
			return false;
		  });

		  
		  $('body').on('submit', '#new_article_form', function(e) {
			e.preventDefault(); 
			$('#new_article_alert').html('<i class="fas fa-spinner fa-pulse mr-3"></i>');
			var action = $(this).attr('action');
			var formData = new FormData($(this)[0]);
			$.ajax({
				type: 'POST',
				data: formData,
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				error: function(data) {
					jQuery.each(data.errors, function(key, value){
						$('#new_article_alert').html('<div class="alert alert-danger">'+value+'</div>');	
					});
				},
				success: function(data) 
				{
					if(data.success)
					{
						window.location.href = data.data;
					}
					else
					{
						$('#new_article_alert').html('<div class="alert alert-danger">'+data.errors+'</div>');	
					}
				}
			});
			return false;
		  });

		  $('body').on('submit', '#word_suggestion', function(e) {
			e.preventDefault(); 
			$('#article_comment_red').html('<i class="fas fa-spinner fa-pulse mr-3"></i>');
			var action = $(this).attr('action');
			var num = $(this).attr('data-num');
			var formData = new FormData($(this)[0]);
			$.ajax({
				type: 'POST',
				data: formData,
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				error: function(data) {
					jQuery.each(data.errors, function(key, value){
						$('#word_suggestion_red'+num).html('<div class="alert alert-danger">'+value+'</div>');	
					});
				},
				success: function(data) 
				{
					if(data.success)
					{
						$('#word_dict_sidebar_'+num).html(data.data);
					}
					else
					{
						$('#word_suggestion_red'+num).html('<div class="alert alert-danger">'+data.errors+'</div>');	
					}
				}
			});
			return false;
		  });


		  $('body').on('submit', '#word_comment', function(e) {
			e.preventDefault(); 
			$('#article_comment_red').html('<i class="fas fa-spinner fa-pulse mr-3"></i>');
			var action = $(this).attr('action');
			var num = $(this).attr('data-num');
			var formData = new FormData($(this)[0]);
			$.ajax({
				type: 'POST',
				data: formData,
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				error: function(data) {
					jQuery.each(data.errors, function(key, value){
						$('#word_comment_red'+num).html('<div class="alert alert-danger">'+value+'</div>');	
					});
				},
				success: function(data) 
				{
					if(data.success)
					{
						$('#word_dict_sidebar_'+num).html(data.data);
					}
					else
					{
						$('#word_comment_red'+num).html('<div class="alert alert-danger">'+data.errors+'</div>');	
					}
				}
			});
			return false;
		  });


		  $('body').on('submit', '#article_comment', function(e) {
			e.preventDefault(); 
			$('#article_comment_red').html('<i class="fas fa-spinner fa-pulse mr-3"></i>');
			var action = $(this).attr('action');
			var formData = new FormData($(this)[0]);
			$.ajax({
				type: 'POST',
				data: formData,
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				error: function(data) {
					jQuery.each(data.errors, function(key, value){
						$('#article_comment_red').html('<div class="alert alert-danger">'+value+'</div>');	
					});
				},
				success: function(data) 
				{
					if(data.success)
					{
						  location.reload();
					}
					else
					{
						$('#article_comment_red').html('<div class="alert alert-danger">'+data.errors+'</div>');	
					}
				}
			});
			return false;
		  });
		  
		$('body').on('submit', '#kt_user_add_form', function(e) {
			e.preventDefault(); 
			$('#kt_user_add_form_result').html('<i class="fas fa-spinner fa-pulse mr-3"></i>');
			var action = $(this).attr('action');
			var formData = new FormData($(this)[0]);
			$.ajax({
				type: 'POST',
				data: formData,
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				error: function(data) {
					jQuery.each(data.errors, function(key, value){
						$('#kt_user_add_form_result').html('<div class="alert alert-danger">'+value+'</div>');	
					});
				},
				success: function(data) 
				{
					if(data.success)
					{
						  location.reload();
					}
					else
					{
						$('#kt_user_add_form_result').html('<div class="alert alert-danger">'+data.errors+'</div>');	
					}
				}
			});
			return false;
		  });
        
        $('body').on('submit', '.kt_user_edit_form', function(e) {
			e.preventDefault(); 
			var aa = $(this).attr('data-book');
			$('#kt_user_edit_form_result_'+aa).html('<i class="fas fa-spinner fa-pulse mr-3"></i>');
			var action = $(this).attr('action');
			var formData = new FormData($(this)[0]);
			$.ajax({
				type: 'POST',
				data: formData,
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				error: function(data) {
					jQuery.each(data.errors, function(key, value){
						$('#kt_user_edit_form_result_'+aa).html('<div class="alert alert-danger">'+value+'</div>');	
					});
				},
				success: function(data) 
				{
					if(data.success)
					{
						  location.reload();
					}
					else
					{
						$('#kt_user_edit_form_result_'+aa).html('<div class="alert alert-danger">'+data.errors+'</div>');	
					}
				}
			});
			return false;
		  });
		  
		$('#nav-icon4').on( 'click', function(){
			$(this).toggleClass('open');
		});
		$('body').on('click', '.word_dictionary_side_actions' , function(){
			var num = $(this).attr('data-num');
			if($(this).hasClass('active_word_action'))
			{
				$(this).removeClass('active_word_action');
				$('#word_dict_sidebar_'+num).html('');
				return false;
			}
			else
			{
				$('.wdsa'+num).removeClass('active_word_action');				
				$(this).addClass('active_word_action');
			}
			var type = $(this).attr('data-type');
			var action = $(this).attr('href');
			$.ajax({
				type: 'POST',
				data: {type: type},
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				success: function(data) 
				{
					$('.word_info').html('');
					$('#word_dict_sidebar_'+num).html(data);
				}
			});
			return false;
		});

		$('body').on('click', '.like_book' , function(e){
			e.preventDefault();
			var action = $(this).attr('href');
			var target = $(this).attr('data-target');
			$.ajax({
				type: 'POST',
				data:{},
				url: action,
				success: function(data) 
				{
					$(target).html(data);
				}
			});
			return false;
		});

		$('body').on('click', '.suggestions_like_action' , function(){
			var num = $(this).attr('data-num');
			var action = $(this).attr('href');
			$.ajax({
				type: 'POST',
				data: {word: num},
				url: action,
				success: function(data) 
				{
					$('#word_dict_sidebar_'+num).html(data.data);
				}
			});


			return false;
		});

		$('body').on('change', '.Formcheckbox', function(){
			$('#SideBarFilterForm').submit();
			return false;
		});
		$('body').on('change', '#sort_by_selector', function(){
			$('#sortingDictionaryForm').submit();
			return false;
		});

		$('body').on('submit', '#login_form', function(e) {
			e.preventDefault(); 
			$('#login_results').html('<i class="fas fa-spinner fa-pulse"></i>');
			var action = $(this).attr('action');
			var formData = new FormData($(this)[0]);
			$.ajax({
				type: 'POST',
				data: formData,
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				error: function(data) {
					jQuery.each(data.errors, function(key, value){
						$('#login_results').html('<div class="alert alert-danger"><i class="fas fa-times-circle"></i> '+value+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
					});
				},
				success: function(data) 
				{
					if(data.success)
					{
						location.reload();
					}
					else
					{
						$('#login_results').html('<div class="alert alert-danger"><i class="fas fa-times-circle"></i> '+data.errors+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
					}
				}
			});
			return false;
		});


		$('body').on('click', '#add_skill', function(e) {
			e.preventDefault(); 
			var action = $(this).attr('href');
			$.ajax({
				type: 'POST',
				data: {},
				url: action,
				success: function(data) 
				{
					$('#skills_list').append(data);
				}
			});
			return false;
		});


		$('body').on('click', '#add_language', function(e) {
			e.preventDefault(); 
			var action = $(this).attr('href');
			$.ajax({
				type: 'POST',
				data: {},
				url: action,
				success: function(data) 
				{
					$('#langauges_list').append(data);
				}
			});
			return false;
		});

		$('body').on('click', '.delte_resume_skill, .delte_resume_language', function(e) {
			e.preventDefault();
			$(this).parent().parent().remove();
			return false;
		});

		$('body').on('change', '.attend_there', function() {
			var ss = $(this).attr('data-selector');
			if($(this).prop('checked') == true)
			{
				$(ss).attr('disabled', 'disabled');
			}
			else
			{
				$(ss).attr('disabled', false);
			}
		});

		$('body').on('submit', '.my_resume_form', function(e) {
			e.preventDefault(); 
			var target = $(this).attr('data-target');

			$(target).html('<i class="fas fa-spinner fa-pulse"></i>');
			var action = $(this).attr('action');
			var formData = new FormData($(this)[0]);
			$.ajax({
				type: 'POST',
				data: formData,
				async: true,
				cache: false,
				contentType: false,
				processData: false,
				url: action,
				error: function(data) {
					jQuery.each(data.errors, function(key, value){
						$(target).html('<div class="alert alert-danger"><i class="fas fa-times-circle"></i> '+value+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
					});
				},
				success: function(data) 
				{
					if(data.success)
					{
						location.reload();
					}
					else
					{
						$(target).html('<div class="alert alert-danger"><i class="fas fa-times-circle"></i> '+data.errors+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
					}
				}
			});
			return false;
		});

	});

	/*************************
		sticky
	*************************/
	POTENZA.isSticky = function () {
		$(window).scroll( function(){
			if ($(this).scrollTop() > 150) {
				$('.header-sticky').addClass('is-sticky');
			} else {
				$('.header-sticky').removeClass('is-sticky');
			}
		});
	};


	/*************************
		tooltip
	*************************/

	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover()

	/*************************
		counter
	*************************/
	
	POTENZA.counters = function () {
		var counter = jQuery(".counter");
		if (counter.length > 0) {
			$counter.each(function () {
				var $elem = $(this);
				$elem.appear(function () {
					$elem.find('.timer').countTo();
				});
			});
		}
	};

	/*************************
       owl carousel
	*************************/

	POTENZA.carousel = function () {
		var owlslider = jQuery("div.owl-carousel");
		if ( owlslider.length > 0 ) {
			owlslider.each( function () {
			var $this = $(this),
				$items = ($this.data('items')) ? $this.data('items') : 1,
				$loop = ($this.attr('data-loop')) ? $this.data('loop') : true,
				$navdots = ($this.data('nav-dots')) ? $this.data('nav-dots') : false,
				$navarrow = ($this.data('nav-arrow')) ? $this.data('nav-arrow') : false,
				$autoplay = ($this.attr('data-autoplay')) ? $this.data('autoplay') : true,
				$autospeed = ($this.attr('data-autospeed')) ? $this.data('autospeed') : 5000,
				$smartspeed = ($this.attr('data-smartspeed')) ? $this.data('smartspeed') : 1500,
				$autohgt = ($this.data('autoheight')) ? $this.data('autoheight') : false,
				$space = ($this.attr('data-space')) ? $this.data('space') : 30,
				$animateOut = ($this.attr('data-animateOut')) ? $this.data('animateOut') : false;

				$(this).owlCarousel({
					loop: $loop,
					items: $items,
					responsive: {
						0: {
							items: $this.data('xx-items') ? $this.data('xx-items') : 1
						},
						480: {
							items: $this.data('xs-items') ? $this.data('xs-items') : 1
						},
						768: {
							items: $this.data('sm-items') ? $this.data('sm-items') : 2
						},
						980: {
							items: $this.data('md-items') ? $this.data('md-items') : 3
						},
						1200: {
							items: $items
						}
					},
					dots: $navdots,
					autoplayTimeout: $autospeed,
					smartSpeed: $smartspeed,
					autoHeight: $autohgt,
					margin: $space,
					nav: $navarrow,
					navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
					autoplay: $autoplay,
					autoplayHoverPause: true
				});
			});
		}
	}

	/*************************
        slickslider
	*************************/
	
	POTENZA.slickslider = function () {
		if ( $('.slider-for').exists() ) {
			$('.slider-for').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				asNavFor: '.slider-nav'
			});
			$('.slider-nav').slick({
				slidesToShow: 3,
				slidesToScroll: 1,
				asNavFor: '.slider-for',
				dots: false,
				centerMode: true,
				focusOnSelect: true
			});
		}
	};

	/*************************
		Magnific Popup
	*************************/
	
	POTENZA.mediaPopups = function () {
		if ($(".popup-single").exists() || $(".popup-gallery").exists() || $('.modal-onload').exists() || $(".popup-youtube, .popup-vimeo, .popup-gmaps").exists()) {
			if ($(".popup-single").exists()) {
				$('.popup-single').magnificPopup({
					type: 'image'
				});
			}

			if ( $(".popup-gallery").exists() ) {
				$('.popup-gallery').magnificPopup({
					delegate: 'a.portfolio-img',
					type: 'image',
					tLoading: 'Loading image #%curr%...',
					mainClass: 'mfp-img-mobile',
					gallery: {
						enabled: true,
						navigateByImgClick: true,
						preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
					}
				});
			}
			
			if ( $(".popup-youtube, .popup-vimeo, .popup-gmaps").exists() ) {
				$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
					disableOn: 700,
					type: 'iframe',
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: false,
					fixedContentPos: false
				});
			}
			
			var $modal = $('.modal-onload');
			if ( $modal.length > 0 ) {
				$('.popup-modal').magnificPopup({
					type: 'inline'
				});
				
				$(document).on('click', '.popup-modal-dismiss', function (e) {
					e.preventDefault();
					$.magnificPopup.close();
				});
		
				var elementTarget = $modal.attr('data-target');
				setTimeout(function () {
					$.magnificPopup.open({
						items: {
							src: elementTarget
						},
						type: "inline",
						mainClass: "mfp-no-margins mfp-fade",
						closeBtnInside: !0,
						fixedContentPos: !0,
						removalDelay: 500
					}, 0 )
				}, 1500 );
			}
		}
	}

	/*************************
		datetimepicker
	*************************/
	
	POTENZA.datetimepickers = function () {
		if ( $('.datetimepickers').exists() ) {
			$('#datetimepicker-01, #datetimepicker-02').datetimepicker({
				format: 'L'
			});
			$('#datetimepicker-03, #datetimepicker-04').datetimepicker({
				format: 'LT'
			});
		}
	};

	/*************************
		select2
	*************************/
	/*	
	POTENZA.select2 = function () {
		if ( $('.basic-select').exists() ) {
			var select = jQuery(".basic-select");
			if ( select.length > 0 ) {
				$('.basic-select').select2();
			}
		}
	};
	*/
	/*************************
		Range Slider
	*************************/
	
	POTENZA.rangesliders = function () {
		if ( $('.property-price-slider').exists() ) {
			var rangeslider = jQuery(".rangeslider-wrapper");
			$("#property-price-slider").ionRangeSlider({
				type: "double",
				min: 0,
				max: 10000,
				prefix: "$"
			});
		}
	};

	/*************************
	   Countdown
	*************************/
	
	POTENZA.countdownTimer = function () {
		if ($countdownTimer.exists()) {
			$countdownTimer.downCount({
				date: '12/25/2020 12:00:00', // Month/Date/Year HH:MM:SS
				offset: 400
			});
		}
	}

    /*************************
        BgSlider
	*************************/
	
	POTENZA.bgSlider = function () {
		var $bgSlider = $('#bg-slider');
		if ( $bgSlider.exists() ) {
			$("#bg-slider").kenburnsy({
				fullscreen: false
			});
		}
	}

	/*************************
		scrollbar
	*************************/
	
	POTENZA.scrollbar = function () {
		var scrollbar = jQuery(".scrollbar");
		if ( scrollbar.length > 0 ) {

			// Sidebar Scroll
			var scroll_light = jQuery(".scroll_light");
			if ( scroll_light.length > 0 ) {
				$( scroll_light ).niceScroll({
					cursorborder: 0,
					cursorcolor: "rgba(255,255,255,0.25)"
				});
				$(scroll_light).getNiceScroll().resize();
			}
		
			// Chat Scroll
			var scroll_dark = jQuery(".scroll_dark");
			if ( scroll_dark.length > 0 ) {
				$(scroll_dark).niceScroll({
					cursorborder: 0,
					cursorcolor: "rgba(0,0,0,0.1)"
				});
				$(scroll_dark).getNiceScroll().resize();
			}
		}
	}


	/*************************
		Secondary menu
	*************************/
	
	POTENZA.secondarymenu = function () {
		$(".secondary-menu ul li a[href^='#']").on('click', function(e) {

			// prevent default anchor click behavior
			e.preventDefault();

			// store hash
			var hash = this.hash;

			// animate
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			 }, 1000, function(){
				// when done, add hash to url
				// (default click behaviour)
				window.location.hash = hash;
			});
		});
	}


	/*************************
		Back to top
	*************************/
	
	POTENZA.goToTop = function () {
		var $goToTop = $('#back-to-top');
		$goToTop.hide();
		$window.scroll( function () {
			if ( $window.scrollTop() > 100 ) $goToTop.fadeIn();
			else $goToTop.fadeOut();
		});
		
		$goToTop.on("click", function () {
			$('body,html').animate({
				scrollTop: 0
			}, 1000);
			return false;
		});
	}

	/****************************************************
		POTENZA Window load and functions
	****************************************************/
	
	// Window load functions
	$window.on("load", function () {
		POTENZA.preloader();
	});
	
	// Document ready functions
	$document.ready(function () {
		POTENZA.counters(),
		POTENZA.slickslider(),
		POTENZA.datetimepickers(),
		// POTENZA.select2(),
		POTENZA.dropdownmenu(),
		POTENZA.isSticky(),
		POTENZA.scrollbar(),
		POTENZA.goToTop(),
		POTENZA.bgSlider(),
		POTENZA.countdownTimer(),
		POTENZA.secondarymenu(),
		POTENZA.mediaPopups(),
		POTENZA.rangesliders(),
		POTENZA.carousel();
	});
})(jQuery);

