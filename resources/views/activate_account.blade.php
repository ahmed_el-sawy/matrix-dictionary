@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('content')
<!--=================================
blog-detail -->
<section id="activate_account_page">
  <div class="container">
    <h1>Your Account Has Been Activated Successfully. You Can Login To Your Account From Here 
    <a class="btn btn-primary" href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal"><i class="far fa-user pr-2"></i>Sign in</a></h1>
  </div>
</section>
  <!--=================================
  blog-detail -->
  
@endsection