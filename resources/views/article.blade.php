@extends('layouts.app')

@section('meta')
<title>{{$article->title}}</title>
<meta property="description" content="{{substr(strip_tags($article->text), 0, 170)}}">

<meta property="og:title" content="{{$article->title}} - {{$article->user_info->name}}">
<meta property="og:description" content="{{substr(strip_tags($article->text), 0, 170)}}">
<meta property="og:image" content="{{asset($article->image)}}">
<meta property="og:url" content={{url()->current()}}">

<meta property="twitter:title" content="{{$article->title}} - {{$article->user_info->name}}">
<meta property="twitter:description" content="{{substr(strip_tags($article->text), 0, 170)}}">
<meta property="twitter:image" content="{{asset($article->image)}}">
<meta property="twitter:url" content={{url()->current()}}">

@endsection

@section('content')

<!--=================================
inner banner -->
<section class="header-inner header-inner-big bg-holder text-white" style="background-image: url({{asset('site/images/bg/banner-01.jpg')}});">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="text-primary">{{$article->title}}</h2>
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{url('/Blog')}}">Blog</a></li>
          <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span>{{$article->title}}</span></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!--=================================
inner banner -->


<!--=================================
blog-detail -->
<section class="space-ptb">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="blog-detail">
          <div class="blog-post">
            
            <div class="blog-post-image">
              <img class="img-fluid" src="{{asset($article->image)}}" alt="{{$article->title}}">
            </div>
            <div class="blog-post-footer border-0 justify-content-start">
              <div class="blog-post-time">
                <a href="{{url('View-Resume/'.$article->user)}}"> <i class="fas fa-user-edit"></i> {{$article->user_info->name}}</a>
              </div>
              <div class="blog-post-time">
                <a href="#"> <i class="far fa-clock"></i> {{date('d M Y', strtotime($article->created_at))}}</a>
              </div>
              
              <div class="blog-post-share ml-auto">
                <a class="facebook" href="https://www.facebook.com/share.php?v=4&u={{url()->current()}}" onclick="window.open(this.href,&quot;sharer&quot;,&quot;toolbar=0,status=0,width=626,height=436&quot;);return false;" rel="nofollow"><span><i class="fab fa-facebook-f"></i> Share</span></a>
                <a class="twitter" href="https://twitter.com/share?url={{url()->current()}}" onclick="window.open(this.href,&quot;sharer&quot;,&quot;toolbar=0,status=0,width=626,height=436&quot;);return false;" rel="nofollow" target="_blank"><span><i class="fab fa-twitter"></i> Share</span></a>
                <a class="linkedin" href="https://linkedin.com/sharing/share-offsite/?url={{url()->current()}}" onclick="window.open(this.href,&quot;sharer&quot;,&quot;toolbar=0,status=0,width=626,height=436&quot;);return false;" rel="nofollow" target="_blank"><span><i class="fab fa-linkedin-in"></i> Share</span></a>
                {{--
                <a class="email" target="_blank" href="mailto:test@gmail.com?subject={{$article->title}}" rel="nofollow"><span><i class="fa fa-envelope"></i> Email</span></a>
                <a class="print" href="Javascript:void(0)" onclick="print()" rel="nofollow" target="_blank"><span><i class="fa fa-print"></i> Print</span></a>
                <a class="whatsapp hidden-lg hidden-md" href="whatsapp://send?text={{url()->current()}}" rel="nofollow" target="_blank"><span><i class="fa fa-whatsapp"></i> Share</span></a>
                <a class="whatsapp hidden-sm hidden-xs" href="https://web.whatsapp.com/send?text={{url()->current()}}" rel="nofollow" target="_blank"><span><i class="fa fa-whatsapp"></i> Share</span></a>
                --}}
              </div>
            </div>
            
            <div class="blog-post-content mt-4">
              <div class="blog-post-description">{!! $article->text !!}</div>
              <nav class="navigation post-navigation">
                <div class="nav-links">
                    @if($prev_article !== NULL)
                      <div class="nav-previous">
                        <a href="{{url('Article/'.$prev_article->id)}}"><span class="pagi-text"> PREV</span><span class="nav-title"> {{$prev_article->title}}</span></a>
                      </div>
                    @endif
                    @if($next_article !== NULL)
                    <div class="nav-next @if($prev_article === NULL) ml-auto @endif">
                        <a href="{{url('Article/'.$next_article->id)}}"><span class="nav-title"> {{$next_article->title}}</span> <span class="pagi-text">NEXT</span></a> 
                    </div>
                    @endif
                </div>
                </nav>
                {{--
                <div class="mt-4">
                  <h5 class="mb-3">About Author</h5>
                  <div class="border p-4">
                    <div class="d-sm-flex">
                      <div class="avatar avatar-xlll mb-3 mb-sm-0">
                        <img class="img-fluid rounded-circle" src="images/avatar/01.jpg" alt="">
                      </div>
                      <div class="pl-sm-4">
                        <h6 class="mb-3"> <span class="text-primary"> Posted by:</span> Alice Williams</h6>
                        <p>SMART is an acronym for Specific, Measurable, Achievable, Realistic and Time Sensitive – S-M-A-R-T. Knowing what you want and setting SMART goals as mileposts on your quest cannot help but give you clarity!</p>
                        <div class="social-icon d-flex">
                          <span>Follow us:</span>
                          <ul class="list-unstyled mb-0 ml-3 list-inline">
                            <li class="list-inline-item"> <a href="#"> <i class="fab fa-facebook-f"></i> </a> </li>
                            <li class="list-inline-item"> <a href="#"> <i class="fab fa-twitter"></i> </a> </li>
                            <li class="list-inline-item"> <a href="#"> <i class="fab fa-instagram"></i> </a> </li>
                            <li class="list-inline-item"> <a href="#"> <i class="fab fa-linkedin"></i> </a> </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                --}}
                <div class="mt-4">
                  <h5 class="mb-3">Related Post</h5>
                  <div class="row">
                    <div class="col-12">
                      <div class="owl-carousel " data-nav-dots="true" data-items="2" data-md-items="2" data-sm-items="1" data-xs-items="1" data-xx-items="1" data-space="15">
                        @foreach ($related_posts as $ppo)
                        <div class="item">
                          <div class="blog-post text-center">
                            <div class="blog-post-image">
                              <img class="img-fluid" src="{{asset($ppo->image)}}" alt="{{$ppo->title}}">
                            </div>
                            <div class="blog-post-content">
                              <div class="blog-post-details">
                                <div class="blog-post-title">
                                  <h5> <a href="{{url('Article/'.$ppo->id)}}">{{$ppo->title}}</a></h5>
                                </div>
                                <div class="blog-post-description mb-0">
                                  <p class="mb-0">{{substr(strip_tags($article->text), 0, 80)}}</p>
                                </div>
                              </div>
                              {{--
                              <div class="blog-post-footer">
                                <div class="blog-post-time">
                                  <a href="#"> <i class="far fa-clock"></i> {{date('d M Y', strtotime($ppo->created_at))}}</a>
                                </div>
                              </div>
                              --}}
                            </div>
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>

                  <hr class="my-5" />
                    @foreach ($article->comments as $comment)
                    <div class="media mb-3">
                    <div class="avatar avatar-lg">
                        @if($comment->user_info->avatar != '')
                            <img class="img-fluid  rounded-circle" src="{{asset($comment->user_info->avatar)}}" alt="{{$comment->user_info->name}}">
                          @else
                            <img class="img-fluid  rounded-circle" src="{{asset('site/images/avatar/00.jpg')}}" alt="{{$comment->user_info->name}}">
                          @endif
                    </div>
                    <div class="media-body ml-3 border p-4">
                      <div class="d-flex">
                        <h6 class="mt-0">{{$comment->user_info->name}}</h6>
                        {{-- <a class="ml-auto" href="#"><i class="fas fa-reply pr-2"></i>Reply</a> --}}
                        <span class="ml-auto">{{$comment->created_at->diffForHumans()}}</span>
                      </div>
                      <p class="mb-0">{{$comment->comment}}</p>
                    </div>
                  </div>
                    @endforeach
                    @if(Auth::check())
                        <div class="mt-4">
                    <h5 class="mb-4">Leave a Reply</h5>
                    <form id="article_comment" method="post" action="{{url('article_comment')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="article" value="{{$article->id}}" />
                        <div id="article_comment_red"></div>
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <textarea class="form-control" rows="4" placeholder="Your Comment" name="comment"></textarea>
                        </div>
                        <div class="col-md-12">
                          <button type="submit" class="btn btn-primary">Publish Comment</a>
                        </div>
                      </div>
                    </form>
                  </div>
                    @endif
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mt-5 mt-lg-0">
          <div class="blog-sidebar">
            {{--
            <div class="widget">
              <div class="widget-title">
                <h5>About The Blog</h5>
              </div>
              <p>Trying to go through life without clarity is similar to sailing a rudder-less ship – no good thing can or will happen!</p>
              <ol class="pl-3">
                <li class="mb-2">Success is something of which we all want.</li>
                <li class="mb-2">Most people believe that success is difficult.</li>
                <li class="mb-2">They’re wrong – it’s not!</li>
              </ol>
            </div>
            --}}
            <div class="widget">
              <div class="widget-title">
                <h5>Recent Articles</h5>
              </div>
              @foreach ($latarts as $latar)
                <div class="d-flex mb-3 align-items-start">
                    <div class="avatar avatar-xl">
                      <img class="img-fluid" src="{{asset($latar->image)}}" alt="{{$latar->title}}">
                    </div>
                    <div class="ml-3 recent-posts">
                      <a href="{{url('Article/'.$latar->id)}}"><b>{{$latar->title}}</b></a>
                      <a class="d-block font-sm mt-1 text-light" href="#">{{date('d M Y' ,strtotime($latar->created_at))}}</a>
                    </div>
                  </div>
              @endforeach  
            </div>
            {{--
            <div class="widget">
              <div class="widget-title">
                <h5>Subscribe & Follow</h5>
              </div>
              <div class="social">
                <ul class="list-unstyled">
                  <li class="facebook">
                    <a class="text-uppercase" href="#"> <i class="fab fa-facebook-f mr-3"></i>Facebook</a>
                    <a class="follow ml-auto" href="#">Like </a>
                  </li>
                  <li class="twitter">
                    <a class="text-uppercase" href="#"> <i class="fab fa-twitter mr-3"></i>twitter</a>
                    <a class="follow ml-auto" href="#">Followers </a>
                  </li>
                  <li class="youtube">
                    <a class="text-uppercase" href="#"> <i class="fab fa-youtube mr-3"></i>youtube</a>
                    <a class="follow ml-auto" href="#">Subscribers </a>
                  </li>
                  <li class="instagram">
                    <a class="text-uppercase" href="#"> <i class="fab fa-instagram mr-3"></i>instagram</a>
                    <a class="follow ml-auto" href="#">Followers </a>
                  </li>
                  <li class="linkedin">
                    <a class="text-uppercase" href="#"> <i class="fab fa-linkedin-in mr-3"></i>linkedin</a>
                    <a class="follow ml-auto" href="#">Followers </a>
                  </li>
                </ul>
              </div>
            </div>
            --}}
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--=================================
  blog-detail -->
  
@endsection