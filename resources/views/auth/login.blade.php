@extends('layouts.app')

@section('content')

<!--=================================
inner banner -->
<section class="header-inner header-inner-big bg-holder text-white text-center" style="background-image: url({{asset('site/images/bg/banner-01.jpg')}});">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="text-primary">Login</h2>
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{url('/')}}"> Home </a></li>
          <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Login </span></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!--=================================
inner banner -->


<!--=================================
Register -->
<section class="space-ptb">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-8 col-lg-10 col-md-12">
        <div class="login-register">
         <div class="section-title">
          <h4 class="text-center">{{ __('Login') }}</h4>
          </div>
          <div class="tab-content">
            <div class="tab-pane active" id="candidate" role="tabpanel">
                <form class="mt-4" method="post" action="{{url('Login')}}" id="login_form">
                {{csrf_field()}}
                <div id="login_results"></div>


                <div class="form-row">
                  <div class="form-group col-12">
                    <label for="email">Email Address:</label>
                    <input type="email" class="form-control" id="email" name="email" />
                  </div>
                  <div class="form-group col-12">
                    <label for="password">Password*</label>
                    <input type="password" class="form-control" id="password" name="password" />
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                  </div>
                  <div class="col-md-6">
                    <div class="ml-md-3 mt-3 mt-md-0 forgot-pass">
                      <a href="#">Forgot Password?</a>
                      <p class="mt-1">Don't have account? <a href="{{url('Register')}}">Sign Up here</a></p>
                    </div>
                  </div>
                </div>
              </form>
                </div>
            </div>
        </div>
    </div>
</div>

  </div>
</section>
@endsection
