@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('content')

<!--=================================
inner banner -->
<section class="header-inner header-inner-big bg-holder text-white" style="background-image: url({{asset('site/images/bg/banner-01.jpg')}});">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="text-primary">Blog</h2>
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
          <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span>Blog</span></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!--=================================
inner banner -->

<!--=================================
blog -->
<section class="space-ptb">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        @foreach ($articles as $article)
            <div class="blog-post text-center">
          <div class="blog-post-image">
            <img class="img-fluid" src="{{asset($article->image)}}" alt="{{$article->title}}">
          </div>
          <div class="blog-post-content">
            <div class="blog-post-details">
              <div class="blog-post-title row">
                <a href="{{url('Article/'.$article->id)}}" class="blog_title col-6 text-left"><h4>{{$article->title}}</h4></a>
                <div class="col-6 text-right">
                    <a href="{{url('View-Resume/'.$article->user)}}" class="blog_time mr-5"><i class="fas fa-user-edit"></i> {{$article->user_info->name}}</a>
                    <a href="#" class="blog_time"><i class="far fa-clock"></i> {{date('d M Y', strtotime($article->created_at))}}</a>
                </div>
              </div>
              <div class="blog-post-description">
                <p class="mb-0">{{substr(strip_tags($article->text), 0, 250)}} ...<a class="ml-1 btn-link d-inline-block" href="{{url('Article/'.$article->id)}}">Continue read</a></p>
              </div>
            </div>
            {{--
            <div class="blog-post-footer">
              <div class="blog-post-time">
                <a href="#"><i class="far fa-clock"></i>{{date('d M Y', strtotime($article->created_at))}}</a>
              </div>
                  <div class="blog-post-author">
                    <span>By<a href="#"><img src="images/avatar/03.jpg" alt="">Michael Bean</a></span>
                  </div>
                  <div class="blog-post-time">
                    <a href="#"><i class="far fa-comment"></i>(4)</a>
                  </div>
                  <div class="blog-post-share">
                    <div class="share-box">
                      <a href="#"> <i class="fas fa-share-alt"></i><span class="pl-2">Share</span></a>
                      <ul class="list-unstyled share-box-social">
                        <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
                        <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
                        <li> <a href="#"><i class="fab fa-linkedin"></i></a> </li>
                        <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
                        <li> <a href="#"><i class="fab fa-pinterest"></i></a> </li>
                      </ul>
                    </div>
                  </div>
            </div>
            --}}
          </div>
        </div>
        @endforeach
        <div class="row justify-content-center">
          <div class="col-12 text-center">
            {{$articles->links()}}
          </div>
        </div>
      </div>
      <div class="col-lg-4 mt-4 mt-lg-0">
        <div class="blog-sidebar">
          {{--
          <div class="widget">
            <div class="widget-title">
              <h5>About The Blog</h5>
            </div>
            <p>Trying to go through life without clarity is similar to sailing a rudder-less ship!</p>
            <ol class="pl-3">
              <li class="mb-2">Success is something of which we all want.</li>
              <li class="mb-2">Most people believe that success is difficult.</li>
              <li class="mb-2">They’re wrong – it’s not!</li>
            </ol>
          </div>
          --}}
          <div class="widget">
            <div class="widget-title">
              <h5>Recent Articles</h5>
            </div>
            @foreach ($latarts as $latar)
                <div class="d-flex mb-3 align-items-start">
                    <div class="avatar avatar-xl">
                      <img class="img-fluid" src="{{asset($latar->image)}}" alt="{{$latar->title}}">
                    </div>
                    <div class="ml-3 recent-posts">
                      <a href="{{url('Article/'.$latar->id)}}"><b>{{$latar->title}}</b></a>
                      <a class="d-block font-sm mt-1 text-light" href="#">{{date('d M Y' ,strtotime($latar->created_at))}}</a>
                    </div>
                  </div>
            @endforeach  
          </div>
          {{--
          <div class="widget">
            <div class="widget-title">
              <h5>Subscribe & Follow</h5>
            </div>
            <div class="social">
              <ul class="list-unstyled">
                <li class="facebook">
                  <a class="text-uppercase" href="#"> <i class="fab fa-facebook-f mr-3"></i>Facebook</a>
                  <a class="follow ml-auto" href="#">Like </a>
                </li>
                <li class="twitter">
                  <a class="text-uppercase" href="#"> <i class="fab fa-twitter mr-3"></i>twitter</a>
                  <a class="follow ml-auto" href="#">Followers </a>
                </li>
                <li class="youtube">
                  <a class="text-uppercase" href="#"> <i class="fab fa-youtube mr-3"></i>youtube</a>
                  <a class="follow ml-auto" href="#">Subscribers </a>
                </li>
                <li class="instagram">
                  <a class="text-uppercase" href="#"> <i class="fab fa-instagram mr-3"></i>instagram</a>
                  <a class="follow ml-auto" href="#">Followers </a>
                </li>
                <li class="linkedin">
                  <a class="text-uppercase" href="#"> <i class="fab fa-linkedin-in mr-3"></i>linkedin</a>
                  <a class="follow ml-auto" href="#">Followers </a>
                </li>
              </ul>
            </div>
          </div>
          --}}
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
blog -->

@endsection