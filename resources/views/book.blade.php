@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('styles')
<style>
    .hidden_footer_section {display: none;}
    .footer.footer.bg-light {margin: 0; padding: 0;}
</style>
@endsection

@section('content')

<!--=================================
banner -->
<section class="header-inner header-inner-big bg-holder text-white" style="background-image: url({{asset('site/images/bg/banner-01.jpg')}});">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="job-search-field">
          <div class="job-search-item">
            <form class="form row" method="get" action="{{url('Library')}}">
              <div class="col-lg-5">
                <div class="form-group left-icon">
                  <input type="text" class="form-control" name="search" placeholder="Enter Keyword ..." value="{{$search_filter}}">
                  <i class="fas fa-search"></i>
                  </div>
              </div>
              <div class="col-lg-5">
                <select class="form-control" name="cat[]">
                  <option value="All">All Categories</option>
                  @foreach ($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->title}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-2 col-sm-12">
                <div class="form-group form-action">
                  <button type="submit" class="btn btn-primary mt-0"><i class="fas fa-search-location"></i> Search</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
banner -->



<!--=================================
job-list -->
<section class="space-ptb">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
          <h1 class="text-center mb-5">{{$book->title}}</h1>
          
          <div class="FramePDFWrapper">
                @if(!Auth::check())
                    <div id="book_overlay">
                        <div id="book_overlay_content">
                            <h2>Login before viewing this file</h2>
                            <a href="{{url('Login')}}" data-target="#LoginModal" data-toggle="modal" class="btn btn-primary">Login from here</a>
                        </div>
                    </div>
                @endif
                <iframe src="{{asset($book->file)}}#toolbar=0" oncontextmenu="return false;"></iframe>
                
          </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
job-list -->

@endsection
