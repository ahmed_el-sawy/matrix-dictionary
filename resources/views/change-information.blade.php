@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('styles')
<style>
    .hidden_footer_section {display: none;}
    .footer.footer.bg-light {margin: 0; padding: 0;}
</style>
@endsection

@section('content')

@include('profile_menu')


<!--=================================
My Profile -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="user-dashboard-info-box">
          <div class="section-title-02 mb-4">
            <h4>My Information</h4>
          </div>

          <form method="post" action="{{url('Change-Information')}}" enctype="multipart/form-data">
				{{csrf_field()}}
                @if($errors->any())
                  <div class="alert alert-danger">{{$errors->first()}}</div>
                @endif
                @if(session('success'))
                  <div class="alert alert-success">{{session('success')}}</div>
                @endif
             <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="register_name">Name</label>
                    <input type="text" class="form-control" name="name" id="register_name" value="{{Auth::user()->name}}" />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_mobile">Mobile</label>
                    <input type="text" class="form-control" name="mobile" id="register_mobile" value="{{Auth::user()->mobile}}" />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_email">Email</label>
                    <input type="email" class="form-control" name="email" id="register_email" value="{{Auth::user()->email}}"  />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_country">Country</label>
                    <select class="form-control" name="country" id="register_country">
                      <option value="" disabled selected>Choose Country</option>
                      @foreach ($countries as $country)
                        <option value="{{$country->id}}" @if($country->id == Auth::user()->country) selected @endif>{{$country->country}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_job">Job Title</label>
                    <select class="form-control" name="job" id="register_job">
                      <option value="" disabled selected>Choose Job Title</option>
                      @foreach ($jobs as $job)
                        <option value="{{$job->id}}" @if($job->id == Auth::user()->job) selected @endif>{{$job->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_language">Language</label>
                    <select class="form-control" name="language" id="register_language">
                      <option value="" disabled selected>Choose Language</option>
                      @foreach ($languages as $language)
                        <option value="{{$language->id}}" @if($language->id == Auth::user()->language) selected @endif>{{$language->language}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_language">Image</label>
          					<input type="file" name="image" class="form-control" />
                  </div>
                  <div class="form-group col-md-6">                    
                    <label for="show_personal_info" id="show_personal_info_pp"><input type="checkbox" name="show_personal_info" @if(Auth::user()->pinfo == 1) checked @endif /> Show Personal Info At Resume</label>
                  </div>                  
            	</div>
             <button class="btn btn-md btn-primary" type="submit">Save Settings</button>
                    
          </form>
        </div>




        </div>

      </div>
    </div>
  </div>
</section>
<!--=================================
My Profile -->

@endsection