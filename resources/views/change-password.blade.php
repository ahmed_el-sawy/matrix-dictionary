@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('styles')
<style>
    .hidden_footer_section {display: none;}
    .footer.footer.bg-light {margin: 0; padding: 0;}
</style>
@endsection

@section('content')

@include('profile_menu')


<!--=================================
Change Password -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="user-dashboard-info-box">
          <div class="section-title-02 mb-4">
            <h4>Change Password</h4>
          </div>
          <div class="row">
            <div class="col-12">
            <form method="post" action="{{url('Change-Password')}}" enctype="multipart/form-data">
              {{csrf_field()}}
              @if($errors->any())
                <div class="alert alert-danger">{{$errors->first()}}</div>
              @endif
              @if(session('success'))
                <div class="alert alert-success">{{session('success')}}</div>
              @endif
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label>New Password</label>
                  <input type="password" class="form-control" name="password" value="">
                </div>
                <div class="form-group col-md-12">
                  <label>Confirm Password</label>
                  <input type="password" class="form-control" name="password_confirmation" value="">
                </div>
                </div>
                <button type="submit" class="btn btn-primary d-block">Save Password</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Change Password -->


@endsection
