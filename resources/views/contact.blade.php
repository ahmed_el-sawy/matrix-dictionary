@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('content')

<!--=================================
inner banner -->
<section class="header-inner header-inner-big bg-holder text-white" style="background-image: url({{asset('site/images/bg/banner-01.jpg')}});">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="text-primary">Contact Us</h2>
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
          <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span>Contact Us</span></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!--=================================
inner banner -->


<!--=================================
blog-detail -->
<section class="space-ptb">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="blog-detail">
          <div class="blog-post">
            <div class="blog-post-content">
              <h5 class="mb-4">Get In Touch</h5>
              <form id="article_comment" method="post" action="{{url('Contact-Us')}}">
                  {{csrf_field()}}
                  <div id="article_comment_red"></div>
                <div class="form-row">
                  <div class="form-group col-md-6 col-sm-12">
                    <input type="text" name="name" class="form-control" placeholder="Your Name" />
                  </div>
                  <div class="form-group col-md-6 col-sm-12">
                    <input type="email" name="email" class="form-control" placeholder="E-mail Address" />
                  </div>
                  <div class="form-group col-md-6 col-sm-12">
                    <input type="text" name="phone" class="form-control" placeholder="Phone Number" />
                  </div>
                  <div class="form-group col-md-6 col-sm-12">
                    <input type="text" name="subject" class="form-control" placeholder="Subject" />
                  </div>
                <div class="form-group col-md-12">
                    <textarea class="form-control" rows="4" placeholder="Your Message" name="message"></textarea>
                  </div>
                  <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-primary">Send</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </section>
  <!--=================================
  blog-detail -->
  
@endsection