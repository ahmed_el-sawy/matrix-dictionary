@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content -->
<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">
        <!--end: Form Wizard Nav -->
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid">
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_user_add_form" method="POST" action="{{ route('dictionaries.store') }}" enctype="multipart/form-data">
                            <!--begin: Form Wizard Step 1-->
                            @csrf
                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                                data-ktwizard-state="current">
                                <div class="kt-heading kt-heading--md">Add new Dictionary :</div>
                                <div class="kt-section kt-section--first">
                                    <div class="kt-wizard-v4__form">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="kt-section__body">
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Title
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="text" name="title" value="{{ old('title') }}" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source Language
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="source_lang">
                                                                    <option value="">Choose Language</option>
                                                                    @foreach($languages as $language)
                                                                        <option value="{{ $language->id }}">
                                                                            {{ $language->language }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Target Language
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="target_lang">
                                                                    <option value="">Choose Language</option>
                                                                    @foreach($languages as $language)
                                                                        <option value="{{ $language->id }}">
                                                                            {{ $language->language }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Category
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="category" id="category_selector" data-url="{{url('admin/sub_categories_dropdown')}}">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{ $category->id }}">
                                                                            {{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Sub Category
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control"
                                                                        name="sub_category" id="sub_category_selector">
                                                                    <option value="">Choose Category</option>
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Status
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="form-group">
                                                                <div class="kt-radio-inline">
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio" name="status" value="1" /> Active
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio" name="status" value="0" checked /> Not Active
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Actions -->
                            <div class="kt-form__actions">
                                <button type="submit"
                                    class="btn btn-primary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Add</button>
                            </div>
                            <!--end: Form Actions -->
                        </form>
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
