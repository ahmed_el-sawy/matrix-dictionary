@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content -->
<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">
        <!--end: Form Wizard Nav -->
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid">
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_user_add_form" method="POST" action="{{ route('books.update', $data->id) }}" enctype="multipart/form-data">
                            <!--begin: Form Wizard Step 1-->
                            @csrf
                            @method('PUT')
                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                                data-ktwizard-state="current">
                                <div class="kt-heading kt-heading--md">Edit Book :</div>
                                <div class="kt-section kt-section--first">
                                    <div class="kt-wizard-v4__form">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="kt-section__body">
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Book File
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="file" name="book" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Title
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="text" name="title" class="form-control" value="{{ $data->title }}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="text" name="source" class="form-control" value="{{ $data->source }}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Copy Right
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="text" name="copyRight" class="form-control" value="{{ $data->copyRight }}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Author
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="text" name="author" class="form-control" value="{{ $data->author }}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Language
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="language">
                                                                    @foreach($languages as $language)
                                                                        <option value="{{ $language->id }}" {{ $language->id == $data->language ? 'selected' : '' }}>
                                                                            {{ $language->language }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Category
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="category">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{ $category->id }}" {{ $language->id == $data->category ? 'selected' : '' }}>
                                                                            {{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Sub Category
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="sub_category">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach($sub_categories as $category)
                                                                        <option value="{{ $category->id }}" {{ $language->id == $data->sub_category ? 'selected' : '' }}>
                                                                            {{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Status
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="form-group">
                                                                <div class="kt-radio-inline">
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio" name="status" value="1" {{ $data->status == 1 ? 'checked' : '' }} /> Active
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio" name="status" value="0" {{ $data->status == 0 ? 'checked' : '' }} /> Not Active
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Actions -->
                            <div class="kt-form__actions">
                                <button type="submit"
                                    class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Save</button>
                            </div>
                            <!--end: Form Actions -->
                        </form>
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
