@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Books
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">{{ $data->count() }} </span>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <a data-toggle="modal" data-target="#NewBook" href="#" class="btn btn-label-brand btn-bold">Add new Book </a>
        

            
        </div>
    </div>
</div>

<div class="modal fade" id="NewBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add New Book</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form class="kt-form" id="kt_user_add_form" method="POST" action="{{ route('books.store') }}" enctype="multipart/form-data">
                                <!--begin: Form Wizard Step 1-->
                                @csrf
                                <div id="kt_user_add_form_result"></div>
                                <div class="row">
										<div class="form-group col-12 col-md-6">
											<label class="col-form-label">Book File</label>                                                            
                                            <input type="file" name="book" class="form-control"/>
                                        </div>
                                    
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Title</label>
										<input type="text" name="title" class="form-control" value="{{ old('title') }}"/>
									</div>
                                	<div class="form-group col-12 col-md-6">
                                    	<label class="col-form-label">Source</label>
										<input type="text" name="source" class="form-control" value="{{ old('source') }}"/>
                                  	</div>
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Copy Right</label>
                                        <input type="text" name="copyRight" class="form-control" value="{{ old('copyRight') }}"/>
                                   	</div>
                                    <div class="form-group col-12 col-md-6">
                                    	<label class="col-form-label">Author</label>
										<input type="text" name="author" class="form-control" value="{{ old('author') }}"/>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Language</label>
										<select class="form-control" name="language">
											<option value="">Choose Language</option>
                                            @foreach($languages as $language)
                                            	<option value="{{ $language->id }}">{{ $language->language }}</option>
                                            @endforeach
										</select>
									</div>
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Category</label>
										<select class="form-control" name="category">
											<option value="">Choose Category</option>
                                            @foreach($categories as $category)
												<option value="{{ $category->id }}">{{ $category->title }}</option>
											@endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Sub Category</label>
										<select class="form-control" name="sub_category">
											<option value="">Choose Category</option>
                                            @foreach($sub_categories as $category)
                                            	<option value="{{ $category->id }}">{{ $category->title }}</option>
											@endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
										<label class="col-form-label">Status</label>
											<div class="kt-radio-inline">
                                                                        <label class="kt-radio">
                                                                            <input type="radio" class="custom-radio" name="status" value="1" checked /> Active
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="kt-radio">
                                                                            <input type="radio" class="custom-radio" name="status" value="0" /> Not Active
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>


                                <div class="kt-form__actions">
                                    <button type="submit" class="btn btn-primary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Save</button>
                                </div>
                                <!--end: Form Actions -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

<!-- end:: Content Head -->
<div class="kt-portlet kt-portlet--mobile p-5">
    <div class="kt-portlet__body kt-portlet__body--fit">
        	<table class="table table-bordered data_table">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Source File</th>
								<th>Status</th>
								<th>Added At</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($data as $book)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$book->title}}</td>
									<td><a href="{{asset($book->file)}}" target="_blank"><i class="fas fa-download"></i></a></td>
									<td>@if($book->status == 1) <span class="badge badge-success">Active</span> @else <span class="badge badge-danger">Not Active</span> @endif</td>
									<td>{{date('d/m/Y', strtotime($book->created_at))}}</td>
									<td>
									<button class="btn btn-brand dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" 
									aria-haspopup="true" aria-expanded="true">Action</button>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" data-toggle="modal" href="#myModalRR-{{ $book->id }}"><i class="fa fa-edit"></i> Edit</a>
									<a class="dropdown-item" data-toggle="modal" href="#myModal-{{ $book->id }}"><i class="fa fa-trash"></i> Delete</a>
									</div>
									<div class="modal fade" id="myModal-{{ $book->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
										<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Delete Book</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
										<form role="form" action="{{ url('/admin/books/'.$book->id) }}" class="" method="POST">
										<input name="_method" type="hidden" value="DELETE">
										{{ csrf_field() }}
										<p>are you sure</p>
										<button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i> delete</button>
										</form>
										</div>
										</div>
										</div>
									</div>
                                    
                                    <div class="modal fade" id="myModalRR-{{ $book->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Book</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form class="kt-form kt_user_edit_form" data-book="{{$book->id}}" method="POST" action="{{ route('books.update', $book->id) }}" enctype="multipart/form-data">
                                <!--begin: Form Wizard Step 1-->
                                @csrf
                                @method('PUT')
                                <div id="kt_user_edit_form_result_{{$book->id}}"></div>
                                <div class="row">
										<div class="form-group col-12 col-md-6">
											<label class="col-form-label">Book File</label>                                                            
                                            <input type="file" name="book" class="form-control"/>
                                        </div>
                                    
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Title</label>
										<input type="text" name="title" class="form-control" value="{{ $book->title }}"/>
									</div>
                                	<div class="form-group col-12 col-md-6">
                                    	<label class="col-form-label">Source</label>
										<input type="text" name="source" class="form-control" value="{{ $book->source }}"/>
                                  	</div>
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Copy Right</label>
                                        <input type="text" name="copyRight" class="form-control" value="{{ $book->copyRight }}"/>
                                   	</div>
                                    <div class="form-group col-12 col-md-6">
                                    	<label class="col-form-label">Author</label>
										<input type="text" name="author" class="form-control" value="{{ $book->author }}"/>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Language</label>
										<select class="form-control" name="language">
											<option value="">Choose Language</option>
                                            @foreach($languages as $language)
                                            	<option value="{{ $language->id }}" @if($book->language ==  $language->id) selected @endif>{{ $language->language }}</option>
                                            @endforeach
										</select>
									</div>
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Category</label>
										<select class="form-control" name="category">
											<option value="">Choose Category</option>
                                            @foreach($categories as $category)
												<option value="{{ $category->id }}" @if($book->category ==  $category->id) selected @endif>{{ $category->title }}</option>
											@endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
										<label class="col-form-label">Sub Category</label>
										<select class="form-control" name="sub_category">
											<option value="">Choose Category</option>
                                            @foreach($sub_categories as $category)
                                            	<option value="{{ $category->id }}" @if($book->sub_category ==  $category->id) selected @endif>{{ $category->title }}</option>
											@endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
										<label class="col-form-label">Status</label>
											<div class="kt-radio-inline">
                                                                        <label class="kt-radio">
                                                                            <input type="radio" class="custom-radio" name="status" value="1"  @if($book->status == 1) checked @endif /> Active
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="kt-radio">
                                                                            <input type="radio" class="custom-radio" name="status" value="0" @if($book->status == 0) checked @endif /> Not Active
                                                                            <span></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>


                                <div class="kt-form__actions">
                                    <button type="submit" class="btn btn-primary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Save</button>
                                </div>
                                <!--end: Form Actions -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
									</td>
								</tr>
							@endforeach
							
						</tbody>
					</table>
    </div>
</div>
@endsection
@section('footer-js')
    <script type="text/javascript">
	
	
		$(document).ready(function() { 
			$('body').on('submit', '#kt_user_add_form', function(e) {
			  e.preventDefault(); 
			  $('#kt_user_add_form_result').html('<i class="fas fa-spinner fa-pulse mr-3"></i>');
			  var action = $(this).attr('action');
			  var formData = new FormData($(this)[0]);
			  $.ajax({
				  type: 'POST',
				  data: formData,
				  async: true,
				  cache: false,
				  contentType: false,
				  processData: false,
				  url: action,
				  error: function(data) {
					  jQuery.each(data.errors, function(key, value){
						  $('#kt_user_add_form_result').html('<div class="alert alert-danger">'+value+'</div>');	
					  });
				  },
				  success: function(data) 
				  {
					  if(data.success)
					  {
							location.reload();
					  }
					  else
					  {
						  $('#kt_user_add_form_result').html('<div class="alert alert-danger">'+data.errors+'</div>');	
					  }
				  }
			  });
			  return false;
			});

			$('body').on('submit', '.kt_user_edit_form', function(e) {
			  e.preventDefault(); 
			  var aa = $(this).attr('data-book');
			  $('#kt_user_edit_form_result_'+aa).html('<i class="fas fa-spinner fa-pulse mr-3"></i>');
			  var action = $(this).attr('action');
			  var formData = new FormData($(this)[0]);
			  $.ajax({
				  type: 'POST',
				  data: formData,
				  async: true,
				  cache: false,
				  contentType: false,
				  processData: false,
				  url: action,
				  error: function(data) {
					  jQuery.each(data.errors, function(key, value){
						  $('#kt_user_edit_form_result_'+aa).html('<div class="alert alert-danger">'+value+'</div>');	
					  });
				  },
				  success: function(data) 
				  {
					  if(data.success)
					  {
							location.reload();
					  }
					  else
					  {
						  $('#kt_user_edit_form_result_'+aa).html('<div class="alert alert-danger">'+data.errors+'</div>');	
					  }
				  }
			  });
			  return false;
			});
			
		  }); 
        var datatable = $('.data_table').DataTable({"iDisplayLength": 25});
    </script>
@endsection

