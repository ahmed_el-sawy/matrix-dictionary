@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content -->
<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">
        <!--end: Form Wizard Nav -->
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid">
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_user_add_form" method="POST" action="{{ route('categories.store') }}" enctype="multipart/form-data">
                            <!--begin: Form Wizard Step 1-->
                            @csrf
                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                                data-ktwizard-state="current">
                                <div class="kt-heading kt-heading--md">Add new category :</div>
                                <div class="kt-section kt-section--first">
                                    <div class="kt-wizard-v4__form">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="kt-section__body">
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Title</label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <input class="form-control" name="title" type="text" value="{{ old('title') }}">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Icon</label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <input class="form-control" name="icon" type="file" />
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Type
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="form-group">
                                                                <div class="kt-radio-inline">
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio category_checker" name="type" 
                                                                        value="0" checked /> Main
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio category_checker" name="type" 
                                                                        value="1" /> Sub
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row d-none" id="category_checker_red">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Sub From
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="form-input">
                                                                <select name="sub_from" class="form-control select2 selectpicker">
                                                                    <option value="0">Choose Category</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{$category->id}}">{{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Actions -->
                            <div class="kt-form__actions">
                                <button type="submit"
                                    class="btn btn-primary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Add</button>
                            </div>
                            <!--end: Form Actions -->
                        </form>
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection


@section('footer-js')
<script>
$(document).ready(function(){
        $('body').on('change', '.category_checker', function(){
        var aa = $(this).val();

        if(aa == '0')
        {
            $('#category_checker_red').addClass('d-none');
        }
        else
        {
            $('#category_checker_red').removeClass('d-none');
        }
	});
});
</script>
@endsection
