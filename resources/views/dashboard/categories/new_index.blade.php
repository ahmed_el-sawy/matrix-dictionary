@extends('layouts.dashboard')
@section('content')
<style>
    .kt-datatable {display: block;}
    .table.kt-datatable {display: table;}
    .main_category_row {cursor: pointer;}
    .sub_category_row {background: #efefef; display: none;}
    .sub_category_row.d-show {display: table-row;}
    .active_row {background: #c7e4f1;}
</style>
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Categories @if($main_category != NULL) -  {{$main_category->title}} @endif
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total"> {{ $data }} </span>
                {{--
                <form class="kt-margin-l-20" id="kt_subheader_search_form">
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                        <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path
                                            d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path
                                            d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                            fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>

                                <!--<i class="flaticon2-search-1"></i>-->
                            </span>
                        </span>
                    </div>
                </form>
                --}}                
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <a href="#" class="">
            </a>
            <a href="{{ route('categories.create') }}" class="btn btn-label-brand btn-bold">
                Add new category </a>
        </div>
    </div>
</div>

<!-- end:: Content Head -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body kt-portlet__body--fit">
        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" id="kt_apps_user_list_datatable" style="display: block; padding: 15px;">
            <table class="table kt-datatable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @php $i = 1; @endphp
                @foreach ($cats as $cat)
                    <tr class="main_category_row" data-num="{{$cat->id}}">
                        <td>{{$loop->iteration}}</td>
                        <td>{{$cat->title}}</td>
                        <td>
                            <div class="dropdown">
                              <a href="#" class="btn btn-hover-brand btn-icon btn-pill" data-toggle="dropdown">
                                  <i class="la la-ellipsis-h"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{url('admin/categories/'.$cat->id.'/edit')}}"><i class="la la-edit"></i> Edit</a>
                                <a class="dropdown-item" href="{{url('admin/categories/'.$cat->id.'/delete')}}"><i class="la la-trash"></i> Delete</a>
                              </div>
                          </div>
                        </td>
                    </tr>
                    @foreach ($cat->subs as $sc)
                    <tr class="sub_category_row sub_category_row{{$cat->id}}" data-num="{{$sc->id}}">
                        <td>{{$i}}.{{$loop->iteration}}</td>
                        <td>{{$sc->title}}</td>
                        <td>
                            <div class="dropdown">
                              <a href="#" class="btn btn-hover-brand btn-icon btn-pill" data-toggle="dropdown">
                                  <i class="la la-ellipsis-h"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{url('admin/categories/'.$sc->id.'/edit')}}"><i class="la la-edit"></i> Edit</a>
                                <a class="dropdown-item" href="{{url('admin/categories/'.$sc->id.'/delete')}}"><i class="la la-trash"></i> Delete</a>
                              </div>
                          </div>
                        </td>
                    </tr>                    
                    
                    @endforeach
                    @php $i++; @endphp
                @endforeach
                
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('footer-js')
<script type="text/javascript">
    $(document).ready( function () {
    var datatable = $('.kt-datatable table').DataTable({});
    
    $('body').on('click', '.main_category_row', function(){
        var num = $(this).attr('data-num');
        if($(this).hasClass('active_row'))
        {
            $(this).removeClass('active_row');
            $('.sub_category_row').removeClass('d-show');           
        }
        else
        {
            $(this).addClass('active_row');
            $('.sub_category_row').removeClass('d-show');
            $('.sub_category_row'+num).addClass('d-show');
        }
    });
});
</script>
@endsection
