@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                المدن
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{ $data }} </span>
                <form class="kt-margin-l-20" id="kt_subheader_search_form">
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                        <input type="text" class="form-control" placeholder="البحث..." id="generalSearch" name="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                    <span><i class="la la-search text-primary"></i></span>
                                </span>
                                <!--<i class="flaticon2-search-1"></i>-->
                            </span>
                        </span>
                    </div>
                </form>
            </div>
            <div class="kt-subheader__group kt-hidden" id="kt_subheader_group_actions">
                <div class="kt-subheader__desc"><span id="kt_subheader_group_selected_rows"></span> Selected:</div>
                <div class="btn-toolbar kt-margin-l-20">
                    <div class="dropdown" id="kt_subheader_group_actions_status_change">
                        <button type="button" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle"
                            data-toggle="dropdown">
                            Update Status
                        </button>
                        <div class="dropdown-menu">
                            <ul class="kt-nav">
                                <li class="kt-nav__section kt-nav__section--first">
                                    <span class="kt-nav__section-text">Change status to:</span>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="1">
                                        <span class="kt-nav__link-text"><span
                                                class="kt-badge kt-badge--unified-success kt-badge--inline kt-badge--bold">Approved</span></span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="2">
                                        <span class="kt-nav__link-text"><span
                                                class="kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--bold">Rejected</span></span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="3">
                                        <span class="kt-nav__link-text"><span
                                                class="kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--bold">Pending</span></span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link" data-toggle="status-change" data-status="4">
                                        <span class="kt-nav__link-text"><span
                                                class="kt-badge kt-badge--unified-info kt-badge--inline kt-badge--bold">On
                                                Hold</span></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <button class="btn btn-label-success btn-bold btn-sm btn-icon-h"
                        id="kt_subheader_group_actions_fetch" data-toggle="modal"
                        data-target="#kt_datatable_records_fetch_modal">
                        Fetch Selected
                    </button>
                    <button class="btn btn-label-danger btn-bold btn-sm btn-icon-h"
                        id="kt_subheader_group_actions_delete_all">
                        Delete All
                    </button>
                </div>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <a href="#" class="">
            </a>
            <a href="{{ route('cities.create') }}" class="btn btn-label-brand btn-bold">
                إضافة مدينة جديد </a>
        </div>
    </div>
</div>

<!-- end:: Content Head -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body kt-portlet__body--fit">
        <div class="kt-datatable" id="kt_apps_user_list_datatable"></div>
    </div>
</div>
@endsection
@section('footer-js')
<script type="text/javascript">
    var datatable = $('.kt-datatable').KTDatatable({
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    url: "{{ url('/admin/cities_ajax') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    map: function (raw) {
                        // sample data mapping
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    },
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
        },

        // layout definition
        layout: {
            scroll: false,
            footer: false,
        },

        // column sorting
        sortable: true,

        pagination: true,

        search: {
            input: $('#generalSearch'),
        },

        // columns definition
        columns: [{
            field: 'id',
            title: '#',
            sortable: 'asc',
            width: 40,
            type: 'number',
            selector: false,
            textAlign: 'center',
            template: function (row, x = 0) {
                x = x + 1;
                return x ;
            },
        }, {
            field: 'city',
            title: 'المدينة',
        },{
            field: 'country',
            title: 'الدولة',
        },{
            field: 'status',
            width: 100,
            title: 'الحالة',
            // callback function support for column rendering
            template: function (row) {
                var status = {
                    0: {
                        'title': 'غير مفعل',
                        'class': 'kt-badge--danger'
                    },
                    1: {
                        'title': 'مفعل',
                        'class': 'kt-badge--brand'
                    },
                };
                return '<span class="kt-badge ' + status[row.status].class +
                    ' kt-badge--inline kt-badge--pill">' + status[row.status].title + '</span>';
            },
        }, {
            field: 'Actions',
            title: 'إضافات',
            sortable: false,
            width: 130,
            overflow: 'visible',
            textAlign: 'center',
            template: function (row, index, datatable) {
                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';

                var edit = "admin/cities/"+row.id+"/edit";
                var status = "admin/cities/"+row.id+"/status";
                var del = "admin/cities/"+row.id+"/delete";

                return '<div class="dropdown ' + dropup + '">\
                              <a href="#" class="btn btn-hover-brand btn-icon btn-pill" data-toggle="dropdown">\
                                  <i class="la la-ellipsis-h"></i>\
                              </a>\
                              <div class="dropdown-menu dropdown-menu-right">\
                                <a class="dropdown-item" href="'+edit+'"><i class="la la-edit"></i> تعديل</a>\
                                <a class="dropdown-item" href="'+status+'"><i class="la la-dot-circle-o"></i> تغيير الحالة</a>\
                                <a class="dropdown-item" href="'+del+'"><i class="la la-trash"></i> حذف</a>\
                              </div>\
                          </div>\
                          ';
            },
        }],

    });
</script>
@endsection
