@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content -->
<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">
        <!--end: Form Wizard Nav -->
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid">
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_user_add_form" method="POST" action="{{ route('countries.update', $data->id) }}">
                            <!--begin: Form Wizard Step 1-->
                            @csrf
                            @method('PUT')
                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                                data-ktwizard-state="current">
                                <div class="kt-heading kt-heading--md">Edit Country :</div>
                                <div class="kt-section kt-section--first">
                                    <div class="kt-wizard-v4__form">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="kt-section__body">
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Country name
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <input class="form-control" name="country" type="text" value="{{ $data->country }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Country Code
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <input class="form-control" name="code" type="text" value="{{ $data->code }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Status
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="form-group">
                                                                <div class="kt-radio-inline">
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio" name="status" value="0" {{ $data->status == 0 ? 'checked' : '' }} /> Active
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio" name="status" value="1" {{ $data->status == 1 ? 'checked' : '' }}  /> Not active
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Actions -->
                            <div class="kt-form__actions">
                                <button type="submit"
                                    class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Save</button>
                            </div>
                            <!--end: Form Actions -->
                        </form>
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
