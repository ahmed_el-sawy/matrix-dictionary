@extends('layouts.dashboard')
@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">Home About Us</h3>
		</div>
	</div>

	<!--begin::Form-->
	<form class="kt-form kt-form--label-left" id="kt_form_1" method="post"  action="{{ url('admin/home_about_us') }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="kt-portlet__body">
		    

			<div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Title</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" name="title" value="{{$section->title}}" />
				</div>
			</div>
			<div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Image</label>
				<div class="col-lg-5">
					<input type="file" class="form-control" name="image" />
				</div>
				<div class="col-lg-5"><img src="{{asset($section->image)}}" /></div>
			</div>
            <div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Description</label>
				<div class="col-lg-10">
					<textarea type="text" class="form-control" name="text">{{$section->text}}</textarea>
				</div>
			</div>
			<div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Link</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" name="link" value="{{$section->link}}" />
				</div>
			</div>

            <div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Button Text</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" name="btn_text" value="{{$section->btn_text}}" />
				</div>
			</div>
			
			<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<div class="row">
						<div class="col-lg-12 text-right">
							<button type="submit" class="btn btn-success">Save</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
@endsection


