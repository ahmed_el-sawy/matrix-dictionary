@extends('layouts.dashboard')
@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">Home Categories Section</h3>
		</div>
	</div>

	<!--begin::Form-->
	<form class="kt-form kt-form--label-left" id="kt_form_1" method="post"  action="{{ url('admin/categories_section') }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="kt-portlet__body">
		    

			<div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Title</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" name="title" value="{{$section->title}}" />
				</div>
			</div>

			<div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Text</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" name="description" value="{{$section->text}}" />
				</div>
			</div>

			<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<div class="row">
						<div class="col-lg-12 text-right">
							<button type="submit" class="btn btn-success">Save</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
@endsection


