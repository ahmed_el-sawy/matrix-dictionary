@extends('layouts.dashboard')
@section('content')
	<!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Home About Us Sections
                </h3>
            </div>
            
            <div class="kt-portlet__head-toolbar">
            	<div class="kt-portlet__head-wrapper">
                	<div class="kt-portlet__head-actions">
                    	<a href="{{url('admin/home_about_us_sections/create')}}" class="btn btn-brand btn-elevate btn-icon-sm"><i class="fas fa-plus"></i> New Section</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="kt-portlet__body">

            <!--begin::Section-->
            <div class="kt-section">
                
                <div class="kt-section__content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($sections as $slider)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$slider->title}}</td>
                            <td>
                                <a href="{{route('home_about_us_sections.edit', $slider->id)}}" class="btn btn-info btn-sm"><i class="fas fa-edit"></i> Edit</a>
                                <a data-toggle="modal" href="#myModal-{{ $slider->id }}" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
                                <div class="modal fade" id="myModal-{{ $slider->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Delete Section</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                <form role="form" action="{{ url('/admin/home_about_us_sections/'.$slider->id) }}" class="" method="POST">
                                <input name="_method" type="hidden" value="DELETE">
                                {{ csrf_field() }}
                                <p>Are You Sure?</p>
                                <button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                </form>
                                </div>
                                </div>
                                </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                        </div>
                        
                </div>
            </div>

            <!--end::Section-->
        </div>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->
@endsection
