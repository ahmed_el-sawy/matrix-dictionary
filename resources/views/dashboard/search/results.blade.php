@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content Head -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body kt-portlet__body--fit">
        <table class="table">
            <tr>
                <td>Words Results</td>
                <td>{{ $results_count }}</td>
                <td>Categories</td>
                <td>{{ $categories }}</td>
                <td>Dictionaries</td>
                <td>{{ $dictionaries_count }}</td>
            </tr>
        </table>
        <br/>
        <form class="kt-form" id="kt_user_add_form" method="get" action='{{ route("search.create") }}'>
            @csrf
            <div class="container">
                <div class="col-xl-12">
                    <div class="form-group row">
                        <div class="input-group">
                            <input type="hidden" class="hidden" value="{{ \request()->get('dictionary') }}" name="dictionary" />
                            <input type="hidden" class="hidden" value="{{ \request()->get('source_lang') }}" name="source_lang" />
                            <input type="hidden" class="hidden" value="{{ \request()->get('target_lang') }}" name="target_lang" />
                            <input type="hidden" class="hidden" value="{{ \request()->get('category') }}" name="category" />
                            <input type="hidden" class="hidden" value="{{ \request()->get('sub_category') }}" name="sub_category" />

                            <div class="col-lg-8 col-xl-8">
                                <input type="text" name="search" value="{{ \request()->get('search') }}" class="form-control" style="border: 2px solid"/>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <button type="submit" class="btn btn-primary btn-md btn-btn-tallest btn-wide kt-font-bold kt-font-transform-u mt-0">Search</button>
                                <a href="{{ route('search.index') }}" class="btn btn-success btn-md btn-btn-tallest btn-wide kt-font-bold kt-font-transform-u mt-0">Return back </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Datatable -->
<div class="kt-portlet kt-portlet--mobile">
    {!! $result !!}
</div>
<!--end: Datatable -->
@endsection
@section('footer-js')
    <script type="text/javascript">
        $(document).ready(function() {
            function format ( d ) {
                // `d` is the original data object for the row
                return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                    '<tr>'+
                        '<td>Source Explain:</td>'+
                        '<td>'+d.SourceExplain+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Target Explain:</td>'+
                        '<td>'+d.TargetExplain+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Examples:</td>'+
                        '<td>'+d.Examples+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>Media:</td>'+
                        '<td>'+d.Media+'</td>'+
                    '</tr>'+
                '</table>';
            }


            var table = $('.kt_table_1').DataTable({
                "columns": [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '<i class="fa fa-plus-circle fa-2x text-success"></i>',
                        "width": "2%"
                    },
                    { "data": "Source Language"},
                    { "data": "Target Language"},
                    { "data": "SourceExplain", name: "Source_Explain"},
                    { "data": "TargetExplain", name: "Target_Explain"},
                    { "data": "Examples"},
                    { "data": "Media"},
                ],
            });

            // Add event listener for opening and closing details
            $('.kt_table_1 tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = table.row( tr );
                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    $(this).children('i').removeClass().addClass('fa fa-plus-circle fa-2x text-success');
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    $(this).children('i').removeClass().addClass('fa fa-minus-circle fa-2x text-danger');
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                }
            } );
        });

        // Get and show word data (examples, media)
        $(document).on('click','.show-more', function(e){
            e.preventDefault();
            $.ajax({
                url: $(this).attr('href')
            }).done(function(data){
                $('#modal').modal();
                $('.modal-body').html(data);
            });
        });
    </script>
    <script src="assets/js/pages/crud/datatables/basic/basic.js" type="text/javascript"></script>
@endsection

