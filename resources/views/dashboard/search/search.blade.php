@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content -->
<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">
        <!--end: Form Wizard Nav -->
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid">
                    <div class="kt-grid__item kt-grid__item--fluid kt-container ">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_user_add_form" method="get" action="{{ route('search.create') }}">
                            <!--begin: Form Wizard Step 1-->
                            @csrf
                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                                data-ktwizard-state="current">
                                <div class="kt-heading kt-heading--md">Search :</div>
                                <div class="kt-section kt-section--first">
                                    <div class="kt-wizard-v4__form">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="kt-section__body">
                                                    <div class="form-group row">
                                                        <div class="input-group">
                                                            <div class="col-lg-12 col-xl-12">
                                                            <label class="col-form-label">Dictionary</label>
                                                                <select class="form-control select2 selectpicker"
                                                                        name="dictionary">
                                                                    <option value="">Choose Dictionary</option>
                                                                    <option value="all">All</option>
                                                                    @foreach($dictionaries as $dictionary)
                                                                        <option value="{{ $dictionary->id }}">
                                                                            {{ $dictionary->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="input-group">
                                                            <div class="col-lg-6 col-xl-6">
                                                                <label class="col-form-label">Source language
                                                                </label>
                                                                <select class="form-control select2 selectpicker"
                                                                        name="source_lang">
                                                                    <option value="">Choose Language</option>
                                                                    @foreach($languages as $language)
                                                                        <option value="{{ $language->id }}">
                                                                            {{ $language->language }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-6 col-xl-6">
                                                                <label class="col-form-label">Target language
                                                                </label>
                                                                <select class="form-control select2 selectpicker"
                                                                        name="target_lang">
                                                                    <option value="">Choose Language</option>
                                                                    @foreach($languages as $language)
                                                                        <option value="{{ $language->id }}">
                                                                            {{ $language->language }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="input-group">
                                                            <div class="col-lg-6 col-xl-6">
                                                                <label class="col-form-label">Category
                                                                </label>
                                                                <select class="form-control select2 selectpicker"
                                                                        name="category">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{ $category->id }}">
                                                                            {{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-6 col-xl-6">
                                                                <label class="col-form-label">Sub Category
                                                                </label>
                                                                <select class="form-control select2 selectpicker"
                                                                        name="sub_category">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach($sub_categories as $category)
                                                                        <option value="{{ $category->id }}">
                                                                            {{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="input-group">
                                                            <div class="col-lg-9 col-xl-9">
                                                                <label class="col-form-label">Search</label>
                                                                <input type="text" name="search" value="{{ old('search') }}" class="form-control"/>
                                                            </div>
                                                            <div class="col-lg-3 col-xl-3">
                                                                <button type="submit" class="btn btn-primary btn-sm btn-btn-tallest btn-wide kt-font-bold kt-font-transform-u mt-5">Search</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                        </form>
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
