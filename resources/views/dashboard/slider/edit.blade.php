@extends('layouts.dashboard')
@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Edit Slider
			</h3>
		</div>
	</div>

<!--begin::Form-->
<form class="m-form m-form--label-align-left" method="post" action="{{route('slider.update', $slider->id)}}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT" />
		
	<div class="kt-portlet__body">
		@if($errors->any())
			<div class="alert alert-danger">{{$errors->first()}}</div>
		@endif
				
			<div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Image  <small class="d-none">(1920 * 700)</small></label>
				<div class="col-lg-7">
					<div class="custom-file">
						<input type="file" class="custom-file-input" name="image" id="image" />
						<label class="custom-file-label" for="image">Choose file</label>
					</div>
				</div>
				<div class="col-lg-3">
					<img src="{{asset($slider->image)}}">
				</div>
			</div>			
		</div>

		<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<div class="row">
						<div class="col-lg-12 text-right">
							<button type="submit" class="btn btn-success">Save</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>

@endsection


