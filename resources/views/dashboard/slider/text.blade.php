@extends('layouts.dashboard')
@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">Search Section Text - Slider</h3>
		</div>
	</div>

	<!--begin::Form-->
	<form class="kt-form kt-form--label-left" id="kt_form_1" method="post"  action="{{ url('admin/slider_text') }}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="kt-portlet__body">
		
									
			<div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Title</label>
				<div class="col-lg-10">
					<textarea class="form-control ckeditor" name="title">{{$search_section->title}}</textarea>
				</div>
			</div>

			<div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Text</label>
				<div class="col-lg-10">
					<textarea class="form-control" name="text">{{$search_section->text}}</textarea>
				</div>
			</div>

			<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<div class="row">
						<div class="col-lg-12 text-right">
							<button type="submit" class="btn btn-success">Save</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
@endsection


