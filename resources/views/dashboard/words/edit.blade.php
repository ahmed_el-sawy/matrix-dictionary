@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content -->
<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">
        <!--end: Form Wizard Nav -->
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid">
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_user_add_form" method="POST" action="{{ route('words.update', $data->id) }}" enctype="multipart/form-data">
                            <!--begin: Form Wizard Step 1-->
                            @csrf
                            @method('PUT')
                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                                data-ktwizard-state="current">
                                <div class="kt-heading kt-heading--md">Word Edit :</div>
                                <div class="kt-section kt-section--first">
                                    <div class="kt-wizard-v4__form">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="kt-section__body">
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source word
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <input class="form-control" name="s_language" type="text" value="{{ $data->s_language }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source explain
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <textarea class="form-control" name="s_explain">{{ $data->s_explain }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Target word
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <input class="form-control" name="t_language" type="text" value="{{ $data->t_language }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Target explain
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <textarea class="form-control" name="t_explain">{{ $data->t_explain }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Examples
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            @if($data->examples != NULL)
                                                                @php $x=0; @endphp
                                                                @foreach(json_decode($data->examples) as $ex)
                                                                    @php $x++; @endphp
                                                                    <div class="more_data">
                                                                        <textarea class="form-control float-left col-11 mb-2 input-data" name="examples[]" placeholder="Examples">{{ $ex }}</textarea>
                                                                        @if($x != 1)
                                                                            <span class="btn btn-danger btn-sm btn-circle float-right col-1 remove-item" style=""><i class="fa fa-trash"></i></span>
                                                                        @endif
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <div class="more_data">
                                                                    <textarea class="form-control float-left col-11 mb-2 input-data" name="examples[]" placeholder="Examples"></textarea>
                                                                </div>
                                                            @endif
                                                            <button type="button" class="btn btn-primary mt-2 more">More Ex</button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Media / Documents
                                                        </label>
                                                        <div class="col-lg-3 col-xl-3">
                                                            <input type="file" multiple name="files[]" />
                                                            <br/>
                                                            @php
                                                                $img_types = ['png', 'jpeg', 'jpg', 'bmp'];
                                                            @endphp

                                                            @foreach($media as $md)
                                                                <div class="more_data">
                                                                    @if($md->media_type != 'url')
                                                                        <input style="visibility: hidden; display: none" name="old_files[]" value="{{ $md->id }}" />
                                                                        @if(in_array($md->media_type, $img_types))
                                                                            <img src="{{ asset($md->media) }}" class="img-thumbnail float-left col-10" style="height: 70px"/>
                                                                        @else
                                                                            <pre class="float-left col-10">
                                                                                <a href="{{ asset($md->media) }}" target="_blank" class="float-left">{{ explode('/media/', $md->media)[1] }}</a>
                                                                            </pre>
                                                                        @endif
                                                                    <span class="btn btn-danger btn-sm btn-circle float-right col-2 remove-item" style=""><i class="fa fa-trash"></i></span>
                                                                    @endif
                                                                </div>
                                                            @endforeach

                                                        </div>
                                                        <div class="col-lg-6 col-xl-6">
                                                            @if(count($media) == 0)
                                                                <div class="more_data">
                                                                    <input type="url" class="form-control float-left col-11 mb-2 input-data" name="urls[]" placeholder="Media URL" value="" />
                                                                </div>
                                                            @endif

                                                            @php $y=0; @endphp
                                                            @foreach($media as $md)
                                                                @if($md->media_type == 'url')
                                                                    @php $y++; @endphp
                                                                <div class="more_data">
                                                                    <input type="url" class="form-control float-left col-11 mb-2 input-data" name="urls[]" placeholder="Media URL" value="{{ $md->media }}" />
                                                                    @if($y != 1)
                                                                        <span class="btn btn-danger btn-sm btn-circle float-right col-1 remove-item" style=""><i class="fa fa-trash"></i></span>
                                                                    @endif
                                                                </div>
                                                                @endif
                                                            @endforeach
                                                            <button type="button" class="btn btn-dark mt-2 more">More Media</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Actions -->
                            <div class="kt-form__actions">
                                <button type="submit"
                                    class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Save</button>
                            </div>
                            <!--end: Form Actions -->
                        </form>
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
@section('footer-js')
<script>
    $('.more').on('click', function(){
        var row = $(this).parent();
        $(this).prev('.more_data').clone().insertBefore( $(this) ).find(".input-data").val("").css('margin-bottom', '2px');

        $('.remove-item').on('click', function(){
            $(this).parent().remove();
        });
    });

    $('.remove-item').on('click', function(){
        $(this).parent().remove();
    });
</script>
@endsection
