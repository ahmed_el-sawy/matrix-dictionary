@extends('layouts.dashboard')
@section('content')
<!-- begin:: Content -->
<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-wizard-v4" id="kt_user_add_user" data-ktwizard-state="step-first">
        <!--end: Form Wizard Nav -->
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid">
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_user_add_form" method="POST" action="{{ route('dictionary_file.update', $file->id) }}" enctype="multipart/form-data">
                            <!--begin: Form Wizard Step 1-->
                            @csrf
                            @method('PUT')
                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                                data-ktwizard-state="current">
                                <div class="kt-heading kt-heading--md"><b>Edit Dictionary File :</div>
                                <div class="kt-section kt-section--first">
                                    <div class="kt-wizard-v4__form">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="kt-section__body">
                                                    {{--
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Excel File
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="file" name="file" class="form-control" accept=".csv,.xlsx,.xls,.xlsb,.xlsm,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
                                                        </div>
                                                    </div>
                                                    --}}
                                                    <input type="hidden" name="dictionary" value="{{$file->dictionary}}" />
                                                    <input type="hidden" name="category" value="{{$file->category}}" />
                                                    <input type="hidden" name="sub_category" value="{{$file->sub_category}}" />
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="text" name="source" class="form-control" value="{{ $file->source }}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Copy Right
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="text" name="copy_right" class="form-control" value="{{ $file->copy_right }}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Author
                                                        </label>

                                                        <div class="col-lg-9 col-xl-9">
                                                            <input type="text" name="author" class="form-control" value="{{ $file->author }}"/>
                                                        </div>
                                                    </div>
                                                    {{--
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Dictionary
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="dictionary">
                                                                    <option value="">Choose Dictionary</option>
                                                                    @foreach($dictionaries as $dictionary)
                                                                        <option value="{{ $dictionary->id }}" @if($file->dictionary == $dictionary->id) selected @endif>
                                                                            {{ $dictionary->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    --}}
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Source Language
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="source_lang">
                                                                    <option value="">Choose Language</option>
                                                                    @foreach($languages as $language)
                                                                        <option value="{{ $language->id }}" @if($file->source_lang == $language->id) selected @endif>
                                                                            {{ $language->language }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Target Language
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="target_lang">
                                                                    <option value="">Choose Language</option>
                                                                    @foreach($languages as $language)
                                                                        <option value="{{ $language->id }}" @if($file->target_lang == $language->id) selected @endif>
                                                                            {{ $language->language }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Category
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker" disabled
                                                                        name="category123">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{ $category->id }}" @if($file->category == $category->id) selected @endif>
                                                                            {{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Sub Category
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker" disabled
                                                                        name="sub_cate345gory">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach($sub_categories as $category)
                                                                        <option value="{{ $category->id }}" @if($file->sub_category == $category->id) selected @endif>
                                                                            {{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    {{--
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Category
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="category">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{ $category->id }}" @if($file->category == $category->id) selected @endif>
                                                                            {{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Sub Category
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="input-group">
                                                                <select class="form-control select2 selectpicker"
                                                                        name="sub_category">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach($sub_categories as $category)
                                                                        <option value="{{ $category->id }}" @if($file->sub_category == $category->id) selected @endif>
                                                                            {{ $category->title }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    --}}
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Status
                                                        </label>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <div class="form-group">
                                                                <div class="kt-radio-inline">
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio" name="status" value="1" /> Active
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="kt-radio">
                                                                        <input type="radio" class="custom-radio" name="status" value="0" checked /> Not Active
                                                                        <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Actions -->
                            <div class="kt-form__actions">
                                <button type="submit"
                                    class="btn btn-primary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Add</button>
                            </div>
                            <!--end: Form Actions -->
                        </form>
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
@endsection
