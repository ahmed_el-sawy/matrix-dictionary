@extends('layouts.dashboard')
@section('content')
<style>
    .hidden{
        visibility: hidden !important;
        display: none !important;
    }
</style>
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Dictionary <b style="margin:0 2px;">{{$dictionary->title}}</b>  Words 
            </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>
            <div class="kt-subheader__group" id="kt_subheader_search">
                <span class="kt-subheader__desc" id="kt_subheader_total">
                    {{ $data }} </span>
                <form class="kt-margin-l-20" id="kt_subheader_search_form">
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                        {{-- <input type="text" class="form-control" placeholder="Search..." id="generalSearch" name="generalSearch"> --}}
                        {{-- <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path
                                            d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                        <path
                                            d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                            fill="#000000" fill-rule="nonzero" />
                                    </g>
                                </svg>
                                <!--<i class="flaticon2-search-1"></i>-->
                            </span> --}}
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            <a href="#" class="">
            </a>
            <a href="{{ url('admin/words_create_file/'.$dictionary->id) }}" class="btn btn-label-brand btn-bold">Add new Dictionary Words</a>
        </div>
    </div>
</div>
<div class="kt-portlet kt-portlet--mobile">
    <table class="table table-bordered table-hover">
        @foreach($files as $file)
            <tr>
                <td class="text-black-50">Author</td>
                <td>{{ $file->author }}</td>
                <td class="text-black-50">Source</td>
                <td>{{ $file->source }}</td>
                <td class="text-black-50">Copy Right</td>
                <td>{{ $file->copy_right }}</td>
            </tr>
            <tr>
                <td class="text-black-50">File</td>
                <td colspan="3">
                    <a href="{{ asset($file->file) }}" target="_blank">{{ $file->file_name }}</a> 
                    @if($file->status == 0) <span class="badge badge-danger">Not Active</span> @else <span class="badge badge-info">Active</span> @endif
                </td>
                <td colspan="2">
                    <a href="{{url('/admin/dictionary_file/'.$file->id.'/edit')}}" class="btn btn-info p-1 text-center"><i class="fas fa-edit"></i> Edit</a>
                    <a href="{{url('/admin/dictionary_file/'.$file->id.'/delete')}}" class="btn btn-danger p-1 text-center"><i class="fas fa-trash"></i> Delete</a>
                </td>
            </tr>
        @endforeach
    </table>
</div>
<!-- end:: Content Head -->
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover" id="kt_table_1">
            <thead>
                <tr>
                    <th class="sm-col">#</th>
                    <th>Source Language</th>
                    <th>Target Language</th>
                    <th style="visibility: hidden;display: none;">SourceExplain</th>
                    <th style="visibility: hidden;display: none;">TargetExplain</th>
                    <th style="visibility: hidden;display: none;">Examples</th>
                    <th style="visibility: hidden;display: none;">Media</th>
                    <th>Actions</th>
                </tr>
            </thead>

        </table>
    </div>
</div>
@endsection
@section('footer-js')
    <script type="text/javascript">
    $(document).ready(function(){
        function format ( d ) {
            // `d` is the original data object for the row
            return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                '<tr>'+
                    '<td>Source Explain:</td>'+
                    '<td>'+d.s_explain+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>Target Explain:</td>'+
                    '<td>'+d.t_explain+'</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>Examples:</td>'+
                    '<td><a href="admin/examples/'+d.id+'" class="show-more">'+d.examples+'</a></td>'+
                '</tr>'+
                '<tr>'+
                    '<td>Media:</td>'+
                    '<td><a href="admin/media/'+d.id+'" class="show-more">'+d.mediaCounts+'</a></td>'+
                '</tr>'+
            '</table>';
        }

        let searchParams = new URLSearchParams(window.location.search)
        searchParams.has('sent') // true
        let param = searchParams.get('d')

        var table = $('#kt_table_1').DataTable({
            // datasource definition
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{ url('/admin/words_ajax') }}"+"?d="+param,
                "type": "POST",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            },

            // columns definition
            "columns": [
                {
                    "class":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": '<i class="fa fa-plus-circle fa-2x text-success"></i>',
                    "width": "2%"
                },
                { "data": "s_language"},
                { "data": "t_language"},
                { "data": "s_explain", "class": "hidden"},
                { "data": "t_explain", "class": "hidden"},
                { "data": "examples", "class": "hidden"},
                { "data": "mediaCounts", "class": "hidden"},
                { "data": "action"},
            ]
        });

        $('#kt_table_1 tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
            if ( row.child.isShown() ) {
                // This row is already open - close it
                $(this).children('i').removeClass().addClass('fa fa-plus-circle fa-2x text-success');
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                $(this).children('i').removeClass().addClass('fa fa-minus-circle fa-2x text-danger');
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );
    });

    $(document).on('click','.show-more', function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href')
        }).done(function(data){
            $('#modal').modal();
            $('.modal-body').html(data);
            // console.log(data);
        });
    });
    </script>
@endsection
