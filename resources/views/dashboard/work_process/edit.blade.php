@extends('layouts.dashboard')
@section('content')
<!--begin::Portlet-->
<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Edit Work process Section
			</h3>
		</div>
	</div>

<!--begin::Form-->
<form class="m-form m-form--label-align-left" method="post" action="{{route('home_work_process_sections.update', $slider->id)}}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value="PUT" />
		
	<div class="kt-portlet__body">
		@if($errors->any())
			<div class="alert alert-danger">{{$errors->first()}}</div>
		@endif
				
				
						<div class="form-group m-form__group row">
		<label class="col-lg-2 col-form-label">Title</label>
			<div class="col-lg-10">
				<input type="text" class="form-control" name="title" id="title" value="{{$slider->title}}" />
			</div>
		</div>
		
		<div class="form-group m-form__group row">
			<label class="col-lg-2 col-form-label">Text</label>
			<div class="col-lg-10">
				<input type="text" class="form-control" name="text" id="text" value="{{$slider->text}}" />
			</div>
		</div>
		
			<div class="form-group m-form__group row">
				<label class="col-lg-2 col-form-label">Image  <small class="d-none">(1920 * 700)</small></label>
				<div class="col-lg-7">
					<div class="custom-file">
						<input type="file" class="custom-file-input" name="image" id="image" />
						<label class="custom-file-label" for="image">Choose file</label>
					</div>
				</div>
				<div class="col-lg-3">
					<img src="{{asset($slider->image)}}">
				</div>
			</div>			
		</div>

		<div class="kt-portlet__foot">
				<div class="kt-form__actions">
					<div class="row">
						<div class="col-lg-12 text-right">
							<button type="submit" class="btn btn-success">Save</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>

@endsection


