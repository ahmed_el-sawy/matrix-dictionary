@extends('layouts.dashboard')
@section('content')
	<!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Home Work Process
                </h3>
            </div>
            
        </div>

        <div class="kt-portlet__body">

            <!--begin::Section-->
            <div class="kt-section">
                
                <div class="kt-section__content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($sections as $slider)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$slider->title}}</td>
                            <td>
                                <a href="{{route('home_work_process_sections.edit', $slider->id)}}" class="btn btn-info btn-sm"><i class="fas fa-edit"></i> Edit</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                        </div>
                        
                </div>
            </div>

            <!--end::Section-->
        </div>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->
@endsection
