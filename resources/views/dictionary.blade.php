@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('content')
<!--=================================
banner -->
<section class="header-inner header-inner-big bg-holder text-white" style="background-image: url({{asset('site/images/bg/banner-01.jpg')}});">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="job-search-field">
          <div class="job-search-item">
            <form class="form row" method="get" action="{{url('Search')}}" id="SearchTermForm">
              <div class="col-lg-3 col-sm-6 col-6">
                <div class="form-group left-icon">
                  <input type="text" class="form-control" name="search" placeholder="Type to search" value="{{$search_filter}}" id="searchterminput">
                  <i class="fas fa-search"></i>
                  </div>
              </div>
              <div class="col-3">
              <select class="form-control" name="cat[]">
                      <option value="All">All Categories</option>
                      @foreach ($cats as $cat)
                        <option value="{{$cat->id}}">{{$cat->title}}</option>
                      @endforeach
                    </select>
              </div>
              <div class="col-2">
                <select class="form-control" name="from">
                  <option value="All" disabled selected>From</option>
                  @foreach ($languages_data as $language)
                    <option value="{{$language->id}}" @if($from == $language->id) selected @endif>{{$language->language}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-2">
                <select class="form-control" name="to">
                  <option value="All" disabled selected>To</option>
                  @foreach ($languages_data as $language)
                    <option value="{{$language->id}}" @if($to == $language->id) selected @endif>{{$language->language}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-2 col-sm-3 col-3">
                <div class="form-group form-action">
                  <button type="submit" class="btn btn-primary mt-0"><i class="fas fa-search"></i> <span class="d-none d-md-inline-block"></span></button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
banner -->

<!--=================================
job-list -->
<section class="space-ptb">
	<div class="container">
  		<div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 order-2 order-lg-1 order-md-1 order-sm-1">
              <form action="{{url('Search')}}" method="get" id="SideBarFilterForm">
                  <input type="hidden" name="search" value="{{$search_filter}}" />
                  <input type="hidden" name="from" value="{{$from}}" />
                  <input type="hidden" name="to" value="{{$to}}" />
                  <!--================================= left-sidebar -->
                    <div class="sidebar">
                    {{--
                      <div class="widget">
                        
                        <div class="widget-title widget-collapse">
                          <h6>Languages</h6>
                          <a class="ml-auto" data-toggle="collapse" href="#languages" role="button" aria-expanded="false" aria-controls="categories">
                            <i class="fas fa-chevron-down"></i>
                          </a>
                          </div>
                          <div class="collapse show" id="languages">
                            <div class="widget-content">
                            --}}
                              @foreach ($languages as $language)
                                {{--
                                @php
                                $lang_word_count = language_words_counter($search_filter, $cats_filter, $dictionaries_filter, $language->id);
                                @endphp
                                @if($lang_word_count > 0)
                                --}}
                                {{--
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input Formcheckbox" name="language[]"
                                  id="language{{$loop->iteration}}"
                                  value="{{$language->id}}" @if(in_array($language->id, $languages_filter)) checked @endif>
                                  <label class="custom-control-label" for="language{{$loop->iteration}}">{{$language->language}}</label>
                                  <span class="badge badge-primary rightcountrerbadge">
                                    {{number_format($lang_word_count, 0)}}
                                  </span>
                                </div>
                                {{--
                                @endif
                                --}}
                              @endforeach
                              {{--
                          </div>
                        </div>
                        </div>
                        <hr />
                        --}}
                        <div class="widget">
                        <div class="widget-title widget-collapse">
                          <h6>Categories</h6>
                          <a class="ml-auto" data-toggle="collapse" href="#categories" role="button" aria-expanded="false" aria-controls="categories">
                            <i class="fas fa-chevron-down"></i>
                          </a>
                          </div>
                          <div class="collapse show" id="categories">
                            <div class="widget-content">
                            @foreach ($cats as $cat)
                                {{--
                                @php
                                $lang_word_count = category_words_counter($search_filter, $cat->id, $dictionaries_filter, $languages_filter);
                                @endphp
                                @if($lang_word_count > 0)
                                --}}
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input Formcheckbox" name="cat[]" id="cat{{$loop->iteration}}"
                                  value="{{$cat->id}}" @if(in_array($cat->id, $cats_filter)) checked @endif>
                                  <label class="custom-control-label" for="cat{{$loop->iteration}}">{{$cat->title}}</label>
                                  {{--
                                  <span class="badge badge-primary rightcountrerbadge">
                                    {{number_format($lang_word_count, 0)}}
                                  </span>
                                  --}}
                                </div>
                                {{-- @endif --}}
                            @endforeach
                          </div>
                        </div>
                        </div>
                        
                        <hr />
                        <div class="widget">
                          <div class="widget-add" id="today_word">
                            <h2>Today Word</h2>
                            <div id="words_dixt">
                              <p class="text-center">{{$word_rand->s_language}}</p>
                              @foreach (all_trans($word_rand->s_language) as $tt)
                                @if($loop->index == 0)
                                  <p class="text-center">{{$tt->t_language}}</p>
                                @endif
                              @endforeach
                              <p class="text-center"><a href="{{url('Search?search='.remove_word_special_chars($word_rand->s_language))}}" class="btn btn-dark btn-sm">More</a></p>
                            </div>
                          </div>
                        </div>
                    </div>
				</form>
            </div>

        <div class="col-lg-9 col-md-9 col-sm-6 col-12 order-1 order-lg-2 order-md-2 order-sm-2">
          <!--=================================
          right-sidebar -->

          <div class="row mb-4">

            <div class="col-md-6">
                {{--
                <div class="section-title mb-0">
                  @if($all_words_count > 0)
                    <h6 class="mb-0">Showing {{($curent_page-1)*10 + 1}}-@if(($curent_page-1)*10 + 10 <= $all_words_count) {{($curent_page-1)*10 + 10}} @else {{$all_words_count}} @endif  of <span class="text-primary">{{number_format($all_words_count, 0)}} Words</span></h6>
                  @else
                    <h6 class="mb-0 text-center">No Results Found</span></h6>
                  @endif
                </div>
                --}}
            </div>

          <div class="col-md-6">
          <div class="job-filter mb-0 d-sm-flex align-items-center">
          <div class="job-shortby ml-sm-auto d-flex align-items-center">
              <form class="form-inline" id="sortingDictionaryForm" action="{{url('Search')}}" method="get">
              <input type="hidden" name="search" value="{{$search_filter}}" />
              @for ($i = 0; $i < count($cats_filter); $i++)
                @if($cats_filter[$i] != 'All')
                  <input type="hidden" name="cat[]" value="{{$cats_filter[$i]}}" />
                @endif
              @endfor

                <div class="form-group mb-0">
                  <label class="justify-content-start mr-2">sort by :</label>
                  <div class="short-by">
                    <select class="form-control basic-select" name="sort_by" id="sort_by_selector">
                      <option value="source" @if($sort_filter == 's_language') selected @endif>Source</option>
                      <option value="target" @if($sort_filter == 't_language') selected @endif>Target</option>
                    </select>
                  </div>
                </div>
              </form>
            </div>
          </div>
          </div>
          </div>

          <div class="row">

          @foreach($words as $word)
            @include ('partials.word_term', ['word'])

          @endforeach
          </div>
          {{$words->appends($_GET)->links()}}
        </div>
      </div>
    </div>
</section>
<!--=================================
job-list -->

@endsection
