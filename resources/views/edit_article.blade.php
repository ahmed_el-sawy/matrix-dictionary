@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('styles')
<style>
    .hidden_footer_section {display: none;}
    .footer.footer.bg-light {margin: 0; padding: 0;}
</style>
@endsection

@section('content')



<!--=================================
tab -->
<section class="header-inner header-inner-big bg-holder text-white" style="background-image: url({{asset('site/images/bg/banner-01.jpg')}});">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="section-title text-center">
        <h2 class="text-primary">Edit Article</h2>
       </div>
      </div>
    </div>
  </div>
</section>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade active show" id="Job-detail" role="tabpanel" aria-labelledby="Job-detail-tab">
    <section class="space-ptb">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <form class="form-row" method="post" action="{{url('Update-Article/'.$article->id)}}" enctype="multipart/form-data" id="new_article_form">
            {{csrf_field()}}
              @if($errors->any())
                <div class="alert alert-danger">{{$errors->first()}}</div>
              @endif
              <div class="form-group col-md-12">
                <label>Title *</label>
                <input name="title" type="text" class="form-control" value="{{$article->title}}" placeholder="Enter a Title">
              </div>
              <div class="form-group col-md-12">
                <label>Description *</label>
                <textarea class="form-control ckeditor" rows="4" name="text">{{$article->text}}</textarea>
              </div>

                <div class="row mt-4 mt-lg-5">
                  <div class="col-12">
                    <h5 class="mb-4">File Attachments</h5>
                  </div>
              
                <div class="custom-file form-group col-6">
                  <input type="file" class="custom-file-input" name="image">
                  <label class="custom-file-label">Choose file</label>
                </div>
                <div class="form-group col-6">
                  <img src="{{asset($article->image)}}" />
                </div>
              </div>
                <div class="form-group col-md-12 text-right">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
            <div id="new_article_alert"></div>

          </div>
        </div>
      </div>
    </section>
  </div>
</div>
<!--=================================
feature-info-->


@endsection
