@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('content')



<!--=================================
banner -->
<section class="banner-bg-slider">
  <div id="bg-slider">
    @foreach ($sliders as $slider)
        <img src="{{asset($slider->image)}}" alt="{{env('APP_NAME')}}">
    @endforeach
  </div>
  <div class="banner-bg-slider-content">
    <div class="container">
    <div class="row justify-content-center">
      <div class=" col-lg-9 col-md-9 d-flex">
        <div class="content text-center">
            @if($search_section->title != '')            
                <h1 class="text-white mb-2">{!!strip_tags($search_section->title, '<span>')!!}</h1>
            @endif

            @if($search_section->text != '')
                <p class="lead mb-4 font-weight-normal text-white">{{$search_section->text}}</p>
            @endif
          <div class="job-search-field">
            <div class="job-search-item">
              <form method="get" action="{{url('Search')}}" id="SearchTermForm">
                <div class="col-sm-12">
                  <div class="form-group mb-md-0 justify-content-center">
                    <input type="text" class="form-control" name="search" placeholder="Type to search" id="searchterminput">
                    <select class="form-control" name="cat[]">
                      <option value="All">All Categories</option>
                      @foreach ($cats as $cat)
                        <option value="{{$cat->id}}">{{$cat->title}}</option>
                      @endforeach
                    </select>
                    <select class="form-control" name="from">
                      <option value="All" disabled selected>From</option>
                      @foreach ($languages_data as $language)
                        <option value="{{$language->id}}">{{$language->language}}</option>
                      @endforeach
                    </select>
                    <select class="form-control" name="to">
                      <option value="All" disabled selected>To</option>
                      @foreach ($languages_data as $language)
                        <option value="{{$language->id}}">{{$language->language}}</option>
                      @endforeach
                    </select>
                    <button type="submit" class="btn btn-primary btn-lg m-0"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<!--=================================
banner -->


<!--=================================
Action-box -->
<section class="bg-primary py-4 py-lg-5 ">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-9 mb-4 mb-sm-4 mb-lg-0">
        <div class="d-sm-flex">
          <h4 class="text-white">{{$banner->title}}</h4>
        </div>
      </div>
      <div class="col-md-3 text-lg-right">
        <a class="btn btn-dark" href="{{$banner->link}}">{{$banner->btn_text}}</a>
      </div>
    </div>
  </div>
</section>
<!--=================================
Action-box -->


<!--=================================
Category-style -->
<section class="space-ptb">
  <div class="container">
    <div class="section-title center">
      <h2 class="title">{{$cats_section->title}}</h2>
      <p class="mb-0">{{$cats_section->text}}</p>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="category-style text-center">
            @foreach ($cats as $cat)
                @if($cat->dictionaries_count > 0)
                <a href="#" class="category-item">
                    <div class="category-icon mb-4">
                    @if($cat->icon != '')
                        <img src="{{asset($cat->icon)}}" alt="{{$cat->title}}" />
                    @else
                      @if($cat->icon != '')
                        <img src="{{asset($cat->icon)}}" alt="{{$cat->title}}" />
                      @else
                        <i class="flaticon-account"></i>
                      @endif
                    @endif
                    </div>
                    <h6>{{$cat->title}}</h6>
                    <span class="mb-0">{{ $cat->words_sum($cat->id) }} Words </span>
                </a>
                @endif
            @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Category-style -->

<!--=================================
Data Stats -->
<section id="ourstats">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="counter mb-4 mb-sm-0">
          <div class="counter-icon">
            <i class="fas fa-language"></i>
          </div>
          <div class="counter-content">
            <span class="timer mb-1 text-dark" data-to="{{$languages, 0}}" data-speed="5000">{{number_format($languages, 0)}}</span>
            <label class="mb-0">Languages</label>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="counter mb-4 mb-sm-0">
          <div class="counter-icon">
            <i class="fas fa-list"></i>
          </div>
          <div class="counter-content">
            <span class="timer mb-1 text-dark" data-to="{{$cats_counter, 0}}" data-speed="5000">{{number_format($cats_counter, 0)}}</span>
            <label class="mb-0">Categories</label>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="counter mb-4 mb-sm-0">
          <div class="counter-icon">
            <i class="fas fa-book"></i>
          </div>
          <div class="counter-content">
            <span class="timer mb-1 text-dark" data-to="{{$dictionaries, 0}}" data-speed="5000">{{number_format($dictionaries, 0)}}</span>
            <label class="mb-0">Dictionaries</label>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="counter mb-4 mb-sm-0">
          <div class="counter-icon">
            <i class="fas fa-search"></i>
          </div>
          <div class="counter-content">
            <span class="timer mb-1 text-dark" data-to="{{$words_counter, 0}}" data-speed="5000">{{number_format($words_counter, 0)}}</span>
            <label class="mb-0">Words & Terms</label>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Data Stats -->

<!--=================================
Why You Choose -->
<section class="bg-light" id="About-Us">
  <div class="container-fluid p-0">
    <div class="row align-self-center m-0">
      <div class="col-md-6 bg-holder" style="background-image: url({{asset($about_section->image)}}); background-size: cover;">
      </div>
      <div class="col-xl-6 col-lg-6 col-md-12">
        <div class="feature-content">
          <div class="row">
            <div class="col-xl-10 col-lg-12">
              <div class="section-title-02">
                <h2>{{$about_section->title}}</h2>
                <p>{{$about_section->text}}</p>
              </div>
            </div>
          </div>
          <div class="align-self-center">
            <div class="row">
              <div class="col-lg-12">
                <div class="row category-style pb-2">
                    @foreach($about_sections as $section)
                      <div class="col-md-6 col-sm-12 mb-3">
                        <div class="category-icon mb-2">
                          <img src="{{asset($section->image)}}" alt="{{$section->title}}" />
                        </div>
                        <h6 class="mb-2">{{$section->title}}</h6>
                        <p>{{$section->text}}</p>
                      </div>
                    @endforeach
                </div>
                <a class="btn btn-primary" href="{{$about_section->link}}">{{$about_section->btn_text}}</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Why You Choose -->

<!--=================================
Top Companies -->
<section class="space-ptb">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 text-center">
        <div class="section-title center">
          <h2 class="title">{{$search_words_sections->title}}</h2>
          <p>{{$search_words_sections->text}}</p>
        </div>
        <div class="owl-carousel owl-nav-bottom-center" data-nav-arrow="false" data-nav-dots="true" data-items="4" data-md-items="3"
        data-sm-items="2" data-xs-items="1" data-xx-items="1" data-space="15" data-autoheight="true">
            @foreach ($words as $word)
                @if(strlen($word->word) > 2)
                  <div class="item">
                    <a href="{{url('Search?search='.remove_word_special_chars($word->word))}}">
                      <div class="employers-grid mb-4 mb-lg-0">
                          <div class="employers-list-details">
                            <div class="employers-list-info">
                              <div class="employers-list-title">
                                <div class="topsearch-icon mb-2">
                                    {{$word->word}}
                                </div>
                                <p>{{remove_word_special_chars($word->word)}}</p>
                                <p class="results text-center mt-5">{{$word->counter}} Search</p>
                              </div>
                            </div>
                          </div>
                      </div>
                    </a>
                  </div>
                @endif
            @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Top Companies -->

<!--=================================
Easiest Way to Use -->
<section class="space-ptb bg-primary">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-8 col-md-10">
        <div class="section-title-02 text-center text-white">
          <h2 class="text-white">{{$work_process_section->title}}</h2>
          <p>{{$work_process_section->text}}</p>
        </div>
      </div>
    </div>
    <div class="row bg-holder-pattern mr-md-0 ml-md-0" style="background-image: url('{{asset('site/images/step/pattern-01.png')}}');">
      @foreach ($processs as $ps)
          <div class="col-md-4 mb-4 mb-md-0">
            <div class="feature-step text-center">
              <div class="feature-info-icon">
                <img src="{{asset($ps->image)}}" alt="{{$ps->title}}" />
              </div>
              <div class=" text-white">
                <h5>{{$ps->title}}</h5>
                <p class="mb-0">{{$ps->text}}</p>
              </div>
            </div>
          </div>
      @endforeach
    </div>
  </div>
</section>
<!--=================================
Easiest Way to Use -->

@endsection
