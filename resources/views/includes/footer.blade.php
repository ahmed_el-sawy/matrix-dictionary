{{--
<!--=================================
Feature info section -->
<section class="feature-info-section hidden_footer_section">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="feature-info feature-info-02 p-4 p-lg-5 bg-primary">
          <div class="feature-info-icon mb-3 mb-sm-0 text-dark">
            <i class="flaticon-team"></i>
          </div>
          <div class="feature-info-content text-white pl-sm-4 pl-0">
            <p>Jobseeker</p>
            <h5 class="text-white">Looking For Job?</h5>
          </div>
          <a class="ml-auto align-self-center" href="#">Apply now<i class="fas fa-long-arrow-alt-right"></i> </a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="feature-info feature-info-02 p-4 p-lg-5 bg-dark">
          <div class="feature-info-icon mb-3 mb-sm-0 text-primary">
            <i class="flaticon-job-3"></i>
          </div>
          <div class="feature-info-content text-white pl-sm-4 pl-0">
            <p>Recruiter</p>
            <h5 class="text-white">Are You Recruiting?</h5>
          </div>
          <a class="ml-auto align-self-center" href="#">Post a job<i class="fas fa-long-arrow-alt-right"></i> </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Feature info section -->
--}}
<!--=================================
Footer -->
<footer class="footer bg-light">
  <div class="position-relative hidden_footer_section">
    <svg class="footer-shape"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      width="100%" height="85px">
      <path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
        d="M-0.000,-0.001 L1923.000,-0.001 L1923.000,84.999 C1608.914,41.669 1279.532,19.653 962.500,19.000 C635.773,18.326 323.692,40.344 -0.000,84.999 C-0.000,-83.334 -0.000,168.332 -0.000,-0.001 Z"/>
      </svg>
    </div>
    <div class="container pt-5 hidden_footer_section">
      <div class="row mt-5">
        <div class="col-lg-3 col-md-6">
          <div class="footer-link">
            <h5 class="text-dark mb-4">Pages</h5>
            <ul class="list-unstyled">
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('About-Us')}}">About Us</a></li>
                <li><a href="{{url('Blog')}}">Blog</a></li>
                <li><a href="{{url('Library')}}">Library</a></li>
                <li><a href="{{url('Contact-Us')}}">Contact Us</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-link">
            <h5 class="text-dark mb-4">Latest Articles</h5>
            <ul class="list-unstyled">
                @foreach ($footer_articles as $article)
                    <li>
                      <a href="{{url('Article/'.$article->id)}}">
                          {{$article->title}}
                      </a>
                    </li>
                @endforeach
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
          <div class="footer-link">
            <h5 class="text-dark mb-4">Latest Books</h5>
            <ul class="list-unstyled">
                @foreach ($footer_books as $book)
                    <li>
                      <a href="{{url('Library-File/'.$book->id)}}">
                          {{$book->title}}
                      </a>
                    </li>
                @endforeach
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 mt-4 mt-lg-0">
          <h5 class="text-dark mb-4">Subscribe Us</h5>
          <div class="footer-subscribe">
            <p>Sign Up to our Newsletter to get the latest news and offers.</p>
            <form id="subscribe_form" action="{{url('subscribe')}}" method="post">
                {{csrf_field()}}
                <div id="subscribe-res"></div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <button type="submit" class="btn btn-primary btn-md">Subscribe Now</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom bg-dark mt-5">
      <div class="container">
        <div class="row">
          <div class="col-md-6 ">
            <div class="d-flex justify-content-md-start justify-content-center">
                <p class="mb-0"> &copy;Copyright <span id="copyright">{{date('Y')}}</span> <a href="#"> Matrix Dictionary </a> All Rights Reserved </p>
            </div>
          </div>
          <div class="col-md-6 text-center text-md-right mt-4 mt-md-0">
            <p class="mb-0"> Designed & Developed By <a href="http://matrixclouds.com/ar" target="_blank"> Matrix Clouds </a></p>
          </div>
        </div>
      </div>
    </div>
  </footer>
<!--=================================
Footer-->

<!--=================================
Back To Top-->
   <div id="back-to-top" class="back-to-top">
     <i class="fas fa-angle-up"></i>
   </div>
<!--=================================
Back To Top-->

@if(!Auth::check())
<!--=================================
Signin Modal Popup -->
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header p-4">
        <h4 class="mb-0 text-center">Login to Your Account</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="login-register">
          <div class="tab-content">
            <div class="tab-pane active" id="candidate" role="tabpanel">
              <form class="mt-4" method="post" action="{{url('Login')}}" id="login_form">
                {{csrf_field()}}
                <div id="login_results"></div>


                <div class="form-row">
                  <div class="form-group col-12">
                    <label for="email">Email Address:</label>
                    <input type="email" class="form-control" id="email" name="email" />
                  </div>
                  <div class="form-group col-12">
                    <label for="password">Password*</label>
                    <input type="password" class="form-control" id="password" name="password" />
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                  </div>
                  <div class="col-md-6">
                    <div class="ml-md-3 mt-3 mt-md-0 forgot-pass">
                      <a href="#">Forgot Password?</a>
                      <p class="mt-1">Don't have account? <a href="{{url('Register')}}">Sign Up here</a></p>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--=================================
Signin Modal Popup -->
@endif
<!--=================================
Javascript -->

    <!-- JS Global Compulsory (Do not remove)-->
    <script src="{{asset('site/js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('site/js/popper/popper.min.js')}}"></script>
    <script src="{{asset('site/js/bootstrap/bootstrap.min.js')}}"></script>

    <!-- Page JS Implementing Plugins (Remove the plugin script here if site does not use that feature)-->
    <script src="{{asset('site/js/owl-carousel/owl-carousel.min.js')}}"></script>
    <script src="{{asset('site/js/slideshow/jquery.velocity.min.js')}}"></script>
    <script src="{{asset('site/js/slideshow/jquery.kenburnsy.js')}}"></script>

    <script src="{{asset('site/js/jquery.appear.js')}}"></script>
    <script src="{{asset('site/js/counter/jquery.countTo.js')}}"></script>
    <script src="{{asset('site/js/owl-carousel/owl-carousel.min.js')}}"></script>
    <script src="{{asset('site/js/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    
    <!-- map -->
    <script src="{{asset('site/js/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{asset('assets/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
    <!-- Template Scripts (Do not remove)-->
    <script src="{{asset('site/js/custom.js?v=3')}}"></script>
    @yield('script')
  {{--

    <script>
        var options = {
            chart: {
                height: 350,
                type: 'area',
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            series: [{
                name: 'series1',
                data: [31, 40, 28, 51, 42, 109, 100]
            }, {
                name: 'series2',
                data: [11, 32, 45, 32, 34, 52, 41]
            }],
            colors: ['#ff8a00', '#001935'],

            xaxis: {
                type: 'datetime',
                categories: ["2018-09-19T00:00:00", "2018-09-19T01:30:00", "2018-09-19T02:30:00", "2018-09-19T03:30:00", "2018-09-19T04:30:00", "2018-09-19T05:30:00", "2018-09-19T06:30:00"],
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#chart"),
            options
        );

        chart.render();
    </script>
  --}}
</body>
</html>