<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link href="{{asset('site/images/favicon.ico')}}" rel="shortcut icon" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">

    <!-- CSS Global Compulsory (Do not remove)-->
    <link rel="stylesheet" href="{{asset('site/css/font-awesome/all.min.css')}}" />
    <link rel="stylesheet" href="{{asset('site/css/flaticon/flaticon.css')}}" />
    <link rel="stylesheet" href="{{asset('site/css/bootstrap/bootstrap.min.css')}}" />

    <!-- Page CSS Implementing Plugins (Remove the plugin CSS here if site does not use that feature)-->
    <link rel="stylesheet" href="{{asset('site/css/range-slider/ion.rangeSlider.css')}}" />
    <link rel="stylesheet" href="{{asset('site/css/owl-carousel/owl.carousel.min.css')}}" />
    <link rel="stylesheet" href="{{asset('site/css/subtle-slideshow.css')}}" />

    <!-- Page CSS Implementing Plugins (Remove the plugin CSS here if site does not use that feature)-->
    <link rel="stylesheet" href="{{asset('site/css/magnific-popup/magnific-popup.css')}}" />
    
    
    <!-- map -->
    <link rel="stylesheet" href="{{asset('site/css/apexcharts/apexcharts.css')}}" />


    <!-- Template Style -->
    <link rel="stylesheet" href="{{asset('site/css/style.css')}}" />
    <link rel="stylesheet" href="{{asset('site/css/custom.css?v=1.1')}}" />
    @yield('styles')
  </head>