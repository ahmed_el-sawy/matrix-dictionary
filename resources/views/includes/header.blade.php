<body>

<!--================================= Header -->
<header class="header bg-dark">
  <nav class="navbar navbar-static-top navbar-expand-lg header-sticky">
    <div class="container-fluid">
      <button id="nav-icon4" type="button" class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse">
          <span></span>
          <span></span>
          <span></span>
      </button>
      <a class="navbar-brand" href="{{url('/')}}">
        <img class="img-fluid" src="{{asset('site/images/logo.png')}}" alt="logo">
      </a>
      <div class="navbar-collapse collapse justify-content-start">
        <ul class="nav navbar-nav">
          <li class="nav-item dropdown active"><a class="nav-link" href="{{url('/')}}">Home</a></li>
          <li class="nav-item dropdown"><a class="nav-link" href="{{url('/#About-Us')}}">About Us</a></li>
          <li class="nav-item dropdown"><a class="nav-link" href="{{url('Blog')}}">Blog</a></li>
          <li class="nav-item dropdown"><a class="nav-link" href="{{url('Library')}}">Library</a></li>
          <li class="nav-item dropdown"><a class="nav-link" href="{{url('Contact-Us')}}">Contact Us</a></li>
          @if(Auth::check())
              <li class="nav-item dropdown"><a class="nav-link" href="{{url('Profile')}}">My Profile</a></li>
          @endif  
        </ul>
      </div>
      <div class="add-listing">
          @if(!Auth::check())
            <div class="login d-inline-block mr-4">
              <a href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal"><i class="far fa-user pr-2"></i>Sign in</a>
            </div>
          @endif
          </div>
    </div>
  </nav>
</header>
<!--================================= Header -->