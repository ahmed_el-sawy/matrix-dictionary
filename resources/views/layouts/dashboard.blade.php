<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->

<head>
    <base href="/dictionary/">
    <meta charset="utf-8" />
    <title>Dictionary | Dashboard</title>
    <meta name="description" content="Updates and statistics">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Fonts -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link href="https://fonts.googleapis.com/css2?family=Tajawal&display=swap" rel="stylesheet">
    <!--end::Fonts -->
<!--begin::Page Custom Styles(used by this page) -->
    <link href="{{asset('assets/css/pages/wizard/wizard-4.css')}}" rel="stylesheet" type="text/css" />

<!--end::Page Custom Styles -->
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{asset('assets/css/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/brand/dark.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/aside/dark.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.ico.css')}}" />
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->

    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="{{ url('admin/') }}">
                <img alt="Logo" src="{{asset('assets/media/logos/logo-light.png')}}" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left"
                id="kt_aside_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more"></i></button>
        </div>
    </div>

    <!-- end:: Header Mobile -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            <!-- begin:: Aside -->

            <!-- Uncomment this to display the close button of the panel
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
-->
            <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"
                id="kt_aside">

                <!-- begin:: Aside -->
                <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                    <div class="kt-aside__brand-logo">
                        <a href="{{ url('admin/') }}">
                            <img alt="Logo" src="{{asset('assets/media/logos/logo-light.png')}}" />
                        </a>
                    </div>
                    <div class="kt-aside__brand-tools">
                        <button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
                            <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path
                                            d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
                                            fill="#000000" fill-rule="nonzero"
                                            transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
                                        <path
                                            d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3"
                                            transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
                                    </g>
                                </svg></span>
                            <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24" />
                                        <path
                                            d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z"
                                            fill="#000000" fill-rule="nonzero" />
                                        <path
                                            d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z"
                                            fill="#000000" fill-rule="nonzero" opacity="0.3"
                                            transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                                    </g>
                                </svg></span>
                        </button>

                        <!--
			<button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
			-->
                    </div>
                </div>

                <!-- end:: Aside -->

                <!-- begin:: Aside Menu -->

                <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
                    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1"
                        data-ktmenu-dropdown-timeout="500">
                        <ul class="kt-menu__nav ">
                            <li class="kt-menu__item" aria-haspopup="true"><a href="{{ url('/admin/dashboard') }}"
                                    class="kt-menu__link ">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-home"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Dashboard</span></a>
                            </li>
                            <li class="kt-menu__item" aria-haspopup="true"><a href="{{ url('/admin/search') }}"
                                    class="kt-menu__link ">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-search"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Search</span></a>
                            </li>
                            {{-- -Languages- --}}
                            <li class="kt-menu__item" aria-haspopup="true"><a href="{{ route('languages.index') }}"
                                                                              class="kt-menu__link ">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-globe"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Languages</span></a>
                            </li>
                            {{--  End Languages  --}}

                            {{-- -Countries- --}}
                            <li class="kt-menu__item" aria-haspopup="true"><a href="{{ route('countries.index') }}"
                                                                                                     class="kt-menu__link ">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-list"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Countries</span></a>
                            </li>
                            {{--   End Countires  --}}

                            {{-- -Jobs- --}}
                            <li class="kt-menu__item" aria-haspopup="true"><a href="{{ route('jobs.index') }}"
                                                                              class="kt-menu__link ">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-list"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Jobs</span></a>
                            </li>
                            {{--  End Jobs  --}}
                            <!-- USers -->
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                    class="kt-menu__link kt-menu__toggle">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-users"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Users</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span
                                                class="kt-menu__link"><span
                                                    class="kt-menu__link-text">Users</span></span>
                                        </li>
                                        @if(Auth::user()->su == 1)
                                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                                data-ktmenu-submenu-toggle="hover">
                                                <a href="{{ route('roles.index') }}" class="kt-menu__link kt-menu__toggle">
                                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                    <span class="kt-menu__link-text">Roles</span>
                                                </a>
                                            </li>
                                            {{--                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"--}}
                                            {{--                                            data-ktmenu-submenu-toggle="hover">--}}
                                            {{--                                            <a href="{{ url('admin/managers') }}" class="kt-menu__link kt-menu__toggle">--}}
                                            {{--                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>--}}
                                            {{--                                                <span class="kt-menu__link-text">Managers</span>--}}
                                            {{--                                            </a>--}}
                                            {{--                                        </li>--}}
                                        @endif
                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                            data-ktmenu-submenu-toggle="hover">
                                            <a href="{{ url('admin/users') }}" class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                <span class="kt-menu__link-text">Users</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- End Users -->
                            {{-- Categories --}}
                            <li class="kt-menu__item" aria-haspopup="true"><a href="{{ route('categories.index') }}"
                                                                              class="kt-menu__link ">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-list"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Categories</span></a>
                            </li>
                            {{--  End Categories  --}}

                            {{-- Dictionaries --}}
                            {{--
                             <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                    class="kt-menu__link kt-menu__toggle">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-book"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Dictionaries</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span
                                                class="kt-menu__link"><span
                                                    class="kt-menu__link-text">Dictionaries</span></span>
                                        </li>

                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                            data-ktmenu-submenu-toggle="hover">
                                            <a href="{{ route('dictionaries.index') }}" class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                <span class="kt-menu__link-text">Dictionaries</span>
                                            </a>
                                        </li>

                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                            data-ktmenu-submenu-toggle="hover">
                                            <a href="{{ route('words.create') }}" class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                <span class="kt-menu__link-text">Add Dictionary Words</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            --}}
                            <li class="kt-menu__item" aria-haspopup="true">
                                <a href="{{ route('dictionaries.index') }}" class="kt-menu__link">
                                    <span class="kt-menu__link-icon"><i class="fa fa-book"></i></span>
                                    <span class="kt-menu__link-text">Dictionaries</span>
                                </a>
                            </li>
                            {{--  End Dictionaries  --}}
                            {{-- Books --}}
                            <li class="kt-menu__item" aria-haspopup="true"><a href="{{ route('books.index') }}"
                                                                              class="kt-menu__link ">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-book"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Books</span></a>
                            </li>
                            {{--  End Books  --}}
                            {{-- Slider --}}
                             <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                                    class="kt-menu__link kt-menu__toggle">
                                    <span class="kt-menu__link-icon">
                                        <i class="fa fa-home"></i>
                                    </span>
                                    <span class="kt-menu__link-text">Home Page</span>
                                    <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span
                                                class="kt-menu__link"><span
                                                    class="kt-menu__link-text">Home Page</span></span>
                                        </li>

                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                            data-ktmenu-submenu-toggle="hover">
                                            <a href="{{ route('slider.index') }}" class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                <span class="kt-menu__link-text">Slider</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                            data-ktmenu-submenu-toggle="hover">
                                            <a href="{{ url('admin/banner_under_slider') }}" class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                <span class="kt-menu__link-text">Banner</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                            data-ktmenu-submenu-toggle="hover">
                                            <a href="{{ url('admin/categories_section') }}" class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                <span class="kt-menu__link-text">Categories Section</span>
                                            </a>
                                        </li>
                                        
                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                                <span class="kt-menu__link-text">About Us</span>
                                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                            </a>
                                            <div class="kt-menu__submenu ">
                                                <span class="kt-menu__arrow"></span>
                                                <ul class="kt-menu__subnav">
                                                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                                        <a href="{{ url('admin/home_about_us') }}" class="kt-menu__link kt-menu__toggle">
                                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                            <span class="kt-menu__link-text">About Us</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                                        <a href="{{ url('admin/home_about_us_sections') }}" class="kt-menu__link kt-menu__toggle">
                                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                            <span class="kt-menu__link-text">Sections</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li> 
                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                            data-ktmenu-submenu-toggle="hover">
                                            <a href="{{ url('admin/search_words') }}" class="kt-menu__link kt-menu__toggle">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                <span class="kt-menu__link-text">Search Words</span>
                                            </a>
                                        </li>
                                        
                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                                <span class="kt-menu__link-text">Working Process</span>
                                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                            </a>
                                            <div class="kt-menu__submenu ">
                                                <span class="kt-menu__arrow"></span>
                                                <ul class="kt-menu__subnav">
                                                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                                        <a href="{{ url('admin/work_process') }}" class="kt-menu__link kt-menu__toggle">
                                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                            <span class="kt-menu__link-text">Working Process</span>
                                                        </a>
                                                    </li>
                                                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                                        <a href="{{ url('admin/home_work_process_sections') }}" class="kt-menu__link kt-menu__toggle">
                                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                                            <span class="kt-menu__link-text">Sections</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li> 
                                        
                                </div>
                            </li>
                            {{--  End Slider  --}}
                        </ul>
                    </div>
                </div>

                <!-- end:: Aside Menu -->
            </div>

            <!-- end:: Aside -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <!-- begin:: Header -->
                <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

                    <!-- begin:: Header Menu -->

                    <!-- Uncomment this to display the close button of the panel
<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
-->
                    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                        <div id="kt_header_menu"
                            class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
                            <ul class="kt-menu__nav ">

                            </ul>
                        </div>
                    </div>

                    <!-- end:: Header Menu -->

                    <!-- begin:: Header Topbar -->
                    <div class="kt-header__topbar">
                        <!--begin: Language bar -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--langs">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                <span class="kt-header__topbar-icon">
                                    <img class="" src="{{asset('assets/media/flags/226-united-states.svg')}}" alt="" />
                                </span>
                            </div>
                            <div
                                class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
                                <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                                    <li class="kt-nav__item kt-nav__item--active">
                                        <a href="#" class="kt-nav__link">
                                            <span class="kt-nav__link-icon"><img
                                                    src="{{asset('assets/media/flags/226-united-states.svg')}}" alt="" /></span>
                                            <span class="kt-nav__link-text">English</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <span class="kt-nav__link-icon"><img src="{{asset('assets/media/flags/128-spain.svg')}}"
                                                    alt="" /></span>
                                            <span class="kt-nav__link-text">Spanish</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <span class="kt-nav__link-icon"><img
                                                    src="{{asset('assets/media/flags/162-germany.svg')}}" alt="" /></span>
                                            <span class="kt-nav__link-text">German</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!--end: Language bar -->

                        <!--begin: User Bar -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--user">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                                <div class="kt-header__topbar-user">
                                    <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                                    <span class="kt-header__topbar-username kt-hidden-mobile">{{ auth()->user()->name }}</span>
                                    <img class="kt-hidden" alt="Pic" src="{{asset('assets/media/users/300_25.jpg')}}" />

                                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                    <span
                                        class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">S</span>
                                </div>
                            </div>
                            <div
                                class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

                                <!--begin: Head -->
                                <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                                    style="background-image: url({{asset('assets/media/misc/bg-1.jpg')}})">
                                    <div class="kt-user-card__avatar">
                                        <img class="kt-hidden" alt="Pic" src="{{asset('assets/media/users/300_25.jpg')}}" />

                                        <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                        <span
                                            class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">S</span>
                                    </div>
                                    <div class="kt-user-card__name">
                                    {{ auth()->user()->name }}
                                    </div>
                                    <div class="kt-user-card__badge">
                                        <span class="btn btn-success btn-sm btn-bold btn-font-md">23 messages</span>
                                    </div>
                                </div>

                                <!--end: Head -->

                                <!--begin: Navigation -->
                                <div class="kt-notification">
                                    <a href="{{ route('users.edit', Auth::user()->id) }}"
                                        class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-calendar-3 kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title kt-font-bold">
                                                My Profile
                                            </div>
                                            <div class="kt-notification__item-time">
                                                Account settings and more
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-mail kt-font-warning"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title kt-font-bold">
                                                My Messages
                                            </div>
                                            <div class="kt-notification__item-time">
                                                Inbox and tasks
                                            </div>
                                        </div>
                                    </a>
                                    <div class="kt-notification__custom kt-space-between">
                                        <a class="btn btn-label btn-label-brand btn-sm btn-bold" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </div>

                                <!--end: Navigation -->
                            </div>
                        </div>

                        <!--end: User Bar -->
                    </div>

                    <!-- end:: Header Topbar -->
                </div>

                <!-- end:: Header -->
                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                    <!-- begin:: Content Head -->
                    <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                        <div class="kt-container  kt-container--fluid ">
                            <div class="kt-subheader__main">
                                <h3 class="kt-subheader__title"><a href="{{ url('/admin') }}">Home</a></h3>
                                <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                                <span class="kt-subheader__desc">{!! @$page_name !!}</span>
                                <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                        <span><i class="flaticon2-search-1"></i></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- end:: Content Head -->

                    <!-- begin:: Content -->
                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                        <!--Begin::Dashboard 1-->
                        @if (session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                        @elseif (session('error'))
                            <div class="alert alert-danger">{{ session('error') }}</div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @yield('content')

                        <!--End::Dashboard 1-->
                    </div>

                    <!-- end:: Content -->
                </div>

                <!-- begin:: Footer -->
                <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-footer__copyright">
                            2020&nbsp;&copy;&nbsp;<a href="https://matrixclouds.com" target="_blank"
                                class="kt-link">Matrix Clouds</a>
                        </div>
                    </div>
                </div>

                <!-- end:: Footer -->
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>

    <div class="modal" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document" >
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Show More</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };

    </script>

    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->

    <!--begin::Page Vendors(used by this page) -->
{{--    <script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>--}}
    <script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript">
    </script>
    <script src="{{asset('assets/plugins/custom/gmaps/gmaps.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/dashboard.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $.each($('a.kt-menu__link'), function() {
            if (window.location.href == $(this).attr('href')) {
                console.log($(this));
                $(this).parents().addClass('kt-menu__item--open kt-menu__item--active');
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('body').on('change', '#category_selector', function (){
            var cat = $(this).val();
            var action = $(this).attr('data-url');
            $.ajax({
                type: 'POST',
                data: {cat: cat},
                url: action,
                success: function(data) 
                {
                    $('#sub_category_selector').html(data);
                    $('#sub_category_selector').select2();                
                    
                }
            }); 
            return false;
        });
    </script>
    @yield('footer-js')
    <!--end::Page Scripts -->
</body>

<!-- end::Body -->

</html>
