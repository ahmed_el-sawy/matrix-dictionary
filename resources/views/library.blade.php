@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('content')
<!--=================================
banner -->
<section class="header-inner header-inner-big bg-holder text-white" style="background-image: url({{asset('site/images/bg/banner-01.jpg')}});">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="job-search-field">
          <div class="job-search-item">
            <form class="form row" method="get" action="{{url('Library')}}">
              <div class="col-lg-5">
                <div class="form-group left-icon">
                  <input type="text" class="form-control" name="search" placeholder="Enter Keyword ..." value="{{$search_filter}}">
                  <i class="fas fa-search"></i>
                  </div>
              </div>
              <div class="col-lg-5">
                <select class="form-control" name="cat[]">
                  <option value="All">All Categories</option>
                  @foreach ($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->title}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-2 col-sm-12">
                <div class="form-group form-action">
                  <button type="submit" class="btn btn-primary mt-0"><i class="fas fa-search-location"></i> Search</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
banner -->

<!--=================================
job-list -->
<section class="space-ptb">
<div class="container">
  <div class="row">
    <div class="col-lg-3">
      <form action="{{url('Library')}}" method="get" id="SideBarFilterForm">
      <input type="hidden" name="search" value="{{$search_filter}}" />
      <!--=================================
      left-sidebar -->
      <div class="sidebar">
          <div class="widget">
            <div class="widget-title widget-collapse">
              <h6>Categories</h6>
              <a class="ml-auto" data-toggle="collapse" href="#categories" role="button" aria-expanded="false" aria-controls="categories">
                <i class="fas fa-chevron-down"></i>
              </a>
              </div>
              <div class="collapse show" id="categories">
                <div class="widget-content">
                  @foreach ($cats as $cat)
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input Formcheckbox" name="cat[]" id="cat{{$loop->iteration}}" 
                      value="{{$cat->id}}" @if(in_array($cat->id, $cats_filter)) checked @endif>
                      <label class="custom-control-label" for="cat{{$loop->iteration}}">{{$cat->title}}</label>
                    </div>
                  @endforeach
              </div>
            </div>
            </div>
            <hr>
            <div class="widget">
              <div class="widget-add"> <img class="img-fluid" src="{{asset('site/images/add-banner.png')}}" alt=""></div>
            </div>
          </div>
        </div>
        <div class="col-lg-9">
          <!--=================================
          right-sidebar -->
          <div class="row mb-4">
              <div class="col-md-6">
              <div class="section-title mb-0">
                <h6 class="mb-0">Showing {{($curent_page-1)*10 + 1}}-@if(($curent_page-1)*10 + 10 <= $all_books) {{($curent_page-1)*10 + 10}} @else {{$all_books}} @endif of <span class="text-primary">{{$all_books}} File</span></h6>
              </div>
            </div>
          <div class="col-md-6">
          <div class="job-filter mb-0 d-sm-flex align-items-center">
            <div class="job-shortby ml-sm-auto d-flex align-items-center">
              <form class="form-inline">
                <div class="form-group mb-0">
                  <label class="justify-content-start mr-2">sort by :</label>
                  <div class="short-by">
                    <select class="form-control basic-select">
                      <option>Newest</option>
                      <option>Oldest</option>
                    </select>
                  </div>
                </div>
              </form>
            </div>
          </div>
          </div>
          </div>
          <div class="row">
          @foreach($books as $book)
          <div class="col-md-4 mb-3">
            <div class="candidate-list candidate-grid">
              <div class="candidate-list-image">
                    <a href="{{url('Library-File/'.$book->id)}}">
                        <img class="img-fluid library_image" src="{{asset('site/images/library/pdf.png')}}" alt="{{$book->title}}">
                    </a>
              </div>
              <div class="candidate-list-details">
                <div class="candidate-list-info">
                  <div class="candidate-list-title">
                    <h5><a href="{{url('Library-File/'.$book->id)}}">{{$book->title}}</a></h5>
                  </div>
                  <div class="candidate-list-option">
                    <ul class="list-unstyled library_list">
                      <li><i class="fas fa-filter pr-1"></i>@if($book->cat_info) {{$book->cat_info->title}} @endif</li>
                      <li><i class="fas fa-language pr-1"></i>{{$book->language_info->language}}</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="candidate-list-favourite-time">

                <a 
                  @if(!Auth::check())
                    href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal" class="candidate-list-favourite order-2 text-danger"
                  @else
                    href="{{url('Favourite-Book/'.$book->id)}}" class="candidate-list-favourite order-2 text-danger like_book" 
                    id="LibraryLike{{$book->id}}" data-target="#LibraryLike{{$book->id}}"
                  @endif
                >
                @if(!Auth::check())
                <i class="far fa-heart"></i>
                @else
                @if(my_book($book->id, Auth::user()->id))
                    <i class="fas fa-heart"></i>
                  @else
                    <i class="far fa-heart"></i>
                  @endif
                  @endif
                </a>
                <span class="candidate-list-time order-1"><i class="far fa-clock pr-1"></i>{{$book->created_at->diffForHumans()}}</span>
              </div>
            </div>
          </div>
          @endforeach
          </div>
          {{$books->appends($_GET)->links()}}
        </div>
      </div>
    </div>
</section>
<!--=================================
job-list -->

@endsection
