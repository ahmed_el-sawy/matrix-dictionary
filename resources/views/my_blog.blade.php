@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('content')

@include('profile_menu')


<!--=================================
My Blog -->

<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="user-dashboard-info-box mb-0">
          <div class="row mb-4">
            <div class="col-md-7 col-sm-5 d-flex align-items-center">
              <div class="section-title-02 mb-0 ">
                <h4 class="mb-0">My Blog <a href="{{url('New-Article')}}" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> Post New Article</a></h4>
              </div>
            </div>
            @if(count($articles) > 0)
            <div class="col-md-5 col-sm-7 mt-3 mt-sm-0">
              <form action="#" class="search" method="get">
                <i class="fas fa-search"></i>
                <input type="text" class="form-control" placeholder="Search..." name="search">
              </form>
            </div>
            @endif
          </div>
          @if(count($articles) > 0)
          <div class="user-dashboard-table table-responsive">
            <table class="table table-bordered">
              <thead class="bg-light">
                <tr >
                  <th scope="col">Title</th>
                  <th scope="col">Publish Date</th>
                  <th scope="col">Status</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($articles as $article)
                <tr>
                  <td>{{$article->title}}</td>
                    <td>{{date('Y-m-d', strtotime($article->created_at))}}</td>
                  <td>
                    <ul class="list-unstyled mb-0 d-flex">
                      <li><a href="{{url('Article/'.$article->id)}}" class="text-primary" data-toggle="tooltip" title="view"><i class="far fa-eye"></i></a></li>
                      <li><a href="{{url('Edit-Article/'.$article->id)}}" class="text-info" data-toggle="tooltip" title="Edit"><i class="fas fa-pencil-alt"></i></a></li>
                      <li><a href="{{url('Delete-Article/'.$article->id)}}" class="text-danger" data-toggle="tooltip" title="Delete"><i class="far fa-trash-alt"></i></a></li>
                    </ul>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="row justify-content-center">
            <div class="col-12 text-center">
              {{$articles->appends($_GET)->links()}}
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
My Blog -->

@endsection