@extends('layouts.app')
@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('styles')
<style>
    .hidden_footer_section {display: none;}
    .footer.footer.bg-light {margin: 0; padding: 0;}
</style>
@endsection

@section('content')

@include('profile_menu')

<!--=================================
Manage Jobs -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="user-dashboard-info-box mb-0">
          <div class="row mb-4">
            <div class="col-md-7 col-sm-5 d-flex align-items-center">
              <div class="section-title-02 mb-0 ">
                <h4 class="mb-0">My Library <a data-toggle="modal" data-target="#NewBook" href="#" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> New File</a></h4>
              </div>
            </div>
            @if(false)
              <div class="col-md-5 col-sm-7 mt-3 mt-sm-0">
                <form action="#" class="search" method="get">
                  <i class="fas fa-search"></i>
                  <input type="text" class="form-control" placeholder="Search..." name="search">
                </form>
              </div>
            @endif
          </div>


          <div class="modal fade" id="NewBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add New Book</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form class="kt-form" id="kt_user_add_form" method="POST" action="{{ url('Save-New-Book') }}" enctype="multipart/form-data">
                                <!--begin: Form Wizard Step 1-->
                                @csrf
                                <div id="kt_user_add_form_result"></div>
                                  <div class="row">
                                    <div class="form-group col-12 col-md-6">
                                      <label class="col-form-label">Book File</label>                                                            
                                      <input type="file" name="book" class="form-control"/>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
									  	                <label class="col-form-label">Title</label>
										                  <input type="text" name="title" class="form-control" value="{{ old('title') }}"/>
									                  </div>
                                	  <div class="form-group col-12 col-md-6">
                                    	<label class="col-form-label">Source</label>
										                  <input type="text" name="source" class="form-control" value="{{ old('source') }}"/>
                                  	</div>
                                    <div class="form-group col-12 col-md-6">
										                  <label class="col-form-label">Copy Right</label>
                                      <input type="text" name="copyRight" class="form-control" value="{{ old('copyRight') }}"/>
                                   	</div>
                                    <div class="form-group col-12 col-md-6">
                                    	<label class="col-form-label">Author</label>
										                  <input type="text" name="author" class="form-control" value="{{ old('author') }}"/>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
										                  <label class="col-form-label">Language</label>
                                      <select class="form-control" name="language">
                                        <option value="">Choose Language</option>
                                          @foreach($languages as $language)
                                            <option value="{{ $language->id }}">{{ $language->language }}</option>
                                          @endforeach
										                    </select>
									                    </div>
                                    <div class="form-group col-12 col-md-6">
                                      <label class="col-form-label">Category</label>
                                      <select class="form-control" name="category">
                                        <option value="">Choose Category</option>
                                        @foreach($categories as $category)
                                          <option value="{{ $category->id }}">{{ $category->title }}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                      <label class="col-form-label">Sub Category</label>
                                      <select class="form-control" name="sub_category">
											                <option value="">Choose Category</option>
                                      @foreach($sub_categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
											                @endforeach
                                      </select>
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                      <label class="col-form-label">Status</label>
                                      <div class="kt-radio-inline">
                                        <div class="custom-control custom-radio custom-control-inline">
                                          <input type="radio" id="customRadioInline1" class="custom-control-input"
                                           name="status" value="1" checked /> 
                                           <label class="custom-control-label" for="customRadioInline1">Active</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                          <input type="radio" id="customRadioInline2" class="custom-control-input" name="status" value="0" /> 
                                          <label class="custom-control-label" for="customRadioInline2">Not Active</label>
                                        </div>
                                      </div>
                                     </div>
                                     <div class="form-group col-12 col-md-6">
                                      <label class="col-form-label">File Privacy</label>
                                      <div class="kt-radio-inline">
                                        <div class="custom-control custom-radio custom-control-inline">
                                          <input type="radio" id="customRadioInline3" class="custom-control-input"
                                           name="privacy" value="1" checked /> 
                                           <label class="custom-control-label" for="customRadioInline3">Private</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                          <input type="radio" id="customRadioInline4" class="custom-control-input" name="privacy" value="0" /> 
                                          <label class="custom-control-label" for="customRadioInline4">Public</label>
                                        </div>
                                      </div>
                                     </div>
                                    <div class="form-group col-12 col-md-6 text-right">
                                        <br />
                                        <button type="submit" class="btn btn-primary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Save</button>
                                    </div>
                                  </div>
                                <!--end: Form Actions -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>

          <div class="user-dashboard-table table-responsive">
            
          <table class="table table-bordered data_table">
						<thead>
							<tr>
								<th>#</th>
								<th>Title</th>
								<th>Status</th>
								<th>Added At</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($data as $book)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$book->title}}</td>
									<td>
									    @if($book->status == 1) <span class="badge badge-success">Active</span> @else <span class="badge badge-danger">Not Active</span> @endif
									    @if($book->privacy == 1) <span class="badge badge-info">Private</span> @else <span class="badge badge-info">Public</span> @endif
									</td>
									<td>{{date('d/m/Y', strtotime($book->created_at))}}</td>
									<td>
                    <ul class="list-unstyled mb-0 d-flex">
                      <li><a href="{{url('/Library-File/'.$book->id)}}" class="text-primary" data-toggle="tooltip" title="view"><i class="far fa-eye"></i></a></li>
                      @if(Auth::user()->id == $book->user)
                      	<li><a class="text-info" data-toggle="modal" href="#myModalRR-{{ $book->id }}"><i class="fa fa-edit"></i></a></li>
                      	<li><a class="text-danger" data-toggle="modal" href="#myModal-{{ $book->id }}"><i class="fa fa-trash"></i></a></li>
						            <div class="modal fade" id="myModal-{{ $book->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                          <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Delete Book</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            </button>
                          </div>
                          <div class="modal-body">
                          <form role="form" action="{{ url('/delete_book/'.$book->id) }}" class="" method="POST">
                          {{ csrf_field() }}
                          <p>are you sure</p>
                          <button type="submit" class="btn btn-danger" name='delete_modal'><i class="fa fa-trash" aria-hidden="true"></i> delete</button>
                          </form>
                          </div>
                          </div>
                          </div>
                        </div>
                        
                      <div class="modal fade" id="myModalRR-{{ $book->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Book</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="kt-form kt_user_edit_form" data-book="{{$book->id}}" method="POST" action="{{ url('Update-Book/'.$book->id) }}" enctype="multipart/form-data">
                                        <!--begin: Form Wizard Step 1-->
                                        @csrf
                                        <div id="kt_user_edit_form_result_{{$book->id}}"></div>
                                        <div class="row">
                                            <div class="form-group col-12 col-md-6">
                                              <label class="col-form-label">Book File</label>                                                            
                                                <input type="file" name="book" class="form-control"/>
                                            </div>
                                            
                                            <div class="form-group col-12 col-md-6">
                                                <label class="col-form-label">Title</label>
                                                <input type="text" name="title" class="form-control" value="{{ $book->title }}"/>
                                          </div>
                                            
                                            <div class="form-group col-12 col-md-6">
                                                <label class="col-form-label">Source</label>
                                                <input type="text" name="source" class="form-control" value="{{ $book->source }}"/>
                                            </div>
                                            
                                            <div class="form-group col-12 col-md-6">
                                                <label class="col-form-label">Copy Right</label>
                                                <input type="text" name="copyRight" class="form-control" value="{{ $book->copyRight }}"/>
                                            </div>
                                            
                                            <div class="form-group col-12 col-md-6">
                                                <label class="col-form-label">Author</label>
                                                <input type="text" name="author" class="form-control" value="{{ $book->author }}"/>
                                            </div>
                                            
                                            <div class="form-group col-12 col-md-6">
                                                <label class="col-form-label">Language</label>
                                                <select class="form-control" name="language">
                                                    <option value="">Choose Language</option>
                                                    @foreach($languages as $language)
                                                      <option value="{{ $language->id }}" @if($book->language ==  $language->id) selected @endif>
                                                        {{ $language->language }}
                                                      </option>
                                                    @endforeach
                                                </select>
                                              </div>
                                            
                                            <div class="form-group col-12 col-md-6">
                                                <label class="col-form-label">Category</label>
                                                <select class="form-control" name="category">
                                                    <option value="">Choose Category</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}" @if($book->category ==  $category->id) selected @endif>
                                                            {{ $category->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                            <div class="form-group col-12 col-md-6">
                                                <label class="col-form-label">Sub Category</label>
                                                <select class="form-control" name="sub_category">
                                                    <option value="">Choose Category</option>
                                                    @foreach($sub_categories as $category)
                                                      <option value="{{ $category->id }}" @if($book->sub_category ==  $category->id) selected @endif>
                                                        {{ $category->title }}
                                                      </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                            <div class="form-group col-12 col-md-6">
                                              <label class="col-form-label">Status</label>
                                              <div class="kt-radio-inline">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                  <input type="radio" id="customRadioInline{{$book->id}}-1" class="custom-control-input"
                                                   name="status" value="1" @if($book->status == 1) checked @endif /> 
                                                   <label class="custom-control-label" for="customRadioInline{{$book->id}}-1">Active</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                  <input type="radio" id="customRadioInline{{$book->id}}-2" class="custom-control-input" name="status" value="0" 
                                                  @if($book->status == 0) checked @endif/> 
                                                  <label class="custom-control-label" for="customRadioInline{{$book->id}}-2">Not Active</label>
                                                </div>
                                              </div>
                                             </div>
                                                           
                                                           
                                            <div class="form-group col-12 col-md-6">
                                              <label class="col-form-label">File Privacy</label>
                                              <div class="kt-radio-inline">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                  <input type="radio" id="customRadioInline{{$book->id}}-3" class="custom-control-input"
                                                   name="privacy" value="1" @if($book->privacy == 1) checked @endif /> 
                                                   <label class="custom-control-label" for="customRadioInline{{$book->id}}-3">Private</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                  <input type="radio" id="customRadioInline{{$book->id}}-4" class="custom-control-input" name="privacy" value="0" 
                                                  @if($book->privacy == 0) checked @endif/> 
                                                  <label class="custom-control-label" for="customRadioInline{{$book->id}}-4">Public</label>
                                                </div>
                                              </div>
                                             </div>
                                                                                 </div>

                                        <div class="kt-form__actions">
                                            <button type="submit" class="btn btn-primary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u">Save</button>
                                        </div>
                                        <!--end: Form Actions -->
                                    </form>
                                </div>
                            </div>
                        </div>
                      </div>

                      @else
                      <!-- <a href="{{url('Dislike-Book/'.$book->id)}}" class="text-danger"><i class="fas fa-heart"></i></a> -->
              		    @endif            
					          </ul>
                    				</td>
								</tr>
							@endforeach
							
						</tbody>
					</table>

          </div>
          <div class="row justify-content-center">
            <div class="col-12 text-center">
              {{$data->links()}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Manage Jobs -->
@endsection