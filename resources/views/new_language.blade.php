<div class="row language_row">
  <input type="hidden" name="language_selector[]" value="0" />
  <div class="form-group col-md-6">
    <label>Language</label>
    <select class="form-control" name="language[]">
      <option value="" disabled selected>Choose Language</option>
      @foreach ($languages as $language)
        <option value="{{$language->id}}">{{$language->language}}</option>
      @endforeach
    </select>
  </div>
  <div class="form-group col-md-5">
  <label>Level</label>
    <select class="form-control" name="language_level[]">
      <option value="" disabled selected>Choose Language Level Skill</option>
      @foreach ($levels as $level)
        <option value="{{$level->id}}">{{$level->title}}</option>
      @endforeach
    </select>  
  </div>
  <div class="form-group col-md-1 text-center">
    <a class="mt-5 d-block delte_resume_language" href="Javascript:void(0);"><i class="far fa-trash-alt text-danger"></i></a>
  </div>
</div>
