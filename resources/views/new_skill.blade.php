<div class="row skill_row">
  <div class="form-group col-md-6">
    <label>Title</label>
    <input type="text" class="form-control" name="title[]" value="">
  </div>
  <div class="form-group col-md-5">
    <label>Percentage</label>
    <div class="input-group">
      <input type="number" class="form-control" name="percentage[]" value="">
      <div class="input-group-append">
        <span class="input-group-text">%</span>
      </div>
    </div>
  </div>
  <div class="form-group col-md-1 text-center">
    <a class="mt-5 d-block delte_resume_skill" href="Javascript:void(0);"><i class="far fa-trash-alt text-danger"></i></a>
  </div>
</div>
