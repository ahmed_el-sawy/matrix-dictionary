@if(!Auth::check())
    <p class="text-right"><a href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> Post New Article</a></p>
@else
    <p class="text-right"><a href="{{url('New-Article/'.$word->id)}}" class="btn btn-primary btn-sm"><i class="fas fa-plus-circle"></i> Post New Article</a></p>
@endif

@if(count($word->articles) > 0)
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Article</th>
                <th>User</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($word->articles as $article)
                <tr>
                    <td><a href="{{ url('Article/'.$article->article_info->id)}}">{{$article->article_info->title}}</a></td>
                    <td><a href="{{ url('View-Resume/'.$article->article_info->user_info->id)}}">{{$article->article_info->user_info->name}}</a></td>
                    <td>{{date('Y-m-d', strtotime($article->created_at))}}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif