@foreach ($word->comments as $comment)
    <div class="media mb-3">
    <div class="avatar avatar-lg">
        @if($comment->user_info->avatar != '')
            <img class="img-fluid  rounded-circle" src="{{asset($comment->user_info->avatar)}}" alt="{{$comment->user_info->name}}">
            @else
            <img class="img-fluid  rounded-circle" src="{{asset('site/images/avatar/00.jpg')}}" alt="{{$comment->user_info->name}}">
            @endif
    </div>
    <div class="media-body ml-3 border p-2">
        <div class="d-flex">
        <h6 class="mt-0">{{$comment->user_info->name}}</h6>
        <span class="ml-auto">{{$comment->created_at->diffForHumans()}}</span>
        </div>
        <p class="mb-0">{{$comment->comment}}</p>
    </div>
    </div>
@endforeach

@if(!Auth::check())
<form id="word_comment" method="post" action="{{url('word_comment')}}" data-num="{{$word->id}}">
    <div class="overlay">
        <h4>Please Login To Be Able To Publish Your Comment</h4>
        @if(!Auth::check())
            <p class="text-center"><a href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal" class="btn btn-primary btn-sm">Login</a></p>
        @endif
    </div>
    {{csrf_field()}}
    <input type="hidden" name="word" value="{{$word->id}}" />
    <div id="word_comment_red{{$word->id}}"></div>
    <div class="form-row">
    <div class="form-group col-md-12">
        <textarea class="form-control" rows="4" placeholder="Your Comment" name="comment"></textarea>
    </div>
    <div class="col-md-12">
        <button type="button" class="btn btn-primary">Publish Comment</a>
    </div>
    </div>
</form>
@else
<form id="word_comment" method="post" action="{{url('word_comment')}}" data-num="{{$word->id}}">
    {{csrf_field()}}
    <input type="hidden" name="word" value="{{$word->id}}" />
    <div id="word_comment_red{{$word->id}}"></div>
    <div class="form-row">
    <div class="form-group col-md-12">
        <textarea class="form-control" rows="4" placeholder="Your Comment" name="comment"></textarea>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">Publish Comment</a>
    </div>
    </div>
</form>
@endif