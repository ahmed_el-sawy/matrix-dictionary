@if(count($word->suggestions) > 0)
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Suggestions</th>
                <th>User</th>
                <th>Liked</th>
                <th>Disliked</th>
                <th>Date</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($word->suggestions as $suggestion)
                <tr>
                    <td>{{$suggestion->comment}}</td>
                    <td><a href="{{ url('View-Resume/'.$suggestion->user_info->id)}}">{{$suggestion->user_info->name}}</a></td>
                    <td>
                    @if(!Auth::check())
                        <a href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal"  class="text-success"><i class="far fa-heart"></i></a>
                    @else
                        <a href="{{url('like_suggesations/'.$suggestion->id)}}" class="text-success suggestions_like_action" data-num="{{$word->id}}">
                            @if(like_suggest($suggestion->id, Auth::user()->id))
                                <i class="fas fa-heart"></i>
                            @else
                                <i class="far fa-heart"></i>
                            @endif
                        </a>
                    @endif
                    {{count($suggestion->likes)}}
                    </td>    
                    <td>
                    @if(!Auth::check())
                        <a href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal"  class="text-success"><i class="far fa-heart"></i></a>
                    @else
                        <a href="{{url('dislike_suggesations/'.$suggestion->id)}}" class="text-danger suggestions_like_action" data-num="{{$word->id}}">
                            @if(dislike_suggest($suggestion->id, Auth::user()->id))
                                <i class="fas fa-heart"></i>
                            @else
                                <i class="far fa-heart"></i>
                            @endif
                        </a>
                    @endif
                    {{count($suggestion->dislikes)}}
                    </td>    
                    <td>{{date('Y-m-d', strtotime($suggestion->created_at))}}
                    <td>{{$suggestion->status}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@if(!Auth::check())
<form id="word_suggestion" method="post" action="{{url('word_suggestions')}}" data-num="{{$word->id}}">
    <div class="overlay">
        <h4>Please Login To Be Able To Publish Your Comment
        @if(!Auth::check())
            <a href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal" class="btn btn-primary btn-sm">Login</a>
        @endif
        </h4>
    </div>
    {{csrf_field()}}
    <input type="hidden" name="word" value="{{$word->id}}" />
    <div id="word_suggestion_red{{$word->id}}"></div>
    <div class="form-row">
    <div class="form-group col-md-10 col-sm-8 col-12">
        <input type="text" class="form-control" placeholder="Your Suggestion" name="comment" />
    </div>
    <div class="col-md-2 col-sm-4 col-12">
        <button type="submit" class="btn btn-primary">Publish</a>
    </div>
    </div>
</form>
@else
<form id="word_suggestion" method="post" action="{{url('word_suggestions')}}" data-num="{{$word->id}}">
    {{csrf_field()}}
    <input type="hidden" name="word" value="{{$word->id}}" />
    <div id="word_suggestion_red{{$word->id}}"></div>
    <div class="form-row">
    <div class="form-group col-md-10 col-sm-8 col-12">
        <input type="text" class="form-control" placeholder="Your Suggestion" name="comment" />
    </div>
    <div class="col-md-2 col-sm-4 col-12">
        <button type="submit" class="btn btn-primary">Publish</a>
    </div>
    </div>
</form>
@endif