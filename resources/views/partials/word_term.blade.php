
<div class="col-12">
<div class="job-list row word_term_options">
    <div class="job-list-details col-12 col-sm-9 col-md-9">
    <div class="job-list-info">
        <div class="job-list-title">
            @if(($search_text_language != 'ar' && $search_text_language != 'fa') && $word->dictionary_info->source_lang != 1)
                <h5 class="mb-0 @if($word->dictionary_info->source_lang == 1) text-right @else text-left @endif">
                    <a href="#{{$word->id}}" class="@if($word->dictionary_info->source_lang == 1) text-right @else text-left  @endif">{{$word->s_language}}</a>
                </h5>
                <h5 class="mb-0  @if($word->dictionary_info->target_lang == 1) text-right  @else text-left @endif">
                    <a class= @if($word->dictionary_info->target_lang == 1) text-right  @else text-left @endif" href="#{{$word->id}}">{{$word->t_language}}</a>
                </h5>
            @else
                <h5 class="mb-0  @if($word->dictionary_info->target_lang == 1) text-right  @else text-left @endif">
                    <a class= @if($word->dictionary_info->target_lang == 1) text-right  @else text-left @endif" href="#{{$word->id}}">{{$word->t_language}}</a>
                </h5>
                <h5 class="mb-0 @if($word->dictionary_info->source_lang == 1) text-right  @else text-left @endif">
                    <a href="#{{$word->id}}" class="@if($word->dictionary_info->source_lang == 1) text-right @endif">{{$word->s_language}}</a>
                </h5>
            @endif
        </div>
        <div class="job-list-option" id="wordsSearchDictionary">
            
            
            <ul class="nav nav-tabs" id="WordTabs{{$word->id}}" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="explantion{{$word->id}}-tab" data-toggle="tab" href="#explantion{{$word->id}}" role="tab" 
                aria-controls="explantion{{$word->id}}" aria-selected="true">
                    Explantion
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="examples-tab{{$word->id}}" data-toggle="tab" href="#examples{{$word->id}}" role="tab" 
                aria-controls="examples{{$word->id}}" aria-selected="false">Examples (
                @if($word->examples == '') 0 @else {{count(json_decode($word->examples, true))}} @endif )</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="media-tab{{$word->id}}" data-toggle="tab" href="#media{{$word->id}}" role="tab" aria-controls="media{{$word->id}}" 
                aria-selected="false">Media ( {{count($word->media)}} )</a>
            </li>
            </ul>
            <div class="tab-content" id="WordTabs{{$word->id}}Content">
            <div class="tab-pane fade show active" id="explantion{{$word->id}}" role="tabpanel" aria-labelledby="explantion{{$word->id}}-tab">
                <p class=" @if($word->dictionary_info->source_lang == 1) text-right @endif">{{$word->s_explain}}</p>
                <p class=" @if($word->dictionary_info->target_lang == 1) text-right @endif">{{$word->t_explain}}</p>
            </div>
            <div class="tab-pane fade" id="examples{{$word->id}}" role="tabpanel" aria-labelledby="examples-tab{{$word->id}}">
                @if($word->examples != '')
                    @foreach (json_decode($word->examples, true) as $example)
                        <p>{{$example}}</p>
                    @endforeach
                @endif
            </div>
            <div class="tab-pane fade" id="media{{$word->id}}" role="tabpanel" aria-labelledby="media-tab{{$word->id}}">
                <div class="row">
                @foreach ($word->media as $media)
                    <div class="col-12 col-sm-6">
                    @if($media->media_type == 'url')
                        <p><a href="{{$media->media}}" target="_blank">{{$media->media}}</a></p>
                    @elseif($media->media_type == 'png')
                        <p><a href="{{$media->media}}" download>Download File</a></p>
                    @endif
                    </div>
                @endforeach
                </div>
            </div>
            </div>
            
            
        </div>
    </div>
    </div>
    <div class="job-list-favourite-time mb-3  col-12 col-sm-3 col-md-3">
        <div class="d-inline-block">
        <a class="job-list-favourite order-2 text-success" 
        @if(!Auth::check())
            href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal"
        @else
            href="{{url('Favourite-Word/'.$word->id)}}"
        @endif
        >
            @if(Auth::check())
                @if(my_word($word->id, Auth::user()->id))
                <i class="fas fa-heart"></i>
                @else
                <i class="far fa-heart"></i>
                @endif
            @else
            <i class="far fa-heart"></i>
            @endif
        </a> {{count($word->favs)}}
        </div>
        <div class="d-inline-block ml-2">
        <a class="job-list-favourite order-2 text-danger" href="#">
            <i class="far fa-heart"></i>
        </a> 0
        </div>    
        <div class="d-block"><a href="{{url('word-comments/'.$word->id)}}" class="dictionary_comments_link word_dictionary_side_actions wdsa{{$word->id}}" data-num="{{$word->id}}" data-type="comments">Comments ({{count($word->comments)}})</a></div>
        <div class="d-block"><a href="{{url('word-suggestions/'.$word->id)}}" class="dictionary_suggestions_link word_dictionary_side_actions wdsa{{$word->id}}" data-num="{{$word->id}}" data-type="suggestions">Suggestions ({{count($word->suggestions)}})</a></div>
        <div class="d-block"><a href="{{url('word-articles/'.$word->id)}}" class="dictionary_articles_link word_dictionary_side_actions wdsa{{$word->id}}" data-num="{{$word->id}}" data-type="articles">Articles ({{count($word->articles)}})</a></div>
    </div>

<div id="word_dict_sidebar_{{$word->id}}" class="word_info col-12"></div>
</div>

</div>
