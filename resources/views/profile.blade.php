@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('styles')
<style>
    .hidden_footer_section {display: none;}
    .footer.footer.bg-light {margin: 0; padding: 0;}
</style>
@endsection

@section('content')

@include('profile_menu')


<!--=================================
Employer Dashboard -->
<section>
  <div class="container">
      
      <div class="row mb-3 mb-lg-5 mt-3 mt-lg-0">
            <div class="col-lg-4 mb-4 mb-lg-0">
              <div class="candidates-feature-info bg-dark">
                <div class="candidates-info-icon text-white">
                  <i class="fas fa-file-alt"></i>
                </div>
                <div class="candidates-info-content">
                 <h6 class="candidates-info-title text-white">My Articles</h6>
                </div>
                <div class="candidates-info-count">
                  <h3 class="mb-0 text-white">{{Auth::user()->articles->count()}}</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-4 mb-4 mb-lg-0">
              <div class="candidates-feature-info bg-success">
                <div class="candidates-info-icon text-white">
                  <i class="fas fa-language"></i>
                </div>
                <div class="candidates-info-content">
                 <h6 class="candidates-info-title text-white">My Dictionaries</h6>
                </div>
                <div class="candidates-info-count">
                  <h3 class="mb-0 text-white">{{Auth::user()->words->count()}}</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-4 mb-4 mb-lg-0">
              <div class="candidates-feature-info bg-danger">
                <div class="candidates-info-icon text-white">
                  <i class="fas fa-book"></i>
                </div>
                <div class="candidates-info-content">
                 <h6 class="candidates-info-title text-white">My Books</h6>
                </div>
                <div class="candidates-info-count">
                  <h3 class="mb-0 text-white">{{Auth::user()->books->count()}}</h3>
                </div>
              </div>
            </div>
          </div>
          
    <div class="row">
      <div class="col-md-12">
        <div class="user-dashboard-info-box mb-0">
          <div class="section-title-02 mb-4">
            <h4>Favourite Terms</h4>
          </div>
          <div class="row">
            @foreach (Auth::user()->words as $mword)
              @php 
                $word = $mword->word_info 
              @endphp
              @include ('partials.word_term', ['word', 'search_text_language'])
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Employer Dashboard -->


@endsection
