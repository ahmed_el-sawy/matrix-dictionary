<style>
header.header {background: #001935 !important;}
</style>

<!--=================================
inner banner -->
<section class="header-inner header-inner-big bg-holder bg-light pb-5">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <div class="jobber-user-info">
            <div class="profile-avatar">
              @if(Auth::user()->avatar != '')
                <img class="img-fluid " src="{{asset(Auth::user()->avatar)}}" alt="{{Auth::user()->name}}">
              @else
                <img class="img-fluid " src="{{asset('site/images/avatar/00.jpg')}}" alt="{{Auth::user()->name}}">
              @endif
                <a href="{{url('Change-Information')}}"><i class="fas fa-pencil-alt"></i></a>
            </div>
            <div class="profile-avatar-info ml-4">
               <h3 class="text-dark">{{Auth::user()->name}}</h3>
            </div>
          </div>
      </div>
      
      <div class="col-lg-6">
            <div class="progress">
              <div class="progress-bar" role="progressbar" style="width:85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                <span class="progress-bar-number">85%</span>
              </div>
            </div>
            <div class="candidates-skills">
              <div class="candidates-skills-info">
                <h3 class="text-primary">85%</h3>
                <span class="d-block">Skills increased by job Title.</span>
              </div>
              <div class="candidates-required-skills ml-auto mt-sm-0 mt-3">
                <a class="btn btn-dark" href="#">Complete Required Skills</a>
              </div>
            </div>
          </div>

    </div>
  </div>
</section>
<!--=================================
inner banner -->


<section>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="secondary-menu-sticky-top">
          <div class="secondary-menu">
            <ul>
              <li><a @if($active_page == 'Dashboard') class="active" @endif href="{{url('Profile')}}">Dashboard</a></li>
              <li><a @if($active_page == 'Change-Information') class="active" @endif href="{{url('Change-Information')}}">Change Information</a></li>
              <li><a @if($active_page == 'Change-Password') class="active" @endif href="{{url('Change-Password')}}">Change Password</a></li>
              <li><a @if($active_page == 'My-Resume') class="active" @endif href="{{url('My-Resume')}}">My Resume</a></li>
              <li><a @if($active_page == 'My-Blog') class="active" @endif  href="{{url('My-Blog')}}">My Blog</a></li>
              <li><a @if($active_page == 'My-Library') class="active" @endif href="{{url('My-Library')}}">My Library</a></li>
              {{-- <li><a @if($active_page == 'active_page') href="my-dictionaries.php" @endif>My Dictionaries</a></li> --}}
              <li><a href="{{url('logout')}}">Log Out</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>