@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('content')



<!--=================================
inner banner -->
<section class="header-inner header-inner-big bg-holder text-white text-center" style="background-image: url({{asset('site/images/bg/banner-01.jpg')}});">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="text-primary">Register</h2>
        <ol class="breadcrumb mb-0 p-0">
          <li class="breadcrumb-item"><a href="{{url('/')}}"> Home </a></li>
          <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Register </span></li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!--=================================
inner banner -->

<!--=================================
Register -->
<section class="space-ptb">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-8 col-lg-10 col-md-12">
        <div class="login-register">
         <div class="section-title">
          <h4 class="text-center">Create Your Account</h4>
         </div>
          <div class="tab-content">
            <div class="tab-pane active" id="candidate" role="tabpanel">
              <form class="mt-4" action="{{url('Register')}}" method="post">
                {{csrf_field()}}
                @if($errors->any())
                  <div class="alert alert-danger">{{$errors->first()}}</div>
                @endif
                @if(session('success'))
                  <div class="alert alert-success">{{session('success')}}</div>
                @endif
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="register_name">Name</label>
                    <input type="text" class="form-control" name="name" id="register_name" value="{{old('name')}}" />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_mobile">Mobile</label>
                    <input type="text" class="form-control" name="mobile" id="register_mobile" value="{{old('mobile')}}" />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_email">Email</label>
                    <input type="email" class="form-control" name="email" id="register_email" value="{{old('email')}}"  />
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_country">Country</label>
                    <select class="form-control" name="country" id="register_country">
                      <option value="" disabled selected>Choose Country</option>
                      @foreach ($countries as $country)
                        <option value="{{$country->id}}" @if($country->id == old('country')) selected @endif>{{$country->country}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_job">Job Title</label>
                    <select class="form-control" name="job" id="register_job">
                      <option value="" disabled selected>Choose Job Title</option>
                      @foreach ($jobs as $job)
                        <option value="{{$job->id}}" @if($job->id == old('job')) selected @endif>{{$job->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="register_language">Language</label>
                    <select class="form-control" name="language" id="register_language">
                      <option value="" disabled selected>Choose Language</option>
                      @foreach ($languages as $language)
                        <option value="{{$language->id}}" @if($language->id == old('language')) selected @endif>{{$language->language}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Password *</label>
                    <input type="password" class="form-control" name="password">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="password2">Confirm Password *</label>
                    <input type="password" class="form-control" name="password_confirmation">
                  </div>

                  <div class="form-group col-12">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="accepts-terms" name="terms">
                      <label class="custom-control-label" for="accepts-terms">you accept our Terms and Conditions and Privacy Policy</label>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-primary d-block">Sign up</button>
                  </div>
                  <div class="col-md-6 text-md-right mt-2 text-center">
                    <p>Already registered? <a href="{{url('Login')}}" data-toggle="modal" data-target="#LoginModal"> Sign in here</a></p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--=================================
Register -->

@endsection
