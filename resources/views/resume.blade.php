@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('styles')
<style>
    .hidden_footer_section {display: none;}
    .footer.footer.bg-light {margin: 0; padding: 0;}
</style>
@endsection

@section('content')

@include('profile_menu')


<!--=================================
My Profile -->

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="section-title-02">
          <h3>My Resume</h3>
        </div>
      </div>
      <div class="col-lg-4 text-lg-right">
        <a class="btn btn-primary btn-md mb-4 mb-lg-0" href="{{url('Preview-Resume')}}">Preview My Resume</a>
      </div>

      <div class="col-12">
        <!-------- Start Cover Letter ----------------------->
        <div class="user-dashboard-info-box">
          <form method="post" class="my_resume_form" data-target="#cover_letter_results" action="{{url('Cover-Letter')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div id="cover_letter_results"></div>
            <div class="form-group col-md-12 p-0">
              <label>Cover Letter</label>
              <textarea class="form-control ckeditor" name="cover_letter" rows="4">@if($cover_letter !== NULL) {{$cover_letter->letter}} @endif</textarea>
            </div>
            <button class="btn btn-md btn-primary" type="submit">Save</button>
          </form>
        </div>
        <!-------- End Cover Letter ----------------------->


        <!-------- Start Education ----------------------->
        <div class="user-dashboard-info-box">
          <div class="dashboard-resume-title d-flex align-items-center">
            <div class="section-title-02 mb-sm-0">
              <h4 class="mb-0">Education</h4>
            </div>
            <a class="btn btn-md ml-sm-auto btn-primary" data-toggle="collapse" 
            href="#dateposted" role="button" aria-expanded="false" aria-controls="dateposted">
            	Add Education
            </a>
          </div>
			    <!-------- Start New Education ----------------------->
          <div class="collapse @if(count(Auth::user()->educations) == 0) show @endif" id="dateposted">
            <form method="post" class="my_resume_form bg-light p-3 mt-4 row" data-target="#new_education" action="{{url('New-Education')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div id="new_education" class="col-12"></div>
                  <div class="form-group col-md-12">
                    <label>Title</label>
                    <input type="text" class="form-control" value="" name="title">
                  </div>
                  <div class="form-group col-md-6">
                    <label>Institute</label>
                    <input type="text" class="form-control" value="" name="institute">
                  </div>
                  <div class="form-group col-md-3 select-border">
                      <label>From</label>
                      <select class="form-control basic-select" name="from_year">
                        @for ($i = date('Y'); $i >= 1960; $i--)
                          <option value="{{$i}}">{{$i}}</option>
                        @endfor
                      </select>
                  </div>
                  <div class="form-group col-md-3 select-border">
                      <label>To</label>
                      <select class="form-control basic-select mb-2" name="to_year" id="education_to_year">
                        @for ($i = date('Y'); $i >= 1960; $i--)
                          <option value="{{$i}}">{{$i}}</option>
                        @endfor
                      </select>
                      <label><input type="checkbox" name="attend" class="attend_there" value="1" data-selector="#education_to_year" />   Currently Attend There
                  </label>
                </div>
                  
                <div class="form-group col-md-12">
                  <label>Description</label>
                  <textarea class="form-control" rows="4" name="text"></textarea>
                </div>
                <div class="form-group col-md-12 mb-0">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
              </div>
            </form>  
          </div>
			    <!-------- End New Education ----------------------->
          @if(count(Auth::user()->educations) > 0)
                <div class="jobber-candidate-timeline mt-4">
                    <div class="jobber-timeline-icon">
                  <i class="fas fa-graduation-cap"></i>
                </div>
                   
                  @foreach (Auth::user()->educations as $education)
                  	<div class="jobber-timeline-item">
                  		<div class="jobber-timeline-cricle">
                    	<i class="far fa-circle"></i>
                  	</div>
                  		<div class="jobber-timeline-info">
                    <div class="dashboard-timeline-info">
                      <div class="dashboard-timeline-edit">
                        <ul class="list-unstyled d-flex">
                          <li><a class="text-right" data-toggle="collapse" href="#education-{{$education->id}}" role="button" aria-expanded="false" aria-controls="dateposted"> <i class="fas fa-pencil-alt text-info mr-2"></i> </a></li>
                          <li><a href="{{url('delete_education/'.$education->id)}}"><i class="far fa-trash-alt text-danger"></i></a></li>
                        </ul>
                      </div>
                      <span class="jobber-timeline-time">{{$education->from_year}} - @if($education->to_year == 0) Present @else {{$education->to_year}} @endif</span>
                      <h6 class="mb-2">{{$education->title}}</h6>
                      <span>- {{$education->institute}}</span>
                      <p class="mt-2">{{$education->text}}</p>
                    </div>
                    <div class="collapse" id="education-{{$education->id}}">
                      <div class="bg-light p-3">
                        <form method="post" class="my_resume_form bg-light p-3 mt-4 row" data-target="#update_education_{{$education->id}}" action="{{url('Update-Education/'.$education->id)}}" enctype="multipart/form-data">
                          {{csrf_field()}}
                          <div id="update_education_{{$education->id}}" class="col-12"></div>
                            <div class="form-group col-md-12">
                              <label>Title</label>
                              <input type="text" class="form-control" value="{{$education->title}}" name="title">
                            </div>
                            <div class="form-group col-md-6">
                              <label>Institute</label>
                              <input type="text" class="form-control" value="{{$education->institute}}" name="institute">
                            </div>
                            <div class="form-group col-md-3 select-border">
                                <label>From</label>
                                <select class="form-control basic-select" name="from_year">
                                  @for ($i = date('Y'); $i >= 1960; $i--)
                                    <option value="{{$i}}" @if($education->from_year == $i) selected @endif>{{$i}}</option>
                                  @endfor
                                </select>
                            </div>
                            <div class="form-group col-md-3 select-border">
                                <label>To</label>
                                <select class="form-control basic-select mb-2" name="to_year" id="education_to_year_{{$education->id}}"
                                @if($education->to_year == 0) disabled="disabled" @endif>
                                  @for ($i = date('Y'); $i >= 1960; $i--)
                                    <option value="{{$i}}" @if($education->to_year == $i) selected @endif>{{$i}}</option>
                                  @endfor
                                </select>
                                <label><input type="checkbox" name="attend" class="attend_there" value="1" @if($education->to_year == 0) checked="checked" @endif
                                data-selector="#education_to_year_{{$education->id}}" />   Currently Attend There
                            </label>
                  </div>
                            
                            <div class="form-group col-md-12">
                              <label>Description</label>
                              <textarea class="form-control" rows="4" name="text">{{$education->text}}</textarea>
                            </div>
                            <div class="form-group col-md-12 mb-0">
                          <button type="submit" class="btn btn-md btn-primary">Save</button>
                        </div>
                      </form> 
                      </div>
                    </div>
                  </div>
                	</div>
                   @endforeach
                </div>
        	@else
            @endif
            
          </div>
        
        <!-------- End Education ----------------------->



        

        <!-------- Start Work & Experince ----------------------->
        <div class="user-dashboard-info-box">
          <div class="dashboard-resume-title d-flex align-items-center">
            <div class="section-title-02 mb-sm-0">
              <h4 class="mb-0">Work & Experince</h4>
            </div>
            <a class="btn btn-md ml-sm-auto btn-primary" data-toggle="collapse" 
            href="#experince" role="button" aria-expanded="false" aria-controls="experince">
            	Add Expericen
            </a>
          </div>
			<!-------- Start New experince ----------------------->
          	<div class="collapse @if(count(Auth::user()->experinces) == 0) show @endif" id="experince">
            	<form method="post" class="my_resume_form bg-light p-3 mt-4 row" data-target="#new_experince" action="{{url('New-Experince')}}" enctype="multipart/form-data">
              		{{csrf_field()}}
              		<div id="new_experince" class="col-12"></div>
                    <div class="form-group col-md-12">
                      <label>Title</label>
                      <input type="text" class="form-control" value="" name="title">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Company</label>
                      <input type="text" class="form-control" value="" name="company">
                    </div>
                    <div class="form-group col-md-3 select-border">
                        <label>From</label>
                        <input type="date" class="form-control" name="from_year" />
                    </div>
                    <div class="form-group col-md-3 select-border">
                        <label>To</label>
                        <input type="date" class="form-control mb-2" name="to_year" id="experince_to_year">
                        <label><input type="checkbox" name="attend" class="attend_there" value="1" data-selector="#experince_to_year" />   Currently Attend There
                    </label>
					</div>
                    
                    <div class="form-group col-md-12">
                      <label>Description</label>
                      <textarea class="form-control" rows="4" name="text"></textarea>
                    </div>
                    <div class="form-group col-md-12 mb-0">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
            	</form>  
         	</div>
			<!-------- End New experince ----------------------->
            @if(count(Auth::user()->experinces) > 0)
                <div class="jobber-candidate-timeline mt-4">
                    <div class="jobber-timeline-icon">
                  <i class="fas fa-briefcase"></i>
                </div>
                   
                  @foreach (Auth::user()->experinces as $experince)
                  	<div class="jobber-timeline-item">
                  		<div class="jobber-timeline-cricle">
                    	<i class="far fa-circle"></i>
                  	</div>
                  		<div class="jobber-timeline-info">
                    <div class="dashboard-timeline-info">
                      <div class="dashboard-timeline-edit">
                        <ul class="list-unstyled d-flex">
                          <li><a class="text-right" data-toggle="collapse" href="#experince-{{$experince->id}}" role="button" aria-expanded="false" aria-controls="dateposted"> <i class="fas fa-pencil-alt text-info mr-2"></i> </a></li>
                          <li><a href="{{url('delete_experince/'.$experince->id)}}"><i class="far fa-trash-alt text-danger"></i></a></li>
                        </ul>
                      </div>
                      <span class="jobber-timeline-time">{{$experince->from_year}} - @if($experince->to_year == '') Present @else {{$experince->to_year}} @endif</span>
                      <h6 class="mb-2">{{$experince->title}}</h6>
                      <span>- {{$experince->company}}</span>
                      <p class="mt-2">{{$experince->text}}</p>
                    </div>
                    <div class="collapse" id="experince-{{$experince->id}}">
                      <div class="bg-light p-3">
                        <form method="post" class="my_resume_form bg-light p-3 mt-4 row" data-target="#update_experince_{{$experince->id}}" action="{{url('Update-Experince/'.$experince->id)}}" enctype="multipart/form-data">
                          {{csrf_field()}}
                          <div id="update_experince_{{$experince->id}}" class="col-12"></div>
                            <div class="form-group col-md-12">
                              <label>Title</label>
                              <input type="text" class="form-control" value="{{$experince->title}}" name="title">
                            </div>
                            <div class="form-group col-md-6">
                              <label>Institute</label>
                              <input type="text" class="form-control" value="{{$experince->company}}" name="company">
                            </div>
                            <div class="form-group col-md-3 select-border">
                                <label>From</label>
                                <input type="date" class="form-control" name="from_year" value="{{$experince->from_year}}" />
                            </div>
                            <div class="form-group col-md-3 select-border">
                                <label>To</label>
                                <input type="date" class="form-control mb-2" name="to_year" id="experince_to_year" value="{{$experince->to_year}}" @if($experince->to_year == '') disabled @endif>
                                <label><input type="checkbox" name="attend" class="attend_there" value="1" @if($experince->to_year == '') checked @endif data-selector="#experince_to_year" />   Currently Attend There</label>
                              </div>                            
                            <div class="form-group col-md-12">
                              <label>Description</label>
                              <textarea class="form-control" rows="4" name="text">{{$experince->text}}</textarea>
                            </div>
                            <div class="form-group col-md-12 mb-0">
                          <button type="submit" class="btn btn-md btn-primary">Save</button>
                        </div>
                      </form> 
                      </div>
                    </div>
                  </div>
                	</div>
                   @endforeach
                </div>
        	@else
            @endif
            
          </div>
        
        <!-------- End Work & Experince ----------------------->


        <!-------- Start Skills ----------------------->

        <div class="user-dashboard-info-box">
          <div class="dashboard-resume-title d-flex align-items-center">
            <div class="section-title-02 mb-sm-0">
              <h4 class="mb-0">Professional Skill</h4>
            </div>
            <a class="btn btn-md ml-sm-auto btn-primary" href="{{url('add_skill')}}" id="add_skill">Add Skill</a>
          </div>
          <div class="collapse show" id="dateposted-11">
            <div class="bg-light p-3 mt-4">
            <form method="post" class="my_resume_form form-row align-items-center" data-target="#skills" action="{{url('Skills')}}" enctype="multipart/form-data">  
              {{csrf_field()}}
              <div id="skills" class="col-12"></div>

              <div class="col-12" id="skills_list">
                @if(count(Auth::user()->skills) > 0)
                @foreach (Auth::user()->skills as $skill)
                  <div class="row skill_row">
                    <div class="form-group col-md-6">
                      <label>Title</label>
                      <input type="text" class="form-control" name="title[]" value="{{$skill->title}}">
                    </div>
                    <div class="form-group col-md-5">
                      <label>Percentage</label>
                      <div class="input-group">
                        <input type="number" class="form-control" name="percentage[]" value="{{$skill->percentage}}">
                        <div class="input-group-append">
                          <span class="input-group-text">%</span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-md-1 text-center">
                      <a class="mt-5 d-block delte_resume_skill" href="Javascript:void(0);"><i class="far fa-trash-alt text-danger"></i></a>
                    </div>
                  </div>
                @endforeach
                @else
                <div class="row skill_row">
                    <div class="form-group col-md-6">
                      <label>Title</label>
                      <input type="text" class="form-control" name="title[]" value="">
                    </div>
                    <div class="form-group col-md-5">
                      <label>Percentage</label>
                      <div class="input-group">
                        <input type="number" class="form-control" name="percentage[]" value="">
                        <div class="input-group-append">
                          <span class="input-group-text">%</span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-md-1 text-center">
                      <a class="mt-5 d-block delte_resume_skill" href="Javascript:void(0);"><i class="far fa-trash-alt text-danger"></i></a>
                    </div>
                  </div>

                @endif
                </div>
                <div class="col-12">
                <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        
        <!-------- End Skills ----------------------->




        <!-------- Start Skills ----------------------->

        <div class="user-dashboard-info-box">
          <div class="dashboard-resume-title d-flex align-items-center">
            <div class="section-title-02 mb-sm-0">
              <h4 class="mb-0">Languages</h4>
            </div>
            <a class="btn btn-md ml-sm-auto btn-primary" href="{{url('add_language')}}" id="add_language">Add Language</a>
          </div>
          <div class="collapse show" id="dateposted-11">
            <div class="bg-light p-3 mt-4">
            <form method="post" class="my_resume_form form-row align-items-center" data-target="#languages" action="{{url('Languages')}}" enctype="multipart/form-data">  
              {{csrf_field()}}
              <div id="languages" class="col-12"></div>

              <div class="col-12" id="langauges_list">
                @if(count(Auth::user()->languages) > 0)
                  @foreach (Auth::user()->languages as $language)
                  <div class="row language_row">
                    <input type="hidden" name="language_selector[]" value="{{$language->id}}" />
                    <div class="form-group col-md-6">
                      <label>Language</label>
                      <select class="form-control" name="language[]">
                        <option value="" disabled selected>Choose Language</option>
                        @foreach ($languages as $lang)
                          <option value="{{$lang->id}}" @if($lang->id == $language->language) selected @endif>{{$lang->language}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-md-5">
                    <label>Level</label>
                      <select class="form-control" name="language_level[]">
                        <option value="" disabled selected>Choose Language Level</option>
                        @foreach ($levels as $level)
                          <option value="{{$level->id}}" @if($level->id == $language->level) selected @endif>{{$level->title}}</option>
                        @endforeach
                      </select>  
                    </div>
                    <div class="form-group col-md-1 text-center">
                      <a class="mt-5 d-block delte_resume_language" href="Javascript:void(0);"><i class="far fa-trash-alt text-danger"></i></a>
                    </div>
                  </div>
                  @endforeach
                @else
                  <div class="row language_row">
                    <input type="hidden" name="language_selector[]" value="0" />
                    <div class="form-group col-md-6">
                      <label>Language</label>
                      <select class="form-control" name="language[]">
                        <option value="" disabled selected>Choose Language</option>
                        @foreach ($languages as $language)
                          <option value="{{$language->id}}">{{$language->language}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-md-5">
                    <label>Level</label>
                      <select class="form-control" name="language_level[]">
                        <option value="" disabled selected>Choose Language Level Skill</option>
                        @foreach ($levels as $level)
                          <option value="{{$level->id}}">{{$level->title}}</option>
                        @endforeach
                      </select>  
                    </div>
                    <div class="form-group col-md-1 text-center">
                      <a class="mt-5 d-block delte_resume_language" href="Javascript:void(0);"><i class="far fa-trash-alt text-danger"></i></a>
                    </div>
                  </div>
                @endif
                </div>
                <div class="col-12">
                <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        
        <!-------- End Skills ----------------------->


        
        <!-------- Start Awards ----------------------->
        <div class="user-dashboard-info-box">
          <div class="dashboard-resume-title d-flex align-items-center">
            <div class="section-title-02 mb-sm-0">
              <h4 class="mb-0">Awards</h4>
            </div>
            <a class="btn btn-md ml-sm-auto btn-primary" data-toggle="collapse" 
            href="#awards" role="button" aria-expanded="false" aria-controls="dateposted">
            	Add Awards
            </a>
          </div>
			<!-------- Start New Education ----------------------->
          	<div class="collapse @if(count(Auth::user()->awards) == 0) show @endif" id="awards">
            	<form method="post" class="my_resume_form bg-light p-3 mt-4 row" data-target="#new_awards" action="{{url('New-Award')}}" enctype="multipart/form-data">
              		{{csrf_field()}}
              		<div id="new_awards" class="col-12"></div>
                    <div class="form-group col-md-12">
                      <label>Title</label>
                      <input type="text" class="form-control" value="" name="title">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Institute</label>
                      <input type="text" class="form-control" value="" name="institute">
                    </div>
                    <div class="form-group col-md-3 select-border">
                        <label>From</label>
                        <input type="date" class="form-control" name="start_date">
                    </div>
                    <div class="form-group col-md-3 select-border">
                        <label>To</label>
                        <input type="date" class="form-control" name="end_date">
          					</div>
                    
                    <div class="form-group col-md-12">
                      <label>Description</label>
                      <textarea class="form-control" rows="4" name="text"></textarea>
                    </div>
                    <div class="form-group col-md-12 mb-0">
                  <button type="submit" class="btn btn-md btn-primary">Save</button>
                </div>
            	</form>  
         	</div>
			<!-------- End New Award ----------------------->
            @if(count(Auth::user()->awards) > 0)
                <div class="jobber-candidate-timeline mt-4">
                    <div class="jobber-timeline-icon">
                  <i class="fas fa-trophy"></i>
                </div>
                   
                  @foreach (Auth::user()->awards as $award)
                  	<div class="jobber-timeline-item">
                  		<div class="jobber-timeline-cricle">
                    	<i class="far fa-circle"></i>
                  	</div>
                  		<div class="jobber-timeline-info">
                    <div class="dashboard-timeline-info">
                      <div class="dashboard-timeline-edit">
                        <ul class="list-unstyled d-flex">
                          <li><a class="text-right" data-toggle="collapse" href="#award-{{$award->id}}" role="button" aria-expanded="false" aria-controls="dateposted"> <i class="fas fa-pencil-alt text-info mr-2"></i> </a></li>
                          <li><a href="{{url('delete_award/'.$award->id)}}"><i class="far fa-trash-alt text-danger"></i></a></li>
                        </ul>
                      </div>
                      <span class="jobber-timeline-time">{{$award->start_date}} - {{$award->end_date}}</span>
                      <h6 class="mb-2">{{$award->title}}</h6>
                      <span>- {{$award->institute}}</span>
                      <p class="mt-2">{{$award->text}}</p>
                    </div>
                    <div class="collapse" id="award-{{$award->id}}">
                      <div class="bg-light p-3">
                        <form method="post" class="my_resume_form bg-light p-3 mt-4 row" data-target="#update_award_{{$award->id}}" 
                          action="{{url('Update-Award/'.$award->id)}}" enctype="multipart/form-data">
                          {{csrf_field()}}
                          <div id="update_award_{{$award->id}}" class="col-12"></div>
                            <div class="form-group col-md-12">
                              <label>Title</label>
                              <input type="text" class="form-control" value="{{$award->title}}" name="title">
                            </div>
                            <div class="form-group col-md-6">
                              <label>Institute</label>
                              <input type="text" class="form-control" value="{{$award->institute}}" name="institute">
                            </div>
                            <div class="form-group col-md-3 select-border">
                              <label>From</label>
                              <input type="date" class="form-control" name="start_date"  value="{{$award->start_date}}">
                          </div>
                          <div class="form-group col-md-3 select-border">
                              <label>To</label>
                              <input type="date" class="form-control" name="end_date"  value="{{$award->end_date}}">
                          </div>
                            
                            <div class="form-group col-md-12">
                              <label>Description</label>
                              <textarea class="form-control" rows="4" name="text">{{$award->text}}</textarea>
                            </div>
                            <div class="form-group col-md-12 mb-0">
                          <button type="submit" class="btn btn-md btn-primary">Save</button>
                        </div>
                      </form> 
                      </div>
                    </div>
                  </div>
                	</div>
                   @endforeach
                </div>
        	@else
            @endif
            
          </div>
        
        <!-------- End Awards ----------------------->


      </div>
    </div>
  </div>
</section>

<!--=================================
My Profile -->

@endsection