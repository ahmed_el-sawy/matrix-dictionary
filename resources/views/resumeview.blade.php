@extends('layouts.app')

@section('meta')
<title>Matrix Dictionary</title>
@endsection

@section('styles')
<style>
header.header.bg-dark {background: #001935 !important;}
</style>
@endsection

@section('content')
<section class="space-ptb">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-9">
        <div class="row">
          <div class="col-lg-5">
            <div class="resume-base bg-light user-dashboard-info-box">
              <div class="profile">
                <div class="jobber-user-info">
                  <div class="profile-avatar">
                      @if($user->avatar != '')
                        <img class="img-fluid " src="{{asset($user->avatar)}}" alt="{{$user->name}}">
                      @else
                        <img class="img-fluid " src="{{asset('site/images/avatar/00.jpg')}}" alt="{{$user->name}}">
                      @endif
                  </div>
                  <div class="profile-avatar-info mt-3">
                    <h5>{{$user->name}}</h5>
                  </div>
                </div>
              </div>
              <div class="about-candidate border-top">
                @if($user->pinfo == 1)
                <div class="candidate-info">
                  <h6>Email:</h6>
                  <p>{{$user->email}}</p>
                </div>
                <div class="candidate-info">
                  <h6>Phone:</h6>
                  <p>{{$user->mobile}}</p>
                </div>
                @endif
                @if($user->country_info !== NULL)
                <div class="candidate-info">
                  <h6>Country:</h6>
                  <p>{{$user->country_info->country}}</p>
                </div>
                @endif
                @if($user->job_info !== NULL)
                <div class="candidate-info">
                  <h6>Job Title:</h6>
                  <p>{{$user->job_info->name}}</p>
                </div>
                @endif
                @if($user->language_info !== NULL)
                <div class="candidate-info">
                  <h6>Language:</h6>
                  <p>{{$user->language_info->language}}</p>
                </div>
                @endif
                @if($user->cover_letter !== NULL)
                {!!$user->cover_letter->letter!!}
                @endif
              </div>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="resume-experience pl-0">
              @if(count($user->educations) > 0)
              <div class="timeline-box">
                <h5 class="resume-experience-title">Education:</h5>
                <div class="jobber-candidate-timeline">
                    @foreach ($user->educations as $education)
                        <div class="jobber-timeline-item">
                            <div class="jobber-timeline-cricle">
                              <i class="far fa-circle"></i>
                            </div>
                            <div class="jobber-timeline-info">
                              <div class="dashboard-timeline-info">
                                <span class="jobber-timeline-time">{{$education->from_year}} - @if($education->to_year == 0) Present @else {{$education->to_year}} @endif</span>
                                <h6 class="mb-2">{{$education->title}}</h6>
                                <span>- {{$education->institute}}</span>
                                <p class="mt-2">{{$education->text}}</p>
                              </div>
                            </div>
                          </div>
                    @endforeach

                </div>
              </div>
              @endif
              @if(count($user->experinces) > 0)
              <div class="timeline-box mt-4">
                <h5 class="resume-experience-title">Work & Experience:</h5>
                <div class="jobber-candidate-timeline">
                    @foreach ($user->experinces as $experince)
                        <div class="jobber-timeline-item">
                            <div class="jobber-timeline-cricle">
                              <i class="far fa-circle"></i>
                            </div>
                            <div class="jobber-timeline-info">
                              <div class="dashboard-timeline-info">
                                <span class="jobber-timeline-time">{{$experince->from_year}} - @if($experince->to_year == 0) Present @else {{$experince->to_year}} @endif</span>
                                <h6 class="mb-2">{{$experince->title}}</h6>
                                <span>- {{$experince->company}}</span>
                                <p class="mt-2">{{$experince->text}}</p>
                              </div>
                            </div>
                          </div>
                    @endforeach
                </div>
              </div>
              @endif
              @if(count($user->skills) > 0)
                <div class="timeline-box mt-4">
                  <h5>Professional Skill:</h5>
                    @foreach ($user->skills as $skill)
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width:{{$skill->percentage}}%" aria-valuenow="{{$skill->percentage}}" 
                        aria-valuemin="0" aria-valuemax="100">
                          <div class="progress-bar-title">{{$skill->title}}</div>
                          <span class="progress-bar-number">{{$skill->percentage}}%</span>
                        </div>
                      </div>
                    @endforeach
                </div>
                @endif
                @if(count($user->languages) > 0)
                <div class="timeline-box mt-4">
                  <h5>Languages:</h5>
                    @foreach ($user->languages as $language)
                      <p class="language_view_resume"><b>{{$language->language_info->language}} : </b><span class="ml-auto">{{$language->level_info->title}}</span></p>
                    @endforeach
                </div>
                @endif
                @if(count($user->awards) > 0)
              <div class="timeline-box mt-4">
                <h5 class="resume-experience-title">Awards:</h5>
                <div class="jobber-candidate-timeline">
                    @foreach ($user->awards as $award)
                        <div class="jobber-timeline-item">
                            <div class="jobber-timeline-cricle">
                              <i class="far fa-circle"></i>
                            </div>
                        <div class="jobber-timeline-info">
                          <div class="dashboard-timeline-info">
                            <span class="jobber-timeline-time">{{$award->start_date}} - {{$award->end_date}}</span>
                            <h6 class="mb-2">{{$award->title}}</h6>
                            <span>- {{$award->institute}}</span>
                            <p class="mt-2">{{$award->text}}</p>
                          </div>
                        </div>
                      </div>
                    @endforeach
                    
                </div>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection