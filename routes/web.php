<?php

Auth::routes();
/**
 * *******************************************
 * Admin Routes ********************************
 * *******************************************
 */

Route::match(['get','post'],'/admin','AdminController@login')->name('loginAdmin');
Route::get('login','AdminController@loginform');
Route::get('admin/login','AdminController@loginform')->name('login');

Route::group(['middleware' => ['admin', 'auth'], 'namespace' => 'Admin', 'prefix' => 'admin'],function () {
    Route::post('logout','AdminController@logout')->name('adminLogout');
    // dashboard home
    Route::get('dashboard', 'HomeController@index');
    Route::get('importFile', 'HomeController@create');
    Route::post('importFile', 'HomeController@store');

    // Users Routes
    Route::resource('users', 'UsersController');
    Route::get('users/{id}/status', 'UsersController@status');
    Route::get('users/{id}/delete', 'UsersController@destroy');
    Route::post('users_ajax', 'UsersController@users');

    // Managers Routes
    Route::resource('managers', 'ManagersController');
    Route::get('managers/{id}/status', 'ManagersController@status');
    Route::get('managers/{id}/delete', 'ManagersController@destroy');
    Route::post('managers_ajax', 'ManagersController@managers');

    // Dictionaries Routes
    Route::resource('dictionaries', 'DictionariesController');
    Route::get('dictionaries/{id}/status', 'DictionariesController@status');
    Route::get('dictionaries/{id}/delete', 'DictionariesController@destroy');
    Route::post('dictionaries_ajax', 'DictionariesController@dictionaries');

    // Countries Routes
    Route::resource('countries', 'CountriesController');
    Route::get('countries/{id}/status', 'CountriesController@status');
    Route::get('countries/{id}/delete', 'CountriesController@destroy');
    Route::post('countries_ajax', 'CountriesController@countries');

    // Cities Routes
    Route::resource('cities', 'CitiesController');
    Route::get('cities/{id}/status', 'CitiesController@status');
    Route::get('cities/{id}/delete', 'CitiesController@destroy');
    Route::post('cities_ajax', 'CitiesController@cities');

    // Permissions Routes
    Route::resource('roles', 'RolesController');
    Route::post('roles_ajax', 'RolesController@roles');

    // Languages Routes
    Route::resource('languages', 'LanguagesController');
    Route::get('languages/{id}/delete', 'LanguagesController@destroy');
    Route::post('languages_ajax', 'LanguagesController@languages');

    // Jobs Routes
    Route::resource('jobs', 'JobsController');
    Route::get('jobs/{id}/delete', 'JobsController@destroy');
    Route::post('jobs_ajax', 'JobsController@jobs');
     // Jobs Routes
     Route::resource('categories', 'CategoriesController');
     Route::get('categories_new', 'CategoriesController@new_index');
     Route::get('categories/{id}/delete', 'CategoriesController@destroy');
     Route::post('categories_ajax', 'CategoriesController@categories');
     Route::post('categories_ajax/{id}', 'CategoriesController@sub_categories');

     Route::post('sub_categories_dropdown', 'CategoriesController@sub_categories_dropdown');
     
     // Dictionaries Routes
     Route::resource('dictionaries', 'DictionariesController');
     Route::get('dictionaries/{id}/status', 'DictionariesController@status');
     Route::get('dictionaries/{id}/delete', 'DictionariesController@destroy');
     Route::post('dictionaries_ajax', 'DictionariesController@dictionaries');
     // Books Routes
     Route::resource('books', 'BooksController');
     Route::get('books/{id}/status', 'BooksController@status');
     Route::get('books/{id}/delete', 'BooksController@destroy');
     Route::post('books_ajax', 'BooksController@books');
     // words Routes
     Route::get('words_create_file/{id}', 'WordsController@create_file');
     Route::resource('words', 'WordsController');
     Route::resource('dictionary_file', 'DictionaryFileController');
     Route::get('words/{id}/delete', 'WordsController@destroy');
     Route::get('dictionary_file/{id}/delete', 'DictionaryFileController@destroy');
     Route::post('words_ajax', 'WordsController@words');

     Route::get('media/{id}', 'WordsController@media');
     Route::get('examples/{id}', 'WordsController@examples');

     // words Routes
     Route::resource('search', 'SearchController');
     Route::post('results_ajax', 'SearchController@results_ajax');

     // Home Page Slider
     Route::resource('slider', 'SliderController');
     Route::get('slider_text', 'SliderController@slider_text');
     Route::post('slider_text', 'SliderController@save_slider_text');
        
     // Banner Under Slider
     Route::get('banner_under_slider', 'HomePageController@banner');
     Route::post('banner_under_slider', 'HomePageController@save_banner');
        
     // Home Categories Section
     Route::get('categories_section', 'HomePageController@categories_section');
     Route::post('categories_section', 'HomePageController@save_categories_section');   
             
     // Home About Us Section
     Route::get('home_about_us', 'HomePageController@home_about_us');
     Route::post('home_about_us', 'HomePageController@save_home_about_us');  
     Route::resource('home_about_us_sections', 'HomeAboutController');
     
    Route::get('search_words', 'HomePageController@search_words');
    Route::post('search_words', 'HomePageController@save_search_words');      

    Route::get('work_process', 'HomePageController@work_process');
    Route::post('work_process', 'HomePageController@save_work_process');      
    Route::resource('home_work_process_sections', 'HomeProcessController');
});

/**
 * *******************************************
 * Web Routes ********************************
 * *******************************************
 */

Route::group(['middleware' => 'FrontEnd'],function () {
    // Home Page
    Route::get('/', 'HomeController@index');
    // Contact Us
    Route::get('Contact-Us', 'HomeController@contact');
    Route::post('Contact-Us', 'HomeController@send_contact');    
    // Search Words
    Route::get('Search', 'HomeController@search');
    // Library
    Route::get('Library', 'LibraryController@index');    
    Route::get('Library-File/{id}', 'LibraryController@show');        
    // Blog
    Route::get('Blog', 'BlogController@index');    
    Route::get('User-Articles/{id}', 'BlogController@user_articles');
    Route::get('View-Resume/{id}', 'UserController@view_resume');
    Route::get('Article/{id}', 'BlogController@article');            
    // Register
    Route::get('Register', 'UserController@register_form');
    Route::post('Register', 'UserController@register');
    // Login
    Route::post('Login', 'UserController@login');
    Route::get('activate_account/{token}', 'UserController@activate_account');
    // Subscribe
    Route::post('subscribe', 'HomeController@subscribe');
    
    Route::post('word-comments/{id}', 'WordController@word_comments');
    Route::post('word_comment', 'WordController@save_word_comments');

    Route::post('word-suggestions/{id}', 'WordController@word_suggestions');
    Route::post('word_suggestions', 'WordController@save_word_suggestions');
    Route::post('like_suggesations/{id}', 'WordController@like_suggesations');
    Route::post('dislike_suggesations/{id}', 'WordController@dislike_suggesations');

    Route::post('word-articles/{id}', 'WordController@word_articles');


    Route::group(['middleware' => 'auth'],function () {
        Route::get('Profile', 'UserController@profile');

        Route::get('Favourite-Word/{id}', 'UserController@favourite_word');
        Route::post('Favourite-Book/{id}', 'UserController@favourite_book');

        Route::get('Change-Information', 'UserController@change_information');
        Route::post('Change-Information', 'UserController@save_information');
 
        Route::get('Change-Password', 'UserController@change_password');
        Route::post('Change-Password', 'UserController@save_password');

        Route::get('My-Resume', 'UserResumeController@index');
        Route::get('Preview-Resume', 'UserController@preview_resume');
        Route::post('Cover-Letter', 'UserResumeController@cover_letter');

        
        Route::post('New-Education', 'UserResumeController@new_education');
        Route::post('Update-Education/{id}', 'UserResumeController@update_education');
        Route::get('delete_education/{id}', 'UserResumeController@delete_education');

        Route::post('New-Experince', 'UserResumeController@new_experince');
        Route::post('Update-Experince/{id}', 'UserResumeController@update_experince');
        Route::get('delete_experince/{id}', 'UserResumeController@delete_experince');

        Route::post('Skills', 'UserResumeController@skills');
        Route::post('add_skill', 'UserResumeController@add_skill');

        Route::post('Languages', 'UserResumeController@languages');
        Route::post('add_language', 'UserResumeController@add_language');

        Route::post('New-Award', 'UserResumeController@new_award');
        Route::post('Update-Award/{id}', 'UserResumeController@update_award');
        Route::get('delete_award/{id}', 'UserResumeController@delete_award');

        Route::get('My-Blog', 'UserBlogController@index');        
        Route::get('New-Article', 'UserBlogController@create');        
        Route::get('New-Article/{word}', 'UserBlogController@create_wword');
        Route::post('Post-Article', 'UserBlogController@store');        
        Route::get('Edit-Article/{id}', 'UserBlogController@edit');        
        Route::post('Update-Article/{id}', 'UserBlogController@update');        
        Route::get('Delete-Article/{id}', 'UserBlogController@destroy');        
        Route::post('article_comment', 'UserBlogController@article_comment');        
        
        Route::get('My-Library', 'UserLibraryController@index');        
        Route::post('Save-New-Book', 'UserLibraryController@save_book');        
        Route::post('Update-Book/{id}', 'UserLibraryController@update_book');        
        Route::post('delete_book/{id}', 'UserLibraryController@delete_book');
        
        Route::get('logout', 'UserController@logout');        
    });
});
